CREATE TABLE IF NOT EXISTS `m_approval` (
  `approval_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) DEFAULT NULL,
  `create_datetime` varchar(14) DEFAULT NULL,
  `update_datetime` varchar(14) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`approval_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `m_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(50) NOT NULL,
  `client_id` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) NOT NULL,
  `api_get_url` varchar(100) NOT NULL,
  `command_as_json` text NOT NULL,
  `request_date` datetime NOT NULL,
  `response_as_json` text NOT NULL,
  `response_date` datetime DEFAULT NULL,
  `result_remark` smallint(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `m_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `role_code` varchar(100) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `m_role_task_crud_active` (
  `role_task_crud_active_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `task_crud_id` bigint(20) DEFAULT NULL,
  `create_task_active` varchar(255) DEFAULT NULL,
  `read_task_active` varchar(255) DEFAULT NULL,
  `update_task_active` varchar(255) DEFAULT NULL,
  `delete_task_active` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_task_crud_active_id`)
) ENGINE=InnoDB AUTO_INCREMENT=645 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `m_task` (
  `task_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `task_code` varchar(255) DEFAULT NULL,
  `task_category_id` bigint(20) NOT NULL,
  `task_sub_category_id` bigint(20) NOT NULL,
  `crud_type` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `m_task_category` (
  `task_category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_category_code` varchar(255) DEFAULT NULL,
  `task_category_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`task_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `m_task_crud` (
  `task_crud_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_category_id` bigint(20) DEFAULT NULL,
  `create_task_id` bigint(20) DEFAULT NULL,
  `read_task_id` bigint(20) DEFAULT NULL,
  `update_task_id` bigint(20) DEFAULT NULL,
  `delete_task_id` bigint(20) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `task_crud_name` varchar(255) DEFAULT NULL,
  `task_crud_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`task_crud_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `m_task_sub_category` (
  `task_sub_category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_sub_category_code` varchar(255) DEFAULT NULL,
  `task_sub_category_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`task_sub_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `m_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `create_datetime` varchar(14) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(14) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `branch_code` varchar(100) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `village` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `user_image` longtext,
  `user_role` varchar(25) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_fullname` varchar(255) DEFAULT NULL,
  `status_login` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(256) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `t_approval_config` (
  `approval_config_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approval_id` bigint(20) DEFAULT NULL,
  `role_task_id_submit` bigint(20) DEFAULT NULL,
  `role_task_id_approval` bigint(20) DEFAULT NULL,
  `flag_need_approval` varchar(14) DEFAULT NULL,
  `create_datetime` varchar(14) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(14) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`approval_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=665 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `t_approval_request` (
  `approval_request_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id_request` bigint(20) DEFAULT NULL,
  `role_id_request` bigint(20) DEFAULT NULL,
  `role_id_approve` bigint(20) DEFAULT NULL,
  `task_id` bigint(20) DEFAULT NULL,
  `json_string_data` longtext NOT NULL,
  `json_string_data_before` longtext NOT NULL,
  `json_string_data_after` longtext NOT NULL,
  `request_status` varchar(100) DEFAULT NULL,
  `create_datetime` varchar(50) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(50) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`approval_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `t_role_task` (
  `role_task_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `task_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`role_task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7460 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `t_task_api` (
  `task_api_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(20) DEFAULT NULL,
  `api_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`task_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS`t_user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_datetime` varchar(255) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `update_datetime` varchar(255) DEFAULT NULL,
  `update_user_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `flg_default_role` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
