package com.mlpt.bvnextgen.controller.userManagement;

import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.PrepareServiceOutput;
import com.mlpt.bvnextgen.resource.userManagement.Logout;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableResourceServer
@Tag(name = "User Management", description = "User Management")
public class LogoutServiceController {

	@Autowired
	Logout logout;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.LOGOUT_SERVICE;
    private static JSONObject boOutput;
    static ServiceOutput serviceOutput = new ServiceOutput();

	@Operation(summary = "Logout API",
			description = " API untuk logout ",
            security = { @SecurityRequirement(name = "bearer-key") },
			tags = { "logout" })
    @RequestMapping(
            value = restUrlAddress,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ServiceOutput>> callService(@RequestBody JSONObject inputUser) {

    	boOutput = logout.processBo(inputUser);
    	finalServiceOutput = PrepareServiceOutput.prepareServiceOutput(boOutput);

        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();
        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,HttpStatus.OK);
    }

}
