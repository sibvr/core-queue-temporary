package com.mlpt.bvnextgen.controller.userManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ActivationUserWithCodeApiEntity;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.JSONUtil;
import com.mlpt.bvnextgen.resource.core.PrepareServiceOutput;
import com.mlpt.bvnextgen.resource.userManagement.ActivationUserWithCode;

@RestController
@EnableResourceServer
@Tag(name = "User Management", description = "User Management")
public class ActivationUserWithCodeServiceController {

	@Autowired
	ActivationUserWithCode activationUserWithCode;
	
	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	@Value("${javainuse.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${javainuse.rabbitmq.routingkey}")
	private String routingkey;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.ACTIVATION_USER_WITH_CODE_SERVICE;
    private static JSONObject boOutput;
    static ServiceOutput serviceOutput = new ServiceOutput();

	@Operation(summary = "Confirmation User Active API",
			description = " API untuk Confirmation User Active",
			tags = { "register" })
    @RequestMapping(
            value = restUrlAddress,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ServiceOutput>> callService(
            @Parameter(
                    required=true,
                    schema=@Schema(implementation = ActivationUserWithCodeApiEntity.class))
            @Valid @RequestBody ActivationUserWithCodeApiEntity inputUser) throws JsonProcessingException {


	    Gson gson = new Gson();
        String jsonStr = gson.toJson(inputUser);
        JSONObject requestJSON =  JSONUtil.parseStringtoJSON(jsonStr);

    	boOutput = activationUserWithCode.processBo(requestJSON);
    	
//    	rabbitTemplate.convertAndSend(exchange, routingkey,requestJSON);
    	
//    	JSONObject foo =(JSONObject) rabbitTemplate.receiveAndConvert("javainuse.queue");
//    	System.out.println("as : "+foo);
    	
    	finalServiceOutput = PrepareServiceOutput.prepareServiceOutput(boOutput);

        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();
        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,HttpStatus.OK);

	}

}
