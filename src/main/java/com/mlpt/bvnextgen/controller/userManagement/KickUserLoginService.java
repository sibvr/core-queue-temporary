package com.mlpt.bvnextgen.controller.userManagement;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.LoginApiEntity;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.JSONUtil;
import com.mlpt.bvnextgen.resource.core.PrepareServiceOutput;
import com.mlpt.bvnextgen.resource.userManagement.KickUserLogin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@EnableResourceServer
@Tag(name = "User Management", description = "User Management")
public class KickUserLoginService {

	@Autowired
	KickUserLogin kickUserLogin;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.KICK_USER_LOGIN_SERVICE;
    private static JSONObject boOutput;
    static ServiceOutput serviceOutput = new ServiceOutput();

	@Operation(summary = "Kick User Login API",
			description = " API untuk auto logout ",
			tags = { "logout" })
    @RequestMapping(
            value = restUrlAddress,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ServiceOutput>> callService(
            @Parameter(
                    required=true,
                    schema=@Schema(implementation = LoginApiEntity.class))
            @Valid @RequestBody LoginApiEntity inputUser) throws JsonProcessingException {


	    Gson gson = new Gson();
        String jsonStr = gson.toJson(inputUser);
        JSONObject requestJSON =  JSONUtil.parseStringtoJSON(jsonStr);

    	boOutput = kickUserLogin.processBo(requestJSON);
    	finalServiceOutput = PrepareServiceOutput.prepareServiceOutput(boOutput);

        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();
        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,HttpStatus.OK);

	}

}
