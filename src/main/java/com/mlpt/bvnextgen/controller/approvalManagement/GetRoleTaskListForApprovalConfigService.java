package com.mlpt.bvnextgen.controller.approvalManagement;

import com.mlpt.bvnextgen.config.BoConstants;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.BoInterceptor;
import com.mlpt.bvnextgen.resource.core.CheckParameter;
import com.mlpt.bvnextgen.resource.exception.UnsupportedParameterException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableResourceServer
@Tag(name = "Approval Management", description = "Approval Management")
public class GetRoleTaskListForApprovalConfigService
{

    @Autowired
    BoInterceptor commandProcessingService;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.GET_ROLE_TASK_LIST_FOR_APPROVAL_CONFIG_SERVICE;
    static ServiceOutput serviceOutput = new ServiceOutput();


    @Operation(summary = "Get Role Task List Approval Config API",
            description = " API untuk inquiry role task list approval config ",
            security = { @SecurityRequirement(name = "bearer-key") },
            tags = { "Approval Management" })
	@PreAuthorize(" hasRole('ROLE_ADMIN') " + " || #oauth2.hasScope('read') " )
	@RequestMapping(value = restUrlAddress,
			method = RequestMethod.GET)
	 public ResponseEntity<Collection<ServiceOutput>> callService(
	         @Parameter(required = true)@RequestParam(value="taskId") Long taskId)  {

        JSONObject boInput = new JSONObject();

        if(CheckParameter.checkParameter(taskId.toString()).equals(MasterGeneralConstants.ID_PARAM)){
            boInput.put("taskId", taskId);
            finalServiceOutput = commandProcessingService.processAndLogCommand(
                    MasterGeneralConstants.READ,
                    restUrlAddress, BoConstants.GetRoleTaskListForApprovalConfig, MasterGeneralConstants.DATAPROCESS,
                    boInput);

        } else {
            throw new UnsupportedParameterException(MasterGeneralConstants.INVALID_PARAM);
        }

        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();

        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,
                HttpStatus.OK);
    }
}
