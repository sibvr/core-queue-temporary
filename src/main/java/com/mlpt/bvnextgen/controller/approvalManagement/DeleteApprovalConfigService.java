package com.mlpt.bvnextgen.controller.approvalManagement;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.BoConstants;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.DeleteEntity;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.BoInterceptor;
import com.mlpt.bvnextgen.resource.core.CheckParameter;
import com.mlpt.bvnextgen.resource.core.JSONUtil;
import com.mlpt.bvnextgen.resource.exception.UnsupportedParameterException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableResourceServer
@Tag(name = "Approval Management", description = "Approval Management")
public class DeleteApprovalConfigService {

	@Autowired
	BoInterceptor commandProcessingService;

	private static Map<BigInteger, ServiceOutput> finalServiceOutput;
	public static final String restUrlAddress = ServiceConstants.BASE_REST_URL
			+ ServiceConstants.DELETE_APPROVAL_CONFIG_SERVICE;
	static ServiceOutput serviceOutput = new ServiceOutput();
	private static JSONObject boInput = new JSONObject();

	@Operation(summary = "Delete Approval Config API", description = " API untuk menghapus approval config ", security = {
			@SecurityRequirement(name = "bearer-key") }, tags = { "Approval Management" })
//	@PreAuthorize(" hasRole('ROLE_ADMIN') " + " || #oauth2.hasScope('read') " )
	@RequestMapping(value = restUrlAddress, method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<ServiceOutput>> callService(
			@Parameter(required = true, schema = @Schema(implementation = DeleteEntity.class)) @Valid @RequestBody DeleteEntity inputUser) {

		Gson gson = new Gson();
		String jsonStr = gson.toJson(inputUser);
		JSONObject requestJSON = JSONUtil.parseStringtoJSON(jsonStr);

		finalServiceOutput = commandProcessingService.processAndLogCommand(MasterGeneralConstants.DELETE,
				restUrlAddress, BoConstants.DeleteApprovalConfig, MasterGeneralConstants.DATAPROCESS, requestJSON);

		Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();

		return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput, HttpStatus.OK);
	}
}
