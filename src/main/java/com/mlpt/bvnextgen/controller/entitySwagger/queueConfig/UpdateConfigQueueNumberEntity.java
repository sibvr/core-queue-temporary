package com.mlpt.bvnextgen.controller.entitySwagger.queueConfig;

import io.swagger.v3.oas.annotations.media.Schema;

public class UpdateConfigQueueNumberEntity {

	@Schema(required = true)
	String userId;
	
	@Schema(required = true)
	String type;
	
	@Schema(required = true)
	String statusQueue;
	
	@Schema(required = true)
	String maxQueue;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatusQueue() {
		return statusQueue;
	}

	public void setStatusQueue(String statusQueue) {
		this.statusQueue = statusQueue;
	}

	public String getMaxQueue() {
		return maxQueue;
	}

	public void setMaxQueue(String maxQueue) {
		this.maxQueue = maxQueue;
	}

	
	
	
}
