package com.mlpt.bvnextgen.controller.entitySwagger;

import io.swagger.v3.oas.annotations.media.Schema;

public class ActivationEntity {

	@Schema(required = true, example = "70", description = "id user yg digunakan login")
	Long updateUserId;

	@Schema(required = true, example = "71", description = "id yg akan di update")
	Long id;

	@Schema(required = true, example = "Y", description = "active code. Y = active, N = Deactive. input kapital")
	String status;

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
