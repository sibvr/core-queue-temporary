package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditUserManagementDetail;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public class EditUserManagementDetailEntity {

	@Schema(required = true)
	Long roleId;

	@Schema(required = true)
	Long managerId;

	@Schema(required = true)
	String username;

	@Schema(required = true)
	String email;

	@Schema(required = true)
	Long updateUserId;

	@Schema(required = true)
	Long id;

	@Schema(required = false)
	String flagNewRole;

	@Schema(required = false)
	String userFullname;

	@Schema(required = false)
	List<EditUserManagementTaskCrudActiveEntity> taskCrudActiveList;

	public Long getRoleId() {
		return roleId;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}



	public List<EditUserManagementTaskCrudActiveEntity> getTaskCrudActiveList() {
		return taskCrudActiveList;
	}

	public String getFlagNewRole() {
		return flagNewRole;
	}

	public String getUserFullname() {
		return userFullname;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public void setTaskCrudActiveList(List<EditUserManagementTaskCrudActiveEntity> taskCrudActiveList) {
		this.taskCrudActiveList = taskCrudActiveList;
	}

	public void setFlagNewRole(String flagNewRole) {
		this.flagNewRole = flagNewRole;
	}

	public void setUserFullname(String userFullname) {
		this.userFullname = userFullname;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

}
