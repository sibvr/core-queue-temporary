package com.mlpt.bvnextgen.controller.entitySwagger;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 
 * @author agus.wijanarko entity class untuk upload file
 */

public class UploadFileEntity {

	@Schema(required = true, example = "70", description = "ID user")
	private Long createUserId;

	@Schema(required = true)
	private String filename;

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	

}
