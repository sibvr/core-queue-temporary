package com.mlpt.bvnextgen.controller.entitySwagger.questionHelp;

import io.swagger.v3.oas.annotations.media.Schema;

public class DeleteQuestionHelpEntity {
	
	@Schema(required = true)
	Long questionId;
	
	@Schema(required = true)
	String userId;
	

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
}
