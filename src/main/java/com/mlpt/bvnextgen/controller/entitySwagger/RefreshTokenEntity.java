package com.mlpt.bvnextgen.controller.entitySwagger;

import io.swagger.v3.oas.annotations.media.Schema;

public class RefreshTokenEntity {

    @Schema(required=true)
    String refreshToken;

    @Schema(required=true)
    Long userId;

    public String getRefreshToken() {
        return refreshToken;
    }

    public Long getUserId() {
        return userId;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }




}
