package com.mlpt.bvnextgen.controller.entitySwagger;

import io.swagger.v3.oas.annotations.media.Schema;

public class ForgotPasswordApiEntity {
	
	@Schema(required = true)
	String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


}
