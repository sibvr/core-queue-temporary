package com.mlpt.bvnextgen.controller.entitySwagger.queueNumber;

import io.swagger.v3.oas.annotations.media.Schema;

public class GenerateQueueNumberEntity {

	@Schema(required = true)
	String email;
	
	@Schema(required = true)
	String type;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
