package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditUserManagementDetail;

import io.swagger.v3.oas.annotations.media.Schema;

public class EditUserManagementTaskCrudActiveEntity {

    @Schema(required=false)
    Long taskCrudId;

    @Schema(required=false)
    String createTaskActive;

    @Schema(required=false)
    String readTaskActive;

    @Schema(required=false)
    String updateTaskActive;

    @Schema(required=false)
    String deleteTaskActive;

    public Long getTaskCrudId() {
        return taskCrudId;
    }

    public String getCreateTaskActive() {
        return createTaskActive;
    }

    public String getReadTaskActive() {
        return readTaskActive;
    }

    public String getUpdateTaskActive() {
        return updateTaskActive;
    }

    public String getDeleteTaskActive() {
        return deleteTaskActive;
    }

    public void setTaskCrudId(Long taskCrudId) {
        this.taskCrudId = taskCrudId;
    }

    public void setCreateTaskActive(String createTaskActive) {
        this.createTaskActive = createTaskActive;
    }

    public void setReadTaskActive(String readTaskActive) {
        this.readTaskActive = readTaskActive;
    }

    public void setUpdateTaskActive(String updateTaskActive) {
        this.updateTaskActive = updateTaskActive;
    }

    public void setDeleteTaskActive(String deleteTaskActive) {
        this.deleteTaskActive = deleteTaskActive;
    }



}
