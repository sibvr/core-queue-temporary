package com.mlpt.bvnextgen.controller.entitySwagger;

import org.json.simple.JSONObject;


public class OutputEntity {


	private JSONObject serviceOutput;

	private String status;
    
	private Long count;
    
	private String errorMessage;

	public JSONObject getServiceOutput() {
		return serviceOutput;
	}

	public void setServiceOutput(JSONObject serviceOutput) {
		this.serviceOutput = serviceOutput;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}






}

