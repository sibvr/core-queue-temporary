package com.mlpt.bvnextgen.controller.entitySwagger.queueNumber;

import io.swagger.v3.oas.annotations.media.Schema;

public class ConsumeQueueNumberEntity {

	@Schema(required = true)
	String userId;
	
	@Schema(required = true)
	String type;
	
	@Schema(required = true)
	String counter;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	
	
	
}
