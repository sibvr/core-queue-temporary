package com.mlpt.bvnextgen.controller.entitySwagger;

public class InquirySumDailyTaskEntity {

	Long userId;

	Long byUserId;

	Long byTeamId;

	String byDate;

	String byStatus;

	public Long getByTeamId() {
		return byTeamId;
	}

	public void setByTeamId(Long byTeamId) {
		this.byTeamId = byTeamId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getByUserId() {
		return byUserId;
	}

	public void setByUserId(Long byUserId) {
		this.byUserId = byUserId;
	}

	public String getByDate() {
		return byDate;
	}

	public void setByDate(String byDate) {
		this.byDate = byDate;
	}

	public String getByStatus() {
		return byStatus;
	}

	public void setByStatus(String byStatus) {
		this.byStatus = byStatus;
	}


}
