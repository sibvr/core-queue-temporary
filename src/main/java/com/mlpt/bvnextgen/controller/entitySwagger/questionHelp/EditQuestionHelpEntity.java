package com.mlpt.bvnextgen.controller.entitySwagger.questionHelp;

import io.swagger.v3.oas.annotations.media.Schema;

public class EditQuestionHelpEntity {
	
	@Schema(required = true)
	Long questionId;
	
	@Schema(required = true)
	String question;
	
	@Schema(required = true)
	String queueingDestination;
	
	@Schema(required = true)
	String userId;
	
	

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQueueingDestination() {
		return queueingDestination;
	}

	public void setQueueingDestination(String queueingDestination) {
		this.queueingDestination = queueingDestination;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
}
