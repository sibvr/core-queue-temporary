package com.mlpt.bvnextgen.controller.entitySwagger.approvalManagement;

import io.swagger.v3.oas.annotations.media.Schema;

public class AddApprovalConfigEntity {

	@Schema(required = true)
	Long approvalId;

	@Schema(required = true)
	Long roleTaskIdSubmit;

	@Schema(required = true)
	Long roleTaskIdApproval;

	@Schema(required = true)
	String flagNeedApproval;

	@Schema(required = true)
	Long createUserId;

	@Schema(required = true)
	Long taskId;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getApprovalId() {
		return approvalId;
	}

	public Long getRoleTaskIdSubmit() {
		return roleTaskIdSubmit;
	}

	public Long getRoleTaskIdApproval() {
		return roleTaskIdApproval;
	}

	public String getFlagNeedApproval() {
		return flagNeedApproval;
	}

	public void setApprovalId(Long approvalId) {
		this.approvalId = approvalId;
	}

	public void setRoleTaskIdSubmit(Long roleTaskIdSubmit) {
		this.roleTaskIdSubmit = roleTaskIdSubmit;
	}

	public void setRoleTaskIdApproval(Long roleTaskIdApproval) {
		this.roleTaskIdApproval = roleTaskIdApproval;
	}

	public void setFlagNeedApproval(String flagNeedApproval) {
		this.flagNeedApproval = flagNeedApproval;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

}
