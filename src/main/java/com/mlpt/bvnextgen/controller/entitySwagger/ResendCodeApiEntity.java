package com.mlpt.bvnextgen.controller.entitySwagger;

import io.swagger.v3.oas.annotations.media.Schema;

public class ResendCodeApiEntity {
	
	@Schema(required = true)
	String email;
	
	@Schema(required = true)
	String fullname;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	
}
