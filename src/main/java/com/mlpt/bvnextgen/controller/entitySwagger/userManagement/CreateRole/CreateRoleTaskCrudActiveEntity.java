package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateRole;

import io.swagger.v3.oas.annotations.media.Schema;

public class CreateRoleTaskCrudActiveEntity {

    @Schema( required = false)
    public Long taskCrudId;

    @Schema( required = false)
    public String createTaskActive;

    @Schema( required = false)
    public String readTaskActive;

    @Schema( required = false)
    public String updateTaskActive;

    @Schema( required = false)
    public String deleteTaskActive;

    @Schema( required = false)
    public Long createTaskId;

    @Schema( required = false)
    public Long readTaskId;

    @Schema( required = false)
    public Long updateTaskId;

    @Schema( required = false)
    public Long deleteTaskId;


    public Long getTaskCrudId() {
        return taskCrudId;
    }

    public String getCreateTaskActive() {
        return createTaskActive;
    }

    public String getReadTaskActive() {
        return readTaskActive;
    }

    public String getUpdateTaskActive() {
        return updateTaskActive;
    }

    public String getDeleteTaskActive() {
        return deleteTaskActive;
    }

    public Long getCreateTaskId() {
        return createTaskId;
    }

    public Long getReadTaskId() {
        return readTaskId;
    }

    public Long getUpdateTaskId() {
        return updateTaskId;
    }

    public Long getDeleteTaskId() {
        return deleteTaskId;
    }

    public void setTaskCrudId(Long taskCrudId) {
        this.taskCrudId = taskCrudId;
    }

    public void setCreateTaskActive(String createTaskActive) {
        this.createTaskActive = createTaskActive;
    }

    public void setReadTaskActive(String readTaskActive) {
        this.readTaskActive = readTaskActive;
    }

    public void setUpdateTaskActive(String updateTaskActive) {
        this.updateTaskActive = updateTaskActive;
    }

    public void setDeleteTaskActive(String deleteTaskActive) {
        this.deleteTaskActive = deleteTaskActive;
    }

    public void setCreateTaskId(Long createTaskId) {
        this.createTaskId = createTaskId;
    }

    public void setReadTaskId(Long readTaskId) {
        this.readTaskId = readTaskId;
    }

    public void setUpdateTaskId(Long updateTaskId) {
        this.updateTaskId = updateTaskId;
    }

    public void setDeleteTaskId(Long deleteTaskId) {
        this.deleteTaskId = deleteTaskId;
    }

}
