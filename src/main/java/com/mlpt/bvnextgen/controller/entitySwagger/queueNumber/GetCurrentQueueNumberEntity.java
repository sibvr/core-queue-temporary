package com.mlpt.bvnextgen.controller.entitySwagger.queueNumber;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetCurrentQueueNumberEntity {
	
	@Schema(required = true)
	String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
