package com.mlpt.bvnextgen.controller.entitySwagger.queueConfig;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetConfigQueueNumberEntity {

	
	@Schema(required = true)
	String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
