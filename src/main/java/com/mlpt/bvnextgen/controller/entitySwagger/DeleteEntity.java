package com.mlpt.bvnextgen.controller.entitySwagger;

import io.swagger.v3.oas.annotations.media.Schema;

public class DeleteEntity {

	@Schema(required = true, example = "70", description = "id user yg digunakan login")
	Long updateUserId;

	@Schema(required = true, example = "71", description = "id yg akan di update")
	Long deleteId;

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Long getDeleteId() {
		return deleteId;
	}

	public void setDeleteId(Long deleteId) {
		this.deleteId = deleteId;
	}

}
