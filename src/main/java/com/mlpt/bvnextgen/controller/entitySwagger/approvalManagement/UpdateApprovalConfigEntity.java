package com.mlpt.bvnextgen.controller.entitySwagger.approvalManagement;

import io.swagger.v3.oas.annotations.media.Schema;

public class UpdateApprovalConfigEntity {

	@Schema(required = true)
	Long approvalConfigId;

	@Schema(required = true)
	Long roleTaskIdSubmit;

	@Schema(required = true)
	Long roleTaskIdApproval;

	@Schema(required = true)
	String flagNeedApproval;

	@Schema(required = true)
	Long updateUserId;

	public Long getApprovalConfigId() {
		return approvalConfigId;
	}

	public Long getRoleTaskIdSubmit() {
		return roleTaskIdSubmit;
	}

	public Long getRoleTaskIdApproval() {
		return roleTaskIdApproval;
	}

	public String getFlagNeedApproval() {
		return flagNeedApproval;
	}


	public void setApprovalConfigId(Long approvalConfigId) {
		this.approvalConfigId = approvalConfigId;
	}

	public void setRoleTaskIdSubmit(Long roleTaskIdSubmit) {
		this.roleTaskIdSubmit = roleTaskIdSubmit;
	}

	public void setRoleTaskIdApproval(Long roleTaskIdApproval) {
		this.roleTaskIdApproval = roleTaskIdApproval;
	}

	public void setFlagNeedApproval(String flagNeedApproval) {
		this.flagNeedApproval = flagNeedApproval;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}


}
