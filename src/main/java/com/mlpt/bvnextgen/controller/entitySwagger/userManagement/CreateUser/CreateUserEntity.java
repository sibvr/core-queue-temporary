package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateUser;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;

public class CreateUserEntity {

	@Schema(required = true)
	String username;

	@Schema(required = true)
	String password;

	@Schema(required = true)
	String email;

	@Schema(required = true)
	String flagNewRole;

	@Schema(required = true)
	Long createUserId;

	@Schema(required = true)
	String userFullname;

	@Schema(required = false)
	List<CreateUserTaskCrudActiveEntity> taskCrudActiveList;

	@Schema(required = true)
	Long roleId;

	@Schema(required = true)
	Long managerId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getFlagNewRole() {
		return flagNewRole;
	}

	public String getUserFullname() {
		return userFullname;
	}

	public List<CreateUserTaskCrudActiveEntity> getTaskCrudActiveList() {
		return taskCrudActiveList;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFlagNewRole(String flagNewRole) {
		this.flagNewRole = flagNewRole;
	}

	public void setUserFullname(String userFullname) {
		this.userFullname = userFullname;
	}

	public void setTaskCrudActiveList(List<CreateUserTaskCrudActiveEntity> taskCrudActiveList) {
		this.taskCrudActiveList = taskCrudActiveList;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	
	

}
