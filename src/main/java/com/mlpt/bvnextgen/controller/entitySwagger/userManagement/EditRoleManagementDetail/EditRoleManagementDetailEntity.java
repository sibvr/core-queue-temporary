package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditRoleManagementDetail;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;

public class EditRoleManagementDetailEntity {

	@Schema(required = true)
	Long roleId;

	@Schema(required = true)
	String roleName;

	@Schema(required = true)
	String roleCode;

	@Schema(required = true)
	Long updateUserId;

	@Schema(required = false)
	List<EditRoleManagementTaskCrudActiveEntity> taskCrudActiveList;

	public Long getRoleId() {
		return roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public List<EditRoleManagementTaskCrudActiveEntity> getTaskCrudActiveList() {
		return taskCrudActiveList;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public void setTaskCrudActiveList(List<EditRoleManagementTaskCrudActiveEntity> taskCrudActiveList) {
		this.taskCrudActiveList = taskCrudActiveList;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
	

}
