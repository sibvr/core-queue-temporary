package com.mlpt.bvnextgen.controller.entitySwagger.userManagement;

import io.swagger.v3.oas.annotations.media.Schema;

public class ChangePasswordEntity {

	@Schema(required = true)
	Long updateUserId;

	@Schema(required = true)
	String prevPassword;

	@Schema(required = true)
	String newPassword;

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getPrevPassword() {
		return prevPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setPrevPassword(String prevPassword) {
		this.prevPassword = prevPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
