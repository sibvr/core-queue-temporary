package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateRole;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;

public class CreateRoleEntity {

	@Schema(required = true)
	String flagNewRole;

	@Schema(required = true)
	Long createUserId;

	@Schema(required = false)
	String roleCode;

	@Schema(required = false)
	String roleName;

	@Schema(required = false)
	List<CreateRoleTaskCrudActiveEntity> taskCrudActiveList;

	public String getFlagNewRole() {
		return flagNewRole;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public List<CreateRoleTaskCrudActiveEntity> getTaskCrudActiveList() {
		return taskCrudActiveList;
	}

	public void setFlagNewRole(String flagNewRole) {
		this.flagNewRole = flagNewRole;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public void setTaskCrudActiveList(List<CreateRoleTaskCrudActiveEntity> taskCrudActiveList) {
		this.taskCrudActiveList = taskCrudActiveList;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

}
