package com.mlpt.bvnextgen.controller.entitySwagger.questionHelp;

import io.swagger.v3.oas.annotations.media.Schema;

public class CreateQuestionHelpEntity {

	@Schema(required = true)
	String question;
	
	@Schema(required = true)
	String queueingDestination;
	
	@Schema(required = true)
	String userId;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionDestination() {
		return queueingDestination;
	}

	public void setQuestionDestination(String queueingDestination) {
		this.queueingDestination = queueingDestination;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
}
