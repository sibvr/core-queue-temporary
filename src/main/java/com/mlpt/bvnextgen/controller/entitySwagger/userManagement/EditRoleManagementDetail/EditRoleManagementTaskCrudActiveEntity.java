package com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditRoleManagementDetail;

import io.swagger.v3.oas.annotations.media.Schema;

public class EditRoleManagementTaskCrudActiveEntity {

    @Schema(required=false)
    Long taskCrudId;

    @Schema(required=false)
    Long roleTaskCrudActiveId;

    @Schema(required=false)
    String createTaskActive;

    @Schema(required=false)
    Long createTaskId;

    @Schema(required=false)
    String readTaskActive;

    @Schema(required=false)
    Long readTaskId;

    @Schema(required=false)
    String updateTaskActive;

    @Schema(required=false)
    Long updateTaskId;

    @Schema(required=false)
    String deleteTaskActive;

    @Schema(required=false)
    Long deleteTaskId;

    public Long getTaskCrudId() {
        return taskCrudId;
    }

    public Long getRoleTaskCrudActiveId() {
        return roleTaskCrudActiveId;
    }

    public String getCreateTaskActive() {
        return createTaskActive;
    }

    public Long getCreateTaskId() {
        return createTaskId;
    }

    public String getReadTaskActive() {
        return readTaskActive;
    }

    public Long getReadTaskId() {
        return readTaskId;
    }

    public String getUpdateTaskActive() {
        return updateTaskActive;
    }

    public Long getUpdateTaskId() {
        return updateTaskId;
    }

    public String getDeleteTaskActive() {
        return deleteTaskActive;
    }

    public Long getDeleteTaskId() {
        return deleteTaskId;
    }

    public void setTaskCrudId(Long taskCrudId) {
        this.taskCrudId = taskCrudId;
    }

    public void setRoleTaskCrudActiveId(Long roleTaskCrudActiveId) {
        this.roleTaskCrudActiveId = roleTaskCrudActiveId;
    }

    public void setCreateTaskActive(String createTaskActive) {
        this.createTaskActive = createTaskActive;
    }

    public void setCreateTaskId(Long createTaskId) {
        this.createTaskId = createTaskId;
    }

    public void setReadTaskActive(String readTaskActive) {
        this.readTaskActive = readTaskActive;
    }

    public void setReadTaskId(Long readTaskId) {
        this.readTaskId = readTaskId;
    }

    public void setUpdateTaskActive(String updateTaskActive) {
        this.updateTaskActive = updateTaskActive;
    }

    public void setUpdateTaskId(Long updateTaskId) {
        this.updateTaskId = updateTaskId;
    }

    public void setDeleteTaskActive(String deleteTaskActive) {
        this.deleteTaskActive = deleteTaskActive;
    }

    public void setDeleteTaskId(Long deleteTaskId) {
        this.deleteTaskId = deleteTaskId;
    }



}
