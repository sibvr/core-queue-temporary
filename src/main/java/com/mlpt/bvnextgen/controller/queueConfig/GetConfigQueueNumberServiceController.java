package com.mlpt.bvnextgen.controller.queueConfig;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.BoInterceptor;
import com.mlpt.bvnextgen.resource.core.PrepareServiceOutput;
import com.mlpt.bvnextgen.resource.queueConfig.GetConfigQueueNumber;

@RestController
@EnableResourceServer
@Tag(name = "Queue Number", description = "Queue Number")
public class GetConfigQueueNumberServiceController
{

	@Autowired
	BoInterceptor commandProcessingService;
	
	@Autowired
	GetConfigQueueNumber getConfigQueueNumber;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.GET_CONFIG_QUEUE_NUMBER_SERVICE;
    private static JSONObject boOutput;
    static ServiceOutput serviceOutput = new ServiceOutput();

    @Operation(summary = "Get Config Queue Number API",
            description = " API untuk Get Config Queue Number",
            security = { @SecurityRequirement(name = "bearer-key") },
            tags = { "Queue Number" })
	@PreAuthorize(" hasRole('ROLE_ADMIN') " + " || #oauth2.hasScope('read') " )
	@RequestMapping(value = restUrlAddress,
			method = RequestMethod.GET)
	 public ResponseEntity<Collection<ServiceOutput>> callService(
			 @Parameter(required = true)@RequestParam(value="type") String Type) {

//        Gson gson = new Gson();
//        String jsonStr = gson.toJson(inputUser);
        JSONObject requestJSON =  new JSONObject();
        requestJSON.put("type", Type);
//        finalServiceOutput = commandProcessingService.processAndLogCommand(
//                MasterGeneralConstants.CREATE,
//                restUrlAddress, BoConstants.AddApprovalConfig, MasterGeneralConstants.DATAPROCESS,
//                requestJSON);
        
        boOutput = getConfigQueueNumber.processBo(requestJSON);
        finalServiceOutput = PrepareServiceOutput.prepareServiceOutput(boOutput);
        
        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();

        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,
                HttpStatus.OK);
    }
}
