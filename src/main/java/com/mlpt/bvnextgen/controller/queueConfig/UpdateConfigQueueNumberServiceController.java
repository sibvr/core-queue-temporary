package com.mlpt.bvnextgen.controller.queueConfig;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.ServiceConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueConfig.UpdateConfigQueueNumberEntity;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.core.BoInterceptor;
import com.mlpt.bvnextgen.resource.core.JSONUtil;
import com.mlpt.bvnextgen.resource.core.PrepareServiceOutput;
import com.mlpt.bvnextgen.resource.queueConfig.UpdateConfigQueueNumber;

@RestController
@EnableResourceServer
@Tag(name = "Queue Number", description = "Queue Number")
public class UpdateConfigQueueNumberServiceController
{

	@Autowired
	BoInterceptor commandProcessingService;
	
	@Autowired
	UpdateConfigQueueNumber updateConfigQueueNumber;

    private static Map<BigInteger, ServiceOutput> finalServiceOutput;
    public static final String restUrlAddress = ServiceConstants.BASE_REST_URL + ServiceConstants.UPDATE_CONFIG_QUEUE_NUMBER_TELLER_SERVICE;
    private static JSONObject boOutput;
    static ServiceOutput serviceOutput = new ServiceOutput();

    @Operation(summary = "Update Config Queue Number API",
            description = " API untuk Update Config Queue Number ",
            security = { @SecurityRequirement(name = "bearer-key") },
            tags = { "Queue Number" })
	@PreAuthorize(" hasRole('ROLE_ADMIN') " + " || #oauth2.hasScope('read') " )
	@RequestMapping(value = restUrlAddress,
			method = RequestMethod.PUT)
	 public ResponseEntity<Collection<ServiceOutput>> callService(
	         @Parameter(
	                 required=true,
	                 schema=@Schema(implementation = UpdateConfigQueueNumberEntity.class))
	         @Valid @RequestBody UpdateConfigQueueNumberEntity inputUser) {

        Gson gson = new Gson();
        String jsonStr = gson.toJson(inputUser);
        JSONObject requestJSON =  JSONUtil.parseStringtoJSON(jsonStr);


//        finalServiceOutput = commandProcessingService.processAndLogCommand(
//                MasterGeneralConstants.CREATE,
//                restUrlAddress, BoConstants.AddApprovalConfig, MasterGeneralConstants.DATAPROCESS,
//                requestJSON);
        
        boOutput = updateConfigQueueNumber.processBo(requestJSON);
        finalServiceOutput = PrepareServiceOutput.prepareServiceOutput(boOutput);
        
        Collection<ServiceOutput> webServiceOutput = finalServiceOutput.values();

        return new ResponseEntity<Collection<ServiceOutput>>(webServiceOutput,
                HttpStatus.OK);
    }
}
