package com.mlpt.bvnextgen.config;

/**
 * @author angga.wijaya 26.05.2020
 * Daftar nama bean BO
 *
 */
public class BoConstants {


    //bean approval bo
    public static final String AddApprovalConfig = "addApprovalConfig";
    public static final String DeleteApprovalConfig ="deleteApprovalConfig";
    public static final String GetApprovalConfigListByTaskId = "getApprovalConfigListByTaskId";
    public static final String GetRoleTaskListForApprovalConfigByRoleId = "getRoleTaskListForApprovalConfigByRoleId";
    public static final String GetRoleTaskListForApprovalConfig = "getRoleTaskListForApprovalConfig";
    public static final String GetTaskCrudListForApprovalFlow = "getTaskCrudListForApprovalFlow";
    public static final String UpdateApprovalConfig = "updateApprovalConfig";

    //bean user management
    public static final String ChangePassword = "changePassword";
    public static final String CreateRole = "createRole";
    public static final String CreateUser = "createUser";
    public static final String DeactivateUserById = "deactivateUserById";
    public static final String DeactivateRoleById = "deactivateRoleById";
    public static final String EditRoleManagementDetail = "editRoleManagementDetail";
    public static final String EditUserManagementDetail = "editUserManagementDetail";
    public static final String InquiryRoleManagement = "inquiryRoleManagement";
    public static final String InquiryRoleManagementDetail = "inquiryRoleManagementDetail";
    public static final String GetRoleByActive = "getRoleByActive";
    public static final String GetTaskCrudByActive = "getTaskCrudByActive";
    public static final String GetRoleTaskByRoleId = "getRoleTaskByRoleId";
    public static final String GetRoleTaskCrudActiveByRoleId = "getRoleTaskCrudActiveByRoleId";
    public static final String GetRoleTaskCrudActiveByActive = "getRoleTaskCrudActiveByActive";
    public static final String GetTaskIdByName = "getTaskIdByName";
    public static final String GetUserManagementByID = "getUserManagementByID";
    public static final String GetUserManagementByActiveCode = "getUserManagementByActiveCode";
    public static final String GetUserManagement = "getUserManagement";
    public static final String GetUserById = "getUserById";
    public static final String GetUserByActive = "getUserByActive";
    public static final String GetUser = "getUser";
    public static final String ActivationUserById = "activationUserById";
    
    public static final String GenerateQueueNumberTeller = "GenerateQueueNumberTeller";



}
