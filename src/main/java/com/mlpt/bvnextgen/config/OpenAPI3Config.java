package com.mlpt.bvnextgen.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author angga.wijaya
 * Configuration OpenAPI3
 */
@Configuration
public class OpenAPI3Config {

    @Bean
    public OpenAPI customOpenAPI() {
      return new OpenAPI()
             .components(new Components()
             .addSecuritySchemes("bearer-key",
             new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
   }

}
