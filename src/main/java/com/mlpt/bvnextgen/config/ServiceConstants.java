package com.mlpt.bvnextgen.config;




public class ServiceConstants {

	public static final String BASE_REST_URL  = "/bvnextgen/api/";
	public static final String BASE_PARAM_URL = "/{userId}";
	public static final String PUT_SERVICE_OUTPUT_3  = "putServiceOutputService3";
	public static final String PUT_SERVICE_OUTPUT_4  = "putServiceOutputService4";

	// user
    public static final String CHANGE_PASSWORD_SERVICE = "changePasswordService";
    public static final String ADD_USER_SERVICE = "addUserService";
    public static final String CREATE_USER_SERVICE = "createUserService";
    public static final String GET_ALL_USER_SERVICE = "getAllUserService";
    public static final String REMOVE_USER_BY_ID_SERVICE = "removeUserByIdService";
    public static final String DEACTIVATE_USER_BY_ID_SERVICE = "deactivateUserByIdService";
    public static final String DEACTIVATE_USER_SERVICE = "deactivateUserService";
    public static final String REACTIVATE_USER_SERVICE = "reactivateUserService";
    public static final String EDIT_USER_MANAGEMENT_DETAIL_SERVICE = "editUserManagementDetailService";
    public static final String GET_USER_BY_ACTIVE_SERVICE = "getUserByActiveService";
    public static final String GET_USER_MANAGEMENT_SERVICE = "getUserManagementService";
    public static final String GET_USER_SERVICE = "getUserService";

    // role
    public static final String CREATE_ROLE_SERVICE = "createRoleService";
    public static final String CREATE_ROLE_TASK_SERVICE = "createRoleTaskService";
    public static final String CREATE_ROLE_TASK_CRUD_ACTIVE_SERVICE = "createRoleTaskCrudActiveService";
    public static final String DEACTIVATE_ROLE_SERVICE = "deactivateRoleService";
    public static final String EDIT_ROLE_MANAGEMENT_DETAIL_SERVICE = "editRoleManagementDetailService";
    public static final String GET_TASK_CRUD_TEMPLATE_BY_ACTIVE_FOR_CREATE_ROLE_SERVICE = "getTaskCrudTemplateByActiveForCreateRoleService";
    public static final String GET_ROLE_TASK_CRUD_ACTIVE = "getRoleTaskCrudActiveService";
    public static final String GET_USER_LIST_BY_ROLE_ID_SERVICE = "getUserListByRoleIdService";
    public static final String GET_ROLE_MANAGEMENT_SERVICE = "getRoleManagementService";
    public static final String GET_ROLE_SERVICE = "getRoleService";
    public static final String GET_ROLE_TASK_SERVICE = "getRoleTaskService";
    public static final String GET_TASK_ID_SERVICE = "getTaskIdService";
    public static final String GET_TASK_CRUD_SERVICE = "getTaskCrudService";

	//approval flow
    public static final String GET_TASK_CRUD_LIST_APPROVAL_SERVICE = "getTaskCrudListApprovalService";
    public static final String GET_APPROVAL_CONFIG_BY_TASK_ID_SERVICE = "getApprovalConfigByTaskIdService";
    public static final String UPDATE_APPROVAL_CONFIG_SERVICE = "updateApprovalConfigService";
    public static final String ADD_APPROVAL_CONFIG_SERVICE = "addApprovalConfigService";
    public static final String DELETE_APPROVAL_CONFIG_SERVICE = "deleteApprovalConfigService";
    public static final String GET_ROLE_TASK_LIST_FOR_APPROVAL_CONFIG_SERVICE = "getRoleTaskListForApprovalConfigService";
    public static final String GET_ROLE_TASK_LIST_FOR_APPROVAL_CONFIG_BY_ROLEID_SERVICE = "getRoleTaskListForApprovalConfigByRoleIdService";

    // login
    public static final String LOGIN_SERVICE = "loginService";
    public static final String LOGIN_WITHOUT_ROLE_SERVICE = "loginWithoutRoleService";
    public static final String KICK_USER_LOGIN_SERVICE = "kickUserLoginService";
    public static final String LOGOUT_SERVICE = "logoutService";
    public static final String REFRESH_TOKEN_SERVICE = "refreshTokenService";
    public static final String CEK_USER_LOGIN_SERVICE = "cekUserLoginService";
    public static final String REGISTER_SERVICE = "registerService";
    public static final String ACTIVATION_USER_WITH_CODE_SERVICE = "activationUserWithCodeService";
    public static final String FORGOT_PASSWORD_SERVICE = "forgotPasswordService";
    public static final String RESEND_ACTIVATION_CODE_SERVICE = "resendActivationCodeService";

    //queue number
    public static final String GENERATE_QUEUE_NUMBER_TELLER_SERVICE = "generateQueueNumberTellerService";
    public static final String CONSUME_QUEUE_NUMBER_TELLER_SERVICE = "consumeQueueNumberTellerService";
    public static final String INQUIRY_QUEUE_NUMBER_TELLER_BY_EMAIL_SERVICE = "inquiryQueueNumberTellerByEmailService";
    public static final String GET_CURRENT_QUEUE_NUMBER_ACTIVE_SERVICE = "getCurrentQueueNumberActiveService";
    public static final String GET_TOTAL_QUEUE_NUMBER_HISTORY_SERVICE = "getTotalQueueNumberHistoryService";
    
    //config queue
    public static final String UPDATE_CONFIG_QUEUE_NUMBER_TELLER_SERVICE = "updateConfigQueueNumberTellerService";
    public static final String GET_CONFIG_QUEUE_NUMBER_SERVICE = "getConfigQueueNumberService";
    
    //question help
    public static final String CREATE_QUESTION_HELP_SERVICE = "createQuestionHelpService";
    public static final String GET_QUESTION_HELP_SERVICE = "getQuestionHelpService";
    public static final String EDIT_QUESTION_HELP_SERVICE = "editQuestionHelpService";
    public static final String DELETE_QUESTION_HELP_SERVICE = "deleteQuestionHelpService";
    
	//test
	public static final String TEST = "test";

}