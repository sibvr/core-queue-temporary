package com.mlpt.bvnextgen.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		//// System.out.println("OAuth2ResourceServer");
		http.
		// anonymous().disable().
				authorizeRequests()
				.antMatchers("/swagger-ui/**", "/bvnextgen/api/loginService", 
						ServiceConstants.BASE_REST_URL+"loginWithoutRoleService",
						ServiceConstants.BASE_REST_URL+"registerService",
						ServiceConstants.BASE_REST_URL+"activationUserWithCodeService",
						ServiceConstants.BASE_REST_URL+"forgotPasswordService",
						ServiceConstants.BASE_REST_URL+"resendActivationCodeService",
						"/bvnextgen/api/refreshTokenService","/bvnextgen/api/kickUserLoginService", "/bvnextgen/api/cekUserLoginService")
				.permitAll().and().authorizeRequests().antMatchers(HttpMethod.GET, "/bvnextgen/api/**")
				.access("#oauth2.hasScope('read')").antMatchers(HttpMethod.POST, "/bvnextgen/api/**")
				.access("#oauth2.hasScope('write')").antMatchers(HttpMethod.PUT, "/bvnextgen/api/**")
				.access("#oauth2.hasScope('write')").antMatchers(HttpMethod.DELETE, "/bvnextgen/api/**")
				.access("#oauth2.hasScope('trust')").and().logout().logoutUrl("/logout").deleteCookies("JSESSIONID")
				.and().csrf().disable().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
		// /

	}
}