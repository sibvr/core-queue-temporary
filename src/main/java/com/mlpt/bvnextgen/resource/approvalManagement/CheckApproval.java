package com.mlpt.bvnextgen.resource.approvalManagement;

import com.mlpt.bvnextgen.model.approvalManagement.ApprovalRequest;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalRequestDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;
import com.mlpt.bvnextgen.resource.exception.ApprovalException;
import com.mlpt.bvnextgen.resource.exception.ResourceNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author angga.wijaya
 * Class approval untuk check kebutuhan approval dan ijin generate
 *
 */
@Service
public class CheckApproval {

    @Autowired
    RoleTaskDao roleTaskDao;

    @Autowired
    ApprovalRequestDao approvalRequestDao;


    public boolean needApprovalToGenerate(JSONObject serviceInput) {

        //Validation
        List<String> validationFieldList = new ArrayList<String>();
        validationFieldList.add("taskCode");
        validationFieldList.add("roleId");
        validationFieldList.add("userId");
        validationFieldList.add("jsonDataBefore");
        validationFieldList.add("jsonDataAfter");
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

        //prepare data
        String taskCode = serviceInput.get("taskCode").toString();
        Long roleId = Long.valueOf(serviceInput.get("roleId").toString());
        Long userId = Long.valueOf(serviceInput.get("userId").toString());
        String jsonStringData = serviceInput.toString();
        String jsonDataBefore = serviceInput.get("jsonDataBefore").toString();
        String jsonDataAfter = serviceInput.get("jsonDataAfter").toString();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String datetime = formatter.format(date);

        List<String[]> flags = null;
        List<JSONObject> tempRoleTaskList = new ArrayList<JSONObject>();
        boolean status = true;

        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append(" select  A.flag_need_approval ")
            .append(" FROM t_approval_config A, m_approval B, m_task C, t_role_task D ")
            .append(" WHERE A.approval_id = B.approval_id ")
            .append(" AND B.task_id = C.task_id ")
            .append(" AND D.role_task_id = A.role_task_id_submit ")
            .append(" AND C.task_code = :taskCode AND D.role_id = :roleId ");

            Query query = roleTaskDao.createSQLQuery(queryString.toString());
            query.setParameter("taskCode", taskCode);
            query.setParameter("roleId", roleId);
            flags = query.list();

            roleTaskDao.closeSessionCreateQuery();

            // System.out.println("count flag :"+flags.size());
            if(flags.isEmpty()){
                throw  new ApprovalException(roleId.toString());

            }else{
                // System.out.println("flag[0] generate : ".concat(flags.get(0).toString()));
                if("Y".equalsIgnoreCase(flags.get(0).toString())){
                    status = true;
                }else{
                    status = false;
                }
            }

            return status;

        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Cannot find any data approval.");
        }

    }

    public boolean needApproval(JSONObject serviceInput){

        //Validation
        List<String> validationFieldList = new ArrayList<String>();
        validationFieldList.add("taskCode");
        validationFieldList.add("roleId");
        validationFieldList.add("userId");
        validationFieldList.add("jsonDataBefore");
        validationFieldList.add("jsonDataAfter");
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

        //prepare data
        String taskCode = serviceInput.get("taskCode").toString();
        Long roleId = Long.valueOf(serviceInput.get("roleId").toString());
        Long userId = Long.valueOf(serviceInput.get("userId").toString());
        String jsonStringData = serviceInput.toString();
        String jsonDataBefore = serviceInput.get("jsonDataBefore").toString();
        String jsonDataAfter = serviceInput.get("jsonDataAfter").toString();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String datetime = formatter.format(date);


        List<Object[]> result = null;
        boolean status = true;
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append(" select A.role_task_id_approval, A.flag_need_approval, C.task_id ")
            .append(" FROM t_approval_config A, m_approval B, m_task C, t_role_task D ")
            .append(" WHERE A.approval_id = B.approval_id ")
            .append(" AND B.task_id = C.task_id ")
            .append(" AND D.role_task_id = A.role_task_id_submit ")
            .append(" AND C.task_code = :taskCode AND D.role_id = :roleId ");

            Query query = roleTaskDao.createSQLQuery(queryString.toString());
            query.setParameter("taskCode", taskCode);
            query.setParameter("roleId", roleId);
            result = query.list();

            roleTaskDao.closeSessionCreateQuery();

            // System.out.println("count flag :"+result.size());
            if(result.isEmpty()){
                throw  new ApprovalException(roleId.toString());

            }else{
                // System.out.println("loop needApproval");
                Iterator iter = result.iterator();
                while(iter.hasNext()){
                    Object[] obj = (Object[]) iter.next();

                    Long roleTaskIdApproval = obj[0] != null ? Long.valueOf(obj[0].toString()) : null;
                    String flagApproval = obj[1] != null ? obj[1].toString() : null;
                    Long taskId = obj[2] != null ? Long.valueOf(obj[2].toString()) : null;

                    //need approval
                    // System.out.println("need approval : ".concat(flagApproval));
                    if(flagApproval.equalsIgnoreCase("Y")){

                        //Add to approval request
                        result = null;
                        StringBuilder queryStringDetail = new StringBuilder();
                        queryStringDetail.append(" Select role_id, task_id "
                                + "FROM t_role_task "
                                + "WHERE role_task_id = :roleTaskIdApproval ");

                        Query queryDetail = roleTaskDao.createSQLQuery(queryStringDetail.toString());
                        queryDetail.setParameter("roleTaskIdApproval", roleTaskIdApproval);
                        result = queryDetail.list();

                        roleTaskDao.closeSessionCreateQuery();

                        Iterator iterDetail = result.iterator();
                        while(iterDetail.hasNext()){
                            Object[] objDetail = (Object[]) iterDetail.next();

                            Long roleIdApproval = objDetail[0] != null ? Long.valueOf(objDetail[0].toString()) : null;

                            ApprovalRequest dataInsert = new ApprovalRequest();
                            dataInsert.setUserIdRequest(userId);
                            dataInsert.setRoleIdRequest(roleId);;
                            dataInsert.setTaskId(taskId);
                            dataInsert.setRoleIdApprove(roleIdApproval);
                            dataInsert.setJsonStringData(jsonStringData);
                            dataInsert.setRequestStatus("IN PROGRESS");
                            dataInsert.setJsonStringDataBefore(jsonDataBefore);
                            dataInsert.setJsonStringDataAfter(jsonDataAfter);
                            dataInsert.setCreateDatetime(datetime);

                            approvalRequestDao.add(dataInsert);
                        }

                        status = true;
                    }else{
                        status = false;
                    }

                }
            }
            return status;

        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Cannot find any data approval.");
        }


    }
}
