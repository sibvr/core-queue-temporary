package com.mlpt.bvnextgen.resource.approvalManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.Task;
import com.mlpt.bvnextgen.model.userManagement.TaskCategory;
import com.mlpt.bvnextgen.model.userManagement.TaskCrud;
import com.mlpt.bvnextgen.model.userManagement.TaskCrudDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.nurkhakim 14.05.2020
 * BO ini akan mengembalikan semua daftar task_crud yang aktif yang ada di database beserta
 * dengan ID, taskCode dan taskName untuk masing-masing Create, Read, Update dan Delete
 *
 */


@Service
public class GetTaskCrudListForApprovalFlow implements DataProcess{

	@Autowired
	TaskCrudDao taskCrudDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();



		//main
		try{
			List<Object[]> result = null;
			List<JSONObject> tempTaskCrudList = new ArrayList<JSONObject>();

			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT A.task_crud_id, A.task_crud_code, A.task_crud_name, ")
			.append(" A.task_category_id, B.task_category_code, B.task_category_name, ")
			.append(" A.create_task_id, COALESCE(C.task_code, '') AS create_task_code, COALESCE(C.task_name, '') AS create_task_name, C.crud_type AS create_crud_type, ")
			.append(" A.update_task_id, COALESCE(E.task_code, '') AS update_task_code, COALESCE(E.task_name, '') AS update_task_name, E.crud_type AS update_crud_type, ")
		    .append(" A.delete_task_id, COALESCE(F.task_code, '') AS delete_task_code, COALESCE(F.task_name, '') AS delete_task_name, F.crud_type AS delete_crud_type ")
			.append(" FROM ").append(TaskCrud.TABLE_NAME).append(" A ")
			.append(" INNER JOIN ").append(TaskCategory.TABLE_NAME).append(" B ON A.task_category_id = B.task_category_id ")
			.append(" LEFT JOIN ").append(Task.TABLE_NAME).append(" C ON A.create_task_id = C.task_id ")
			.append(" LEFT JOIN ").append(Task.TABLE_NAME).append(" D ON A.read_task_id = D.task_id ")
			.append(" LEFT JOIN ").append(Task.TABLE_NAME).append(" E ON A.update_task_id = E.task_id ")
			.append(" LEFT JOIN ").append(Task.TABLE_NAME).append(" F ON A.delete_task_id = F.task_id ")
			.append(" WHERE A.active = :YES ");

			Query query = taskCrudDao.createSQLQuery(queryString.toString());
			query.setParameter("YES", MasterGeneralConstants.YES);
			result = query.list();

			taskCrudDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				JSONObject tempTaskCrud = new JSONObject();

				Long taskCrudId = obj[0] != null ? Long.valueOf(obj[0].toString()) : null;
				String taskCrudCode = obj[1] != null ? obj[1].toString() : null;
				String taskCrudName = obj[2] != null ? obj[2].toString() : null;
				Long taskCtgrId = obj[3] != null ? Long.valueOf(obj[3].toString()) : null;
				String taskCtgrCode = obj[4] != null ? obj[4].toString() : null;
				String taskCtgrName = obj[5] != null ? obj[5].toString() : null;

				Long createTaskId = obj[6] != null ? Long.valueOf(obj[6].toString()) : null;
				String createTaskCode = obj[7] != null ? obj[7].toString() : null;
				String createTaskName = obj[8] != null ? obj[8].toString() : null;
				String createCrudType = obj[9] != null ? obj[9].toString() : null;

				Long updateTaskId = obj[10] != null ? Long.valueOf(obj[10].toString()) : null;
				String updateTaskCode = obj[11] != null ? obj[11].toString() : null;
				String updateTaskName = obj[12] != null ? obj[12].toString() : null;
				String updateCrudType = obj[13] != null ? obj[13].toString() : null;

				Long deleteTaskId = obj[14] != null ? Long.valueOf(obj[14].toString()) : null;
				String deleteTaskCode = obj[15] != null ? obj[15].toString() : null;
				String deleteTaskName = obj[16] != null ? obj[16].toString() : null;
				String deleteCrudType = obj[17] != null ? obj[17].toString() : null;

				tempTaskCrud.put("taskCrudId", taskCrudId);
				tempTaskCrud.put("taskCrudCode", taskCrudCode);
				tempTaskCrud.put("taskCrudName", taskCrudName);
				tempTaskCrud.put("taskCtgrId", taskCtgrId);
				tempTaskCrud.put("taskCtgrCode", taskCtgrCode);
				tempTaskCrud.put("taskCtgrName", taskCtgrName);

				tempTaskCrud.put("createTaskId", createTaskId);
				tempTaskCrud.put("createTaskCode", createTaskCode);
				tempTaskCrud.put("createTaskName", createTaskName);
				tempTaskCrud.put("createCrudType", createCrudType);

				tempTaskCrud.put("updateTaskId", updateTaskId);
				tempTaskCrud.put("updateTaskCode", updateTaskCode);
				tempTaskCrud.put("updateTaskName", updateTaskName);
				tempTaskCrud.put("updateCrudType", updateCrudType);

				tempTaskCrud.put("deleteTaskId", deleteTaskId);
				tempTaskCrud.put("deleteTaskCode", deleteTaskCode);
				tempTaskCrud.put("deleteTaskName", deleteTaskName);
				tempTaskCrud.put("deleteCrudType", deleteCrudType);


				tempTaskCrudList.add(tempTaskCrud);
			}

			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			resultOutput.put("taskCrudList", tempTaskCrudList);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);

			return resultOutput;
		}
	}



}
