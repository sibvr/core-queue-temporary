package com.mlpt.bvnextgen.resource.approvalManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.DeleteEntity;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfig;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfigDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 26.05.2020 BO ini akan menghapus salah satu approval
 *
 */

@Service
public class DeleteApprovalConfig implements DataProcess {

	@Autowired
	ApprovalConfigDao approvalConfigDao;

	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		// Validation
		Field[] fields = DeleteEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);
		
		// prepare data
		Gson gson = new Gson();
		String jsonStr = gson.toJson(serviceInput);
		DeleteEntity dataDelete = gson.fromJson(jsonStr, DeleteEntity.class);


		// main
		try {
			// cek data
			JSONObject statusCek = checkData(serviceInput);
			if (!statusCek.get("status").equals(
					MasterGeneralConstants.STATUS_SUCCESS)) {
				return statusCek;
			}

			Long approvalConfigId = dataDelete.getDeleteId();

			Query deleteApprovalConfig = approvalConfigDao
					.createQuery("DELETE " + ApprovalConfig.ENTITY_NAME
							+ " WHERE id = :id ");
			deleteApprovalConfig.setParameter("id", approvalConfigId);
			deleteApprovalConfig.executeUpdate();
			approvalConfigDao.closeSessionCreateQuery();

			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		} catch (Exception e) {
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", e.toString());

			return resultOutput;
		}
	}

	public JSONObject checkData(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();
		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(serviceInput);
		DeleteEntity dataCheck = gson.fromJson(jsonStr, DeleteEntity.class);
		
		Long approvalConfigId = dataCheck.getDeleteId();
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT COUNT(1) ").append(" FROM ")
					.append(ApprovalConfig.TABLE_NAME).append(" A ")
					.append(" WHERE A.approval_config_id = :approvalConfigId ");

			Query query = approvalConfigDao.createSQLQuery(queryString
					.toString());
			query.setParameter("approvalConfigId", approvalConfigId);
			Long countApprovalConfig = Long.valueOf(query.uniqueResult()
					.toString());

			approvalConfigDao.closeSessionCreateQuery();

			if (countApprovalConfig.equals(0L)) {
				resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				resultOutput.put("errorMessage",
						"Approval Config Does not Exist");
				return resultOutput;
			} else {
				resultOutput.put("status",MasterGeneralConstants.STATUS_SUCCESS);
				return resultOutput;
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);

			return resultOutput;
		}

	}

}
