package com.mlpt.bvnextgen.resource.approvalManagement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 14.05.2020
 * BO ini akan mengembalikan semua daftar role pada task tertentu kecuali role submit
 *
 */


@Service
public class GetRoleTaskListForApprovalConfigByRoleId implements DataProcess{


	@Autowired
	RoleTaskDao roleTaskDao;

	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		List<String> validationFieldList = new ArrayList<String>();
		validationFieldList.add("taskId");
		validationFieldList.add("roleTaskIdCheck");
		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);


		//main
		try{

			List<Object[]> result = null;
			List<JSONObject> tempRoleTaskList = new ArrayList<JSONObject>();

			Long roleTaskIdCheck = Long.valueOf(serviceInput.get("roleTaskIdCheck").toString());
			Long taskId = Long.valueOf(serviceInput.get("taskId").toString());

			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT A.role_task_id, A.role_id, B.role_code, B.role_name, A.task_id  ")
			.append(" FROM ").append(RoleTask.TABLE_NAME).append(" A ")
			.append(" INNER JOIN ").append(Role.TABLE_NAME).append(" B ON A.role_id = B.role_id ")
			.append(" WHERE task_id = :taskId AND  A.role_task_id != :roleTaskIdCheck ");

			Query query = roleTaskDao.createSQLQuery(queryString.toString());
			query.setParameter("taskId", taskId);
			query.setParameter("roleTaskIdCheck", roleTaskIdCheck);
			result = query.list();

			roleTaskDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				JSONObject tempRoleTask = new JSONObject();

				Long roteTaskId = obj[0] != null ? Long.valueOf(obj[0].toString()) : null;
				Long roleId = obj[1] != null ? Long.valueOf(obj[1].toString()) : null;
				String roleCode = obj[2] != null ? obj[2].toString() : null;
				String roleName = obj[3] != null ? obj[3].toString() : null;
				Long outputTaskId = obj[4] != null ? Long.valueOf(obj[4].toString()) : null;


				tempRoleTask.put("roteTaskId", roteTaskId);
				tempRoleTask.put("roleId", roleId);
				tempRoleTask.put("roleCode", roleCode);
				tempRoleTask.put("roleName", roleName);
				tempRoleTask.put("taskId", outputTaskId);

				tempRoleTaskList.add(tempRoleTask);
			}

			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			resultOutput.put("roleTaskList", tempRoleTaskList);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);

			return resultOutput;
		}
	}



}
