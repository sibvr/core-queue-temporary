package com.mlpt.bvnextgen.resource.approvalManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.approvalManagement.Approval;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfig;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfigDao;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.nurkhakim 14.05.2020
 * BO ini akan mengembalikan semua daftar approval berdasarkan task id
 *
 */


@Service
public class GetApprovalConfigListByTaskId implements DataProcess{

	@Autowired
	ApprovalConfigDao ApprovalConfigDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		List<String> validationFieldList = new ArrayList<String>();
		validationFieldList.add("taskId");
		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);


		//main
		try{

			List<Object[]> result = null;
			List<JSONObject> tempApprovalConfigList = new ArrayList<JSONObject>();

			Long taskId = Long.valueOf(serviceInput.get("taskId").toString());

			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT A.approval_config_id, A.approval_id, ")
			.append(" A.role_task_id_submit, COALESCE(D.role_code, '') AS role_code_submit, COALESCE(D.role_name, '') AS role_name_submit, ")
		    .append(" A.role_task_id_approval, COALESCE(F.role_code, '') AS role_code_approval, COALESCE(F.role_name, '') AS role_name_approval, ")
		    .append(" A.flag_need_approval ")
			.append(" FROM ").append(ApprovalConfig.TABLE_NAME).append(" A ")
			.append(" INNER JOIN ").append(Approval.TABLE_NAME).append(" B ON A.approval_id = B.approval_id ")
			.append(" LEFT JOIN ").append(RoleTask.TABLE_NAME).append(" C ON A.role_task_id_submit = C.role_task_id AND B.task_id = C.task_id ")
			.append(" LEFT JOIN ").append(Role.TABLE_NAME).append(" D ON C.role_id = D.role_id ")
			.append(" LEFT JOIN ").append(RoleTask.TABLE_NAME).append(" E ON A.role_task_id_approval = E.role_task_id AND B.task_id = E.task_id ")
			.append(" LEFT JOIN ").append(Role.TABLE_NAME).append(" F ON E.role_id = F.role_id ")
			.append(" WHERE A.active = :YES ")
			.append(" AND B.task_id = :taskId ");

			Query query = ApprovalConfigDao.createSQLQuery(queryString.toString());
			query.setParameter("YES", MasterGeneralConstants.YES);
			query.setParameter("taskId", taskId);
			result = query.list();

			ApprovalConfigDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				JSONObject tempApprovalConfig = new JSONObject();

				Long approvalConfigId = obj[0] != null ? Long.valueOf(obj[0].toString()) : null;
				Long approvalId = obj[1] != null ? Long.valueOf(obj[1].toString()) : null;
				Long roleTaskIdSubmit = obj[2] != null ? Long.valueOf(obj[2].toString()) : null;
				String roleCodeSubmit = obj[3] != null ? obj[3].toString() : null;
				String roleNameSubmit = obj[4] != null ? obj[4].toString() : null;
				Long roleTaskIdApproval = obj[5] != null ? Long.valueOf(obj[5].toString()) : null;
				String roleCodeApproval = obj[6] != null ? obj[6].toString() : null;
				String roleNameApproval = obj[7] != null ? obj[7].toString() : null;
				String flagNeedApproval = obj[8] != null ? obj[8].toString() : null;


				tempApprovalConfig.put("approvalConfigId", approvalConfigId);
				tempApprovalConfig.put("approvalId", approvalId);
				tempApprovalConfig.put("roleTaskIdSubmit", roleTaskIdSubmit);
				tempApprovalConfig.put("roleCodeSubmit", roleCodeSubmit);
				tempApprovalConfig.put("roleNameSubmit", roleNameSubmit);
				tempApprovalConfig.put("roleTaskIdApproval", roleTaskIdApproval);
				tempApprovalConfig.put("roleCodeApproval", roleCodeApproval);
				tempApprovalConfig.put("roleNameApproval", roleNameApproval);
				tempApprovalConfig.put("flagNeedApproval", flagNeedApproval);

				tempApprovalConfigList.add(tempApprovalConfig);
			}

			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			resultOutput.put("approvalConfigRoleTaskList", tempApprovalConfigList);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);

			return resultOutput;
		}
	}



}
