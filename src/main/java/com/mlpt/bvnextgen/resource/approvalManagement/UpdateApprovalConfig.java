package com.mlpt.bvnextgen.resource.approvalManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.approvalManagement.UpdateApprovalConfigEntity;
import com.mlpt.bvnextgen.model.approvalManagement.Approval;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfig;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfigDao;
import com.mlpt.bvnextgen.model.audittrail.Audittrail;
import com.mlpt.bvnextgen.model.audittrail.AudittrailDao;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.Task;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;
import com.mlpt.bvnextgen.resource.userManagement.DeactivateRoleById;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.nurkhakim 14.05.2020
 * BO ini akan mengedit approval yang sudah ada
 *
 */


@Service
public class UpdateApprovalConfig implements DataProcess{

	@Autowired
	ApprovalConfigDao approvalConfigDao;

	@Autowired
	AudittrailDao audittrailDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = UpdateApprovalConfigEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);


		//main
		try{

			Long approvalConfigId = Long.valueOf(serviceInput.get("approvalConfigId").toString());
			Long roleTaskIdSubmit = Long.valueOf(serviceInput.get("roleTaskIdSubmit").toString());
			Long roleTaskIdApproval = Long.valueOf(serviceInput.get("roleTaskIdApproval").toString());
			String flagNeedApproval = serviceInput.get("flagNeedApproval").toString();
			String datetime = DateUtil.getDatetimeNow();
			Long userLoginId = Long.valueOf(serviceInput.get("userLoginId").toString());


			auditTrailCustom(approvalConfigId.toString(),roleTaskIdApproval.toString(),flagNeedApproval);

			Query updateApprovalConfig = approvalConfigDao
					.createQuery(" Update "+ ApprovalConfig.ENTITY_NAME
							+ " Set roleTaskIdSubmit = :roleTaskIdSubmit, roleTaskIdApproval = :roleTaskIdApproval, flagNeedApproval = :flagNeedApproval, "
							+ " updateDatetime = :datetime, updateUserId = :userId "
							+ " where id = :id ");

			updateApprovalConfig.setParameter("id", approvalConfigId);
			updateApprovalConfig.setParameter("roleTaskIdSubmit", roleTaskIdSubmit);
			updateApprovalConfig.setParameter("roleTaskIdApproval", roleTaskIdApproval);
			updateApprovalConfig.setParameter("flagNeedApproval", flagNeedApproval);
			updateApprovalConfig.setParameter("datetime", datetime);
			updateApprovalConfig.setParameter("userId", userLoginId);

			updateApprovalConfig.executeUpdate();
			approvalConfigDao.closeSessionCreateQuery();

			JSONObject outputJson = new JSONObject();
			outputJson.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);

			return resultOutput;
		}
	}
	public String auditTrailCustom(String approvalConfigId, String roleTaskIdApproval, String flagNeedApproval ){
		try{
		List<Object[]> result = null;
		StringBuilder queryString = new StringBuilder();

		String roleNameApprovalNew = "";
		if(!roleTaskIdApproval.equals("-99")){
			queryString = new StringBuilder();
			queryString.append(" SELECT B.role_name,1 ")
			.append(" FROM ")
			.append(RoleTask.TABLE_NAME)
			.append(" A INNER JOIN ")
			.append(Role.TABLE_NAME).append(" B ON A.role_id = B.role_id ")
			.append(" WHERE A.role_task_id = :role_task_id ");

			Query query = approvalConfigDao.createSQLQuery(queryString.toString());
			query.setParameter("role_task_id", roleTaskIdApproval);
			result = query.list();

			approvalConfigDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				roleNameApprovalNew = obj[0] != null ? obj[0].toString() : null;


			}
		}


		queryString = new StringBuilder();
		queryString.append(" SELECT G.task_name, COALESCE(D.role_name, '') AS role_name_submit, ")
	    .append(" COALESCE(F.role_name, '') AS role_name_approval, ")
	    .append(" A.flag_need_approval ")
		.append(" FROM ").append(ApprovalConfig.TABLE_NAME).append(" A ")
		.append(" INNER JOIN ").append(Approval.TABLE_NAME).append(" B ON A.approval_id = B.approval_id ")
		.append(" LEFT JOIN ").append(RoleTask.TABLE_NAME).append(" C ON A.role_task_id_submit = C.role_task_id AND B.task_id = C.task_id ")
		.append(" LEFT JOIN ").append(Role.TABLE_NAME).append(" D ON C.role_id = D.role_id ")
		.append(" LEFT JOIN ").append(RoleTask.TABLE_NAME).append(" E ON A.role_task_id_approval = E.role_task_id AND B.task_id = E.task_id ")
		.append(" LEFT JOIN ").append(Role.TABLE_NAME).append(" F ON E.role_id = F.role_id ")
		.append(" LEFT JOIN ").append(Task.TABLE_NAME).append(" G ON B.task_id = G.task_id ")
		.append(" WHERE A.active = :YES ")
		.append(" AND A.approval_config_id = :approvalConfigId ");

		Query query = approvalConfigDao.createSQLQuery(queryString.toString());
		query.setParameter("YES", MasterGeneralConstants.YES);
		query.setParameter("approvalConfigId", approvalConfigId);
		result = query.list();

		approvalConfigDao.closeSessionCreateQuery();

		Iterator iter = result.iterator();
		while(iter.hasNext()){
			Object[] obj = (Object[]) iter.next();

			Audittrail audit = new Audittrail();
			audit.setPrimaryKey(obj[0] != null ? obj[0].toString() : null);
			audit.setPrimaryKeySecondary(obj[1] != null ? obj[1].toString() : null);
			audit.setFieldName("flag_need_approval");
			audit.setOriginalValue(obj[3] != null ? obj[3].toString() : null);
			audit.setNewValue(flagNeedApproval);
			audit.setUserId("test");
			audit.setApiName("UpdateApprovalConfigService");
			audit.setDatetime(DateUtil.getDatetimeNow());

			audittrailDao.add(audit);

			audit = new Audittrail();
			audit.setPrimaryKey(obj[0] != null ? obj[0].toString() : null);
			audit.setPrimaryKeySecondary(obj[1] != null ? obj[1].toString() : null);
			audit.setFieldName("role_name_approval");
			audit.setOriginalValue(obj[2] != null ? obj[2].toString() : null);
			audit.setNewValue(roleNameApprovalNew);
			audit.setUserId("test");
			audit.setApiName("UpdateApprovalConfigService");
			audit.setDatetime(DateUtil.getDatetimeNow());

			audittrailDao.add(audit);
		}

		}catch(Exception e){

			e.printStackTrace();

//			return resultOutput;
		}
		return null;

	}



}
