package com.mlpt.bvnextgen.resource.approvalManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.approvalManagement.AddApprovalConfigEntity;
import com.mlpt.bvnextgen.model.approvalManagement.Approval;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfig;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfigDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;
import com.mlpt.bvnextgen.resource.userManagement.DeactivateRoleById;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.nurkhakim 26.05.2020
 * BO ini akan menambahkan sebuah approval baru
 *
 */


@Service
public class AddApprovalConfig implements DataProcess{

	@Autowired
	ApprovalConfigDao approvalConfigDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = AddApprovalConfigEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
//			cek data
			JSONObject statusCek = checkData(serviceInput);
			if(!statusCek.get("status").equals("S")){
				return statusCek;
			}

			ApprovalConfig newApprovalConfig = new ApprovalConfig();

			Long approvalId = Long.valueOf(serviceInput.get("approvalId").toString());
			// Long taskId = Long.valueOf(serviceInput.get("taskId").toString());
			Long roleTaskIdSubmit = Long.valueOf(serviceInput.get("roleTaskIdSubmit").toString());
			Long roleTaskIdApproval = Long.valueOf(serviceInput.get("roleTaskIdApproval").toString());
			String flagNeedApproval = serviceInput.get("flagNeedApproval").toString();
			Long userLoginId = Long.valueOf(serviceInput.get("userLoginId").toString());
			String datetime = DateUtil.getDatetimeNow();

			newApprovalConfig.setApprovalId(approvalId);
			newApprovalConfig.setRoleTaskIdSubmit(roleTaskIdSubmit);
			newApprovalConfig.setRoleTaskIdApproval(roleTaskIdApproval);
			newApprovalConfig.setFlagNeedApproval(flagNeedApproval);
			newApprovalConfig.setCreateDatetime(datetime);
			newApprovalConfig.setCreateUserId(userLoginId);
			newApprovalConfig.setUpdateDatetime(datetime);
			newApprovalConfig.setCreateUserId(userLoginId);
			newApprovalConfig.setActive(MasterGeneralConstants.YES);

			approvalConfigDao.add(newApprovalConfig);


			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			resultOutput.put("status","F");
			resultOutput.put("errorMessage",e.toString());

			e.printStackTrace();

			return resultOutput;
		}
	}


	public JSONObject checkData(JSONObject serviceInput){
		JSONObject resultOutput = new JSONObject();
		Long taskId = Long.valueOf(serviceInput.get("taskId").toString());
		Long roleTaskIdSubmit = Long.valueOf(serviceInput.get("roleTaskIdSubmit").toString());
		Long roleTaskIdApproval = Long.valueOf(serviceInput.get("roleTaskIdApproval").toString());
		String flagNeedApproval = serviceInput.get("flagNeedApproval").toString();
		Long userLoginId = Long.valueOf(serviceInput.get("userLoginId").toString());
		try{
			if (flagNeedApproval.equals(MasterGeneralConstants.YES)) {
				if(roleTaskIdSubmit.equals(roleTaskIdApproval)){
					resultOutput.put("status","F");
					resultOutput.put("errorMessage","role Submit and role Approval Cannot be The Same Role");
					return resultOutput;
				}
			}

			// mendapatkan approvalId menggunakan taskId
			StringBuilder getApprovalString = new StringBuilder();
			getApprovalString.append(" SELECT approval_id ")
			.append(" FROM ").append(Approval.TABLE_NAME).append(" A ")
			.append(" WHERE A.task_id = :taskId AND A.active = :YES");

			Query getApprovalQuery = approvalConfigDao.createSQLQuery(getApprovalString.toString());
			getApprovalQuery.setParameter("taskId", taskId);
			getApprovalQuery.setParameter("YES", MasterGeneralConstants.YES);
			Long approvalId = Long.valueOf(getApprovalQuery.uniqueResult().toString());

			approvalConfigDao.closeSessionCreateQuery();

			// Validasi apakah sudah ada roleTaskSubmitId yang terdaftar dengan approvalId tersebut.
			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT COUNT(1) ")
			.append(" FROM ").append(ApprovalConfig.TABLE_NAME).append(" A ")
			.append(" WHERE A.approval_id = :approvalId AND A.role_task_id_submit = :roleTaskIdSubmit ");

			Query getApprovalConfigQuery = approvalConfigDao.createSQLQuery(queryString.toString());
			getApprovalConfigQuery.setParameter("approvalId", approvalId);
			getApprovalConfigQuery.setParameter("roleTaskIdSubmit", roleTaskIdSubmit);
			Long countApprovalConfig = Long.valueOf(getApprovalConfigQuery.uniqueResult().toString());

			approvalConfigDao.closeSessionCreateQuery();

			if(!countApprovalConfig.equals(0L)){
				resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
				resultOutput.put("errorMessage","role Submit with task already registered");
				return resultOutput;
			}else{
				resultOutput.put("status",MasterGeneralConstants.STATUS_SUCCESS);
				return resultOutput;
			}
		}catch(Exception e){
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",e.toString());

			e.printStackTrace();

			return resultOutput;
		}

	}


}
