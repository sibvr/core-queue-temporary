package com.mlpt.bvnextgen.resource.customLog;
import com.mlpt.bvnextgen.model.customLog.CustomLog;
import com.mlpt.bvnextgen.model.customLog.CustomLogDao;
import java.util.Date;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

@Service
public class LogActivity {

    @Autowired
    CustomLogDao logDao;

    private CustomLog clog = new CustomLog();

    public void initLog(String action,String uri,String commandJson){

        //get the current authentication
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2Authentication oAuth2 = ((OAuth2Authentication) authentication);

        String clientId = oAuth2.getOAuth2Request().getClientId();
        // System.out.println("currentClient : "+clientId);
        String username = oAuth2.getName();
        // System.out.println("username : "+username);


        Date requestDate = new Date();
        clog.setRequestDate(requestDate);
        clog.setActionName(action);
        clog.setClienId(clientId);
        clog.setUserId(username);
        clog.setApiGetUri(uri);
        clog.setCommandJson(commandJson);

    }

    public boolean writeLog(JSONObject responseJson) {

        try{
            // System.out.println("masuk write Log");
            clog.setResponseJson(responseJson.toJSONString());

            //current date
            Date dateResponse = new Date();
            clog.setResponseDate(dateResponse);

            //fail = 0, success = 1
            if("S".equalsIgnoreCase((String)responseJson.get("status"))){
                clog.setResultRemark(1);
            }else{
                clog.setResultRemark(0);
            }

            //Save the employee in database
            logDao.add(clog);


        }catch(Exception e){

            e.printStackTrace();
            return false;
        }

        return true;
    }


}