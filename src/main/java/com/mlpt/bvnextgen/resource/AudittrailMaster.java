package com.mlpt.bvnextgen.resource;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.audittrail.Audittrail;
import com.mlpt.bvnextgen.model.audittrail.AudittrailDao;
import com.mlpt.bvnextgen.resource.core.DateUtil;

import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AudittrailMaster {

    @Autowired
    AudittrailDao audittrailDao;

    public String updateTable(JSONObject input){
        try{

            String primaryKeyField = input.get("primaryKeyField").toString();
            String primaryKeySecondaryField = input.get("primaryKeySecondaryField").toString();
            String fieldName = input.get("fieldName").toString();
            String newValue = input.get("newValue").toString();
            String idField = input.get("idField").toString();
            String idValue = input.get("idValue").toString();
            String tableName = input.get("tableName").toString();
            String apiName = input.get("apiName").toString();


            List<Object[]> result = null;
            StringBuilder queryString = new StringBuilder();


            queryString = new StringBuilder();
            queryString.append(" SELECT ")
            .append(primaryKeyField).append(",")
            .append(primaryKeySecondaryField).append(",")
            .append(fieldName)
            .append(" FROM ").append(tableName)
            .append(" WHERE ").append(idField).append(" = :idValue ");

            // System.out.println(queryString);
            Query query = audittrailDao.createSQLQuery(queryString.toString());
//          query.setParameter("primaryKeyField", primaryKeyField);
//          query.setParameter("primaryKeySecondaryField", primaryKeySecondaryField);
//          query.setParameter("tableName", tableName);
//          query.setParameter("idField", idField);
            query.setParameter("idValue", idValue);
            result = query.list();

            audittrailDao.closeSessionCreateQuery();

            Iterator iter = result.iterator();
            while(iter.hasNext()){
                Object[] obj = (Object[]) iter.next();

                String originalValue = obj[2] != null ? obj[2].toString() : null;
                if(!newValue.equals(originalValue)){
                    Audittrail audit = new Audittrail();
                    audit.setPrimaryKey(obj[0] != null ? obj[0].toString() : null);
                    audit.setPrimaryKeySecondary(obj[1] != null ? obj[1].toString() : null);
                    audit.setFieldName(fieldName);
                    audit.setOriginalValue(originalValue);
                    audit.setNewValue(newValue);
                    audit.setUserId("test");
                    audit.setApiName(apiName);
                    audit.setDatetime(DateUtil.getDatetimeNow());

                    audittrailDao.add(audit);
                }


            }

            return MasterGeneralConstants.STATUS_SUCCESS;
        }catch(Exception e){

        	e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage=errorMessage.substring((errorMessage.indexOf(':')+1),errorMessage.length());
			errorMessage = errorMessage.trim();
            return MasterGeneralConstants.STATUS_FAILED;
//          return resultOutput;
        }

    }
    public String deleteTable(JSONObject input){
        try{

            String primaryKeyField = input.get("primaryKeyField").toString();
            String primaryKeySecondaryField = input.get("primaryKeySecondaryField").toString();
            String fieldName = input.get("fieldName").toString();
            String newValue = "";
            String idField = input.get("idField").toString();
            String idValue = input.get("idValue").toString();
            String tableName = input.get("tableName").toString();
            String apiName = input.get("apiName").toString();


            List<Object[]> result = null;
            StringBuilder queryString = new StringBuilder();


            queryString = new StringBuilder();
            queryString.append(" SELECT :primaryKeyField, :primaryKeySecondaryField, :fieldName ")
            .append(" FROM :tableName ")
            .append(" WHERE :idField = :idValue ");

            Query query = audittrailDao.createSQLQuery(queryString.toString());
            query.setParameter("primaryKeyField", primaryKeyField);
            query.setParameter("primaryKeySecondaryField", primaryKeySecondaryField);
            query.setParameter("fieldName", fieldName);
            query.setParameter("tableName", tableName);
            query.setParameter("idField", idField);
            query.setParameter("idValue", idValue);
            result = query.list();

            audittrailDao.closeSessionCreateQuery();

            Iterator iter = result.iterator();
            while(iter.hasNext()){
                Object[] obj = (Object[]) iter.next();

                Audittrail audit = new Audittrail();
                audit.setPrimaryKey(obj[0] != null ? obj[0].toString() : null);
                audit.setPrimaryKeySecondary(obj[1] != null ? obj[1].toString() : null);
                audit.setFieldName(fieldName);
                audit.setOriginalValue(obj[3] != null ? obj[3].toString() : null);
                audit.setNewValue(newValue);
                audit.setUserId("test");
                audit.setApiName(apiName);
                audit.setDatetime(DateUtil.getDatetimeNow());

                audittrailDao.add(audit);

            }

            return MasterGeneralConstants.STATUS_SUCCESS;
        }catch(Exception e){

        	e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage=errorMessage.substring((errorMessage.indexOf(':')+1),errorMessage.length());
			errorMessage = errorMessage.trim();
            return MasterGeneralConstants.STATUS_FAILED;
//          return resultOutput;
        }

    }

    public String addTable(JSONObject input){
        try{

            String primaryKeyValue = input.get("primaryKeyValue").toString();
            String primaryKeySecondaryValue = input.get("primaryKeySecondaryValue").toString();
            String fieldName = input.get("fieldName").toString();
            String newValue = input.get("newValue").toString();
            String idField = "";
            String idValue = "";
            String tableName = "";
            String apiName = input.get("apiName").toString();


            Audittrail audit = new Audittrail();
            audit.setPrimaryKey(primaryKeyValue);
            audit.setPrimaryKeySecondary(primaryKeySecondaryValue);
            audit.setFieldName(fieldName);
            audit.setOriginalValue("");
            audit.setNewValue(newValue);
            audit.setUserId("test");
            audit.setApiName(apiName);
            audit.setDatetime(DateUtil.getDatetimeNow());

            audittrailDao.add(audit);


            return MasterGeneralConstants.STATUS_SUCCESS;
        }catch(Exception e){

        	e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage=errorMessage.substring((errorMessage.indexOf(':')+1),errorMessage.length());
			errorMessage = errorMessage.trim();
            return MasterGeneralConstants.STATUS_FAILED;
//          return resultOutput;
        }

    }
}
