package com.mlpt.bvnextgen.resource.queueConfig;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueConfig.GetConfigQueueNumberEntity;
import com.mlpt.bvnextgen.model.configuration.Configuration;
import com.mlpt.bvnextgen.model.configuration.ConfigurationDao;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini untuk melihat konfigurasi antrian
 *
 */


@Service
public class GetConfigQueueNumber implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;
	
	@Autowired
	ConfigurationDao configurationDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = GetConfigQueueNumberEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			String type = serviceInput.get("type").toString();
			
			Query checkSetting = configurationDao.createQuery("Select configValue from " + Configuration.ENTITY_NAME
					+ " where configMenu = :type and subconfig = :subconfig ");
			checkSetting.setParameter("type", type.toUpperCase());
			checkSetting.setParameter("subconfig", "statusQueue");
			String statusQueue = (String) checkSetting.uniqueResult();
			
			configurationDao.closeSessionCreateQuery();
			
			Query checkQueue = configurationDao.createQuery("Select configValue from " + Configuration.ENTITY_NAME
					+ " where configMenu = :type and subconfig = :subconfig ");
			checkQueue.setParameter("type", type.toUpperCase());
			checkQueue.setParameter("subconfig", "maxQueue");
			String maxQueue = (String) checkQueue.uniqueResult();
			
			configurationDao.closeSessionCreateQuery();
			
			resultOutput.put("statusQueue", statusQueue);
			resultOutput.put("maxQueue", maxQueue);
			
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	

}
