package com.mlpt.bvnextgen.resource.queueConfig;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueConfig.UpdateConfigQueueNumberEntity;
import com.mlpt.bvnextgen.model.configuration.Configuration;
import com.mlpt.bvnextgen.model.configuration.ConfigurationDao;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini akan mengupdate konfigurasi antrian
 *
 */


@Service
public class UpdateConfigQueueNumber implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;
	
	@Autowired
	ConfigurationDao configurationDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = UpdateConfigQueueNumberEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			String userId = serviceInput.get("userId").toString();
			String type = serviceInput.get("type").toString();
			String statusQueue = serviceInput.get("statusQueue").toString().toUpperCase();
			String maxQueue = serviceInput.get("maxQueue").toString();
			
//			Query deleteData = queueNumberTellerDao.createQuery
//					("Delete from " + QueueNumberTeller.ENTITY_NAME
//					+ " where consumeStatus = :consumeStatus and queueingDate = :queueingDate"
//					+ " and type = :type and queueingNumber > :maxQueue");
//			deleteData.setParameter("consumeStatus", MasterGeneralConstants.NO);
//			deleteData.setParameter("queueingDate", formatter.format(date));
//			deleteData.setParameter("type", type);
//			deleteData.setParameter("maxQueue", Long.valueOf(maxQueue));
//			
//			deleteData.executeUpdate();
//			
//			queueNumberTellerDao.closeSessionCreateQuery();
			
			Query updateSetting = configurationDao.createQuery("Update " + Configuration.ENTITY_NAME
					+ " set configValue= :configValue where configMenu = :type and subconfig = :subconfig ");
			updateSetting.setParameter("type", type.toUpperCase());
			updateSetting.setParameter("subconfig", "statusQueue");
			updateSetting.setParameter("configValue", statusQueue);
			
			updateSetting.executeUpdate();
			
			configurationDao.closeSessionCreateQuery();
			
			updateSetting = configurationDao.createQuery("Update " + Configuration.ENTITY_NAME
					+ " set configValue = :configValue where configMenu = :type and subconfig = :subconfig ");
			updateSetting.setParameter("type", type.toUpperCase());
			updateSetting.setParameter("subconfig", "maxQueue");
			updateSetting.setParameter("configValue", maxQueue);
			
			updateSetting.executeUpdate();
			
			configurationDao.closeSessionCreateQuery();
			
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	

}
