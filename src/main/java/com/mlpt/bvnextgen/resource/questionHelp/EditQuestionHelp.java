package com.mlpt.bvnextgen.resource.questionHelp;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.questionHelp.EditQuestionHelpEntity;
import com.mlpt.bvnextgen.model.questionHelp.QuestionHelp;
import com.mlpt.bvnextgen.model.questionHelp.QuestionHelpDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 14.12.2020
 * BO ini akan mengedit pertanyaan bantuan
 *
 */


@Service
public class EditQuestionHelp implements DataProcess{

	@Autowired
	QuestionHelpDao QuestionHelpDao;
	
	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = EditQuestionHelpEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			Long questionId = Long.valueOf(serviceInput.get("questionId").toString());
			String question = serviceInput.get("question").toString();
			String queueingDestination = serviceInput.get("queueingDestination").toString();
			
			Long totalData = checkData(question,questionId);
			
			if(totalData<1){
				
				Query updateData = QuestionHelpDao.createQuery
						("update " + QuestionHelp.ENTITY_NAME
						+ " set question = :question, "
						+ " queueingDestination = :queueingDestination "
						+ " where questionId = :questionId ");
				updateData.setParameter("question", question);
				updateData.setParameter("queueingDestination", queueingDestination);
				updateData.setParameter("questionId", questionId);
				updateData.executeUpdate();
				
				QuestionHelpDao.closeSessionCreateQuery();
				
				resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			}else{
				resultOutput.put("errorMessage", "Pertanyaan sudah ada!");
				resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			}
			
			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	
	
	public Long checkData(String question, Long questionId){
		
		Query checkQueue = QuestionHelpDao.createQuery("Select count(1) from " + QuestionHelp.ENTITY_NAME
				+ " where question = :question and questionId <> :questionId");
		checkQueue.setParameter("question", question);
		checkQueue.setParameter("questionId", questionId);
		Long totalData = (Long) checkQueue.uniqueResult();
		
		QuestionHelpDao.closeSessionCreateQuery();
		
		return totalData;
	}
	
}
