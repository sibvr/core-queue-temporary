package com.mlpt.bvnextgen.resource.questionHelp;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.questionHelp.CreateQuestionHelpEntity;
import com.mlpt.bvnextgen.model.questionHelp.QuestionHelp;
import com.mlpt.bvnextgen.model.questionHelp.QuestionHelpDao;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 14.12.2020
 * BO ini untuk melihat list pertanyaan
 *
 */


@Service
public class GetQuestionHelp implements DataProcess{

	@Autowired
	QuestionHelpDao QuestionHelpDao;
	
	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = CreateQuestionHelpEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			List<JSONObject> ListResult = new ArrayList<JSONObject>();
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			Query getData = QuestionHelpDao.createQuery
					("Select questionId,question,queueingDestination from " + QuestionHelp.ENTITY_NAME
					+ " ");
			result = getData.list();
			
			QuestionHelpDao.closeSessionCreateQuery();
			
			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				Long questionId = obj[0] != null ? Long.valueOf(obj[0].toString()): null;
				String question = obj[1] != null ? obj[1].toString(): null;
				String queueingDestination = obj[2] != null ? obj[2].toString(): null;
				
				JSONObject data = new JSONObject();
				data.put("questionId", questionId);
				data.put("question", question);
				data.put("queueingDestination", queueingDestination);
				
				ListResult.add(data);
			}
			
			resultOutput.put("questionList", ListResult);
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			
			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	
}
