package com.mlpt.bvnextgen.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.resource.core.DataProcess;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini untuk melihat konfigurasi antrian
 *
 */


@Service
public class SendMailFunction implements DataProcess{

	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();
//		System.out.println("mulai");
//		Properties prop = System.getProperties();
//        prop.put("mail.smtp.auth", "true");
//        prop.put("mail.smtp.port", "587");
//        prop.put("mail.smtp.starttls.enable", "true"); //TLS
//
//        Session session = Session.getInstance(prop, null);
//        Message msg = new MimeMessage(session);
//        
//        String SMTP_SERVER = "smtp.google.com";
//		String USERNAME = "feelbettergame@gmail.com";
//		String PASSWORD = "gaming14";
//		String EMAIL_FROM = "feelbettergame@gmail.com";
//
//		//main
//		try{
//			System.out.println("masuk try");
//			String activationCode = serviceInput.get("code").toString();
//			
//			String EMAIL_SUBJECT = "Activation Code Queueing App";
//			String EMAIL_TO = serviceInput.get("emailTo").toString();
//			String EMAIL_TEXT = "<h1>Hello Java Mail \n "+activationCode+"</h1>";
//			
//			
//			msg.setFrom(new InternetAddress(EMAIL_FROM));
//
//            msg.setRecipients(Message.RecipientType.TO,
//                    InternetAddress.parse(EMAIL_TO, false));
//
//            msg.setSubject(EMAIL_SUBJECT);
//            
//            // TEXT email
//            //msg.setText(EMAIL_TEXT);
//
//            // HTML email
//            msg.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));
//
//            
//            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
//            
//            System.out.println("sebelum connect");
//            // connect
//            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
//            
//            System.out.println("setelah connect");
//            // send
//            t.sendMessage(msg, msg.getAllRecipients());
//
//            System.out.println("Response: " + t.getLastServerResponse());
//
//            t.close();
//			
//			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
//
//			return resultOutput;
//		}catch(Exception e){
//			e.printStackTrace();
//			String errorMessage = new String();
//			errorMessage = e.getCause().toString();
//			errorMessage = errorMessage.substring(
//					(errorMessage.indexOf(':') + 1), errorMessage.length());
//			errorMessage = errorMessage.trim();
//			resultOutput = new JSONObject();
//			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
//			resultOutput.put("errorMessage",errorMessage);
//
//			e.printStackTrace();
//
//			return resultOutput;
//		}
		
		// Recipient's email ID needs to be mentioned.
        String to = serviceInput.get("emailTo").toString();
        String subject = serviceInput.get("subject").toString();
        String content = serviceInput.get("content").toString();
//        String activationCode = serviceInput.get("code").toString();
//        String fullname = serviceInput.get("fullname").toString();

        // Sender's email ID needs to be mentioned
        String from = "feelbettergame@gmail.com";

        // Assuming you are sending email from through gmails smtp
        String host = "smtp.gmail.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("feelbettergame@gmail.com", "gaming14");

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setContent(content, "text/html");

            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
            resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
        } catch (MessagingException mex) {
            mex.printStackTrace();
            
            resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
        }
        
        return resultOutput;
	}
	
	
	static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }

}


