package com.mlpt.bvnextgen.resource.core;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;

public class AESUtil {
	private static byte[] key;
	private static SecretKeySpec secretKey;
	
	public static void setKey(String myKey) 
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); 
            secretKey = new SecretKeySpec(key, "AES");
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
	
	public static String encrypt(String inputString) {
        try {
        		setKey(MasterGeneralConstants.SECRET_KEY);
	  	      //Creating a Cipher object
	  	      Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	  	        
	  	      //Initializing a Cipher object
	  	      cipher.init(Cipher.ENCRYPT_MODE, secretKey);

	  	      return Base64.getEncoder().encodeToString(cipher.doFinal(inputString.getBytes("UTF-8")));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public static String decrypt(String inputString) {
        try {
        	setKey(MasterGeneralConstants.SECRET_KEY);
	  	      //Creating a Cipher object
	  	      Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	  	        
	  	      //Initializing a Cipher object
	  	      cipher.init(Cipher.DECRYPT_MODE, secretKey);
	
	  	      return new String(cipher.doFinal(Base64.getDecoder().decode(inputString)));
	    }
	    catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
