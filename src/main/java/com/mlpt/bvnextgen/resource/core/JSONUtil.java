package com.mlpt.bvnextgen.resource.core;

import com.mlpt.bvnextgen.resource.exception.ParsingFailedException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONUtil {

    public static JSONObject parseStringtoJSON(String strJson){

        try{
            // System.out.println("strJson : "+strJson);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(strJson);

            // System.out.println("after convert:"+json.toJSONString());
            return json;

        }catch(ParseException e ){
            throw new ParsingFailedException("Failed to parse JSON.");
        }

    }


}
