package com.mlpt.bvnextgen.resource.core;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import java.util.regex.Pattern;

public class CheckParameter {

    public static String checkParameter(String input) {
        // System.out.println("check parameter");

        Pattern patternDigit = Pattern.compile("-?\\d+(\\.\\d+)?");

    	 if (input == null || input.equals(" ")) {
    	        return MasterGeneralConstants.NO_PARAM;
    	 }
    	 else if (patternDigit.matcher(input).matches()){
    		 return MasterGeneralConstants.ID_PARAM;
    	 }
    	 else if (input.equals(MasterGeneralConstants.ACTIVATE) || input.equals(MasterGeneralConstants.DEACTIVATE) || input.equals(MasterGeneralConstants.DONE) ){
    		 return MasterGeneralConstants.ACTIVE_PARAM;
    	 }
    	 else {
    		 return MasterGeneralConstants.OTHER_PARAM;
    	 }

    }

}