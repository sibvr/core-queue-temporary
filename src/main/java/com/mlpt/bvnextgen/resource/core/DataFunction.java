package com.mlpt.bvnextgen.resource.core;

import org.json.simple.JSONObject;

public interface DataFunction {

	public abstract JSONObject process(JSONObject serviceInput);

}
