package com.mlpt.bvnextgen.resource.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.Cipher;

public class RSAUtil {
	public static String encryptRSA(String inputString) {
        try {
//        	String md5input = Md5Util.getMD5(inputString);
//        	String AESUtilinput = AESUtil.encrypt(md5input); 
        	Key publicKey = readFilePublicKey();
	  	      //Creating a Cipher object
	  	      Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	  	        
	  	      //Initializing a Cipher object
	  	      cipher.init(Cipher.ENCRYPT_MODE, publicKey);

	  	      return Base64.getEncoder().encodeToString(cipher.doFinal(inputString.getBytes("UTF-8")));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public static String decryptRSA(String inputString) {
        try {
        		Key privateKey = readFilePrivateKey();
	  		
	  	      //Creating a Cipher object
	  	      Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	  	        
	  	      //Initializing a Cipher object
	  	      cipher.init(Cipher.DECRYPT_MODE, privateKey);
	  		  

	  	      return new String(cipher.doFinal(Base64.getDecoder().decode(inputString)));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public static PublicKey readFilePublicKey() {
		String data = "";
		PublicKey publicKey = null;
		try {
	    	
	      File myObj = new File("D:\\ProjectJava\\wela-cessa-service\\RSA\\publicKey.txt");
	      Scanner myReader = new Scanner(myObj);
	      while (myReader.hasNextLine()) {
	    	  data+= myReader.nextLine();
	//	        // System.out.println(data);
	      }
	      myReader.close();
	      
	      X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(data.getBytes()));
          KeyFactory keyFactory = KeyFactory.getInstance("RSA");
          publicKey = keyFactory.generatePublic(keySpec);
          return publicKey;
	      
	    } catch (FileNotFoundException e) {
	      // System.out.println("An error occurred.");
	      e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return publicKey;
	}
	
	public static PrivateKey readFilePrivateKey() {
		String data = "";
		PrivateKey privateKey = null;
		try {
	    	
	      File myObj = new File("D:\\ProjectJava\\wela-cessa-service\\RSA\\privateKey.txt");
	      Scanner myReader = new Scanner(myObj);
	      while (myReader.hasNextLine()) {
	    	  data+= myReader.nextLine();
	//	        // System.out.println(data);
	      }
	      myReader.close();
	      
	      PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(data.getBytes()));
          KeyFactory keyFactory = KeyFactory.getInstance("RSA");
          privateKey = keyFactory.generatePrivate(keySpec);
          return privateKey;
	      
	    } catch (FileNotFoundException e) {
	      // System.out.println("An error occurred.");
	      e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return privateKey;
	}
}
