package com.mlpt.bvnextgen.resource.core;

import java.math.BigInteger;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.ServiceOutput;
import com.mlpt.bvnextgen.resource.approvalManagement.CheckApproval;
import com.mlpt.bvnextgen.resource.authentication.AuthenticationUser;
import com.mlpt.bvnextgen.resource.customLog.LogActivity;
import com.mlpt.bvnextgen.resource.exception.UnrecognizedBoParamException;

/**
 *
 * @author angga.wijaya Class ini sebagai interception ke Class BO
 */
@Service
public class BoInterceptor {

	@Autowired
	LogActivity logActivity;

	@Autowired
	CheckApproval checkApproval;

	@Autowired
	AuthenticationUser authenticationUser;

	private final ApplicationContext applicationContext;
	private static Map<BigInteger, ServiceOutput> output;
	private static JSONObject boOutput;
	private static JSONObject checkOutput;
	private static Long userID = new Long(0);

	@Autowired
	public BoInterceptor(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public Map<BigInteger, ServiceOutput> processAndLogCommand(String action, String url, String boName,
			String processDataMode, JSONObject serviceInput) {

		switch (action) {
		case MasterGeneralConstants.CREATE:
			//System.out.println("create");
			userID = Long.valueOf(serviceInput.get("createUserId").toString());
			checkOutput = authenticationUser.AuthenticationUser(userID);
			break;

		case MasterGeneralConstants.READ:
			//System.out.println("read");
			userID = Long.valueOf(serviceInput.get("userId").toString());
			checkOutput = authenticationUser.AuthenticationUser(userID);
			break;

		case MasterGeneralConstants.UPDATE:
			//System.out.println("Update");
			userID = Long.valueOf(serviceInput.get("updateUserId").toString());
			//System.out.println("Update = "+userID);
			checkOutput = authenticationUser.AuthenticationUser(userID);
			break;
		case MasterGeneralConstants.DELETE:
			//System.out.println("delete");
			userID = Long.valueOf(serviceInput.get("updateUserId").toString());
			checkOutput = authenticationUser.AuthenticationUser(userID);
			break;
		
		default:
			//System.out.println("else");
			checkOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			checkOutput.put("errorMessage", "action belum terdaftar");
			break;

		}
		
		if(checkOutput.get("status").equals(MasterGeneralConstants.STATUS_SUCCESS)) {
			// init log
			logActivity.initLog(action, url, serviceInput.toJSONString());

			// call BO
			//System.out.println("processDataMode : " + processDataMode);
			switch (processDataMode) {
			case "DataProcess":
				final DataProcess boProces = callBoProcess(boName);
				//System.out.println("object bo process : " + boProces);
				boOutput = boProces.processBo(serviceInput);
				break;

			case "DataTransaction":
				final DataTransaction boTransaction = callBoTransaction(boName);
				//System.out.println("object bo transatction : " + boTransaction);
				boOutput = boTransaction.process(serviceInput);
				break;

			default:
				final DataFunction boFunction = callBoFunction(boName);
				//System.out.println("object bo function : " + boFunction);
				boOutput = boFunction.process(serviceInput);
				break;

			}

			// write log
			logActivity.writeLog(boOutput);

			output = PrepareServiceOutput.prepareServiceOutput(boOutput);
		}
		
		else {
			output = PrepareServiceOutput.prepareServiceOutput(checkOutput);
		}


		return output;

	}

	public Map<BigInteger, ServiceOutput> checkApprovalProcessAndLogCommand(String action, String url, String boName,
			String processDataMode, JSONObject serviceInput) {

		// init log
		logActivity.initLog(action, url, serviceInput.toJSONString());

		// approval
		if (checkApproval.needApproval(serviceInput)) {
			boOutput.put("message", "you need approval from admin.");
			boOutput.put("status", MasterGeneralConstants.STATUS_FAILED);

		} else {
			// remove key approval
			serviceInput.remove("taskCode");
			serviceInput.remove("roleId");
			serviceInput.remove("userId");
			serviceInput.remove("jsonDataBefore");
			serviceInput.remove("jsonDataAfter");

			// call BO
			//System.out.println("processDataMode : " + processDataMode);
			switch (processDataMode) {
			case "DataProcess":
				final DataProcess boProces = callBoProcess(boName);
				//System.out.println("object bo process : " + boProces);
				boOutput = boProces.processBo(serviceInput);
				break;

			case "DataTransaction":
				final DataTransaction boTransaction = callBoTransaction(boName);
				//System.out.println("object bo transatction : " + boTransaction);
				boOutput = boTransaction.process(serviceInput);
				break;

			default:
				final DataFunction boFunction = callBoFunction(boName);
				//System.out.println("object bo function : " + boFunction);
				boOutput = boFunction.process(serviceInput);
				break;

			}
		}

		// write log
		logActivity.writeLog(boOutput);

		output = PrepareServiceOutput.prepareServiceOutput(boOutput);
		return output;

	}

	public Map<BigInteger, ServiceOutput> checkGenerateProcessAndLogCommand(String clientId, String action, String url,
			String boName, String processDataMode, JSONObject serviceInput) {

		// init log
		logActivity.initLog(action, url, serviceInput.toJSONString());

		// approval
		if (checkApproval.needApprovalToGenerate(serviceInput)) {
			boOutput.put("message", "you can't generate this process");
			boOutput.put("status", MasterGeneralConstants.STATUS_FAILED);

		} else {

			// remove key approval
			serviceInput.remove("taskCode");
			serviceInput.remove("roleId");
			serviceInput.remove("userId");
			serviceInput.remove("jsonDataBefore");
			serviceInput.remove("jsonDataAfter");

			// call BO
			//System.out.println("processDataMode : " + processDataMode);
			switch (processDataMode) {
			case "DataProcess":
				final DataProcess boProces = callBoProcess(boName);
				//System.out.println("object bo process : " + boProces);
				boOutput = boProces.processBo(serviceInput);
				break;

			case "DataTransaction":
				final DataTransaction boTransaction = callBoTransaction(boName);
				//System.out.println("object bo transatction : " + boTransaction);
				boOutput = boTransaction.process(serviceInput);
				break;

			default:
				final DataFunction boFunction = callBoFunction(boName);
				//System.out.println("object bo function : " + boFunction);
				boOutput = boFunction.process(serviceInput);
				break;

			}
		}

		// write log
		logActivity.writeLog(boOutput);

		output = PrepareServiceOutput.prepareServiceOutput(boOutput);
		return output;

	}

	// -----------------------------BO SELECTOR-----------------------------------
	private DataProcess callBoProcess(String beanName) {
		//System.out.println("--data process--");
		try {
			DataProcess bo = this.applicationContext.getBean(beanName, DataProcess.class);
			return bo;

		} catch (NoSuchBeanDefinitionException e) {
			e.printStackTrace();
			throw new UnrecognizedBoParamException(beanName);
		}

	}

	private DataTransaction callBoTransaction(String beanName) {
		//System.out.println("--data transaction--");
		try {
			DataTransaction bo = this.applicationContext.getBean(beanName, DataTransaction.class);
			return bo;

		} catch (NoSuchBeanDefinitionException e) {
			e.printStackTrace();
			throw new UnrecognizedBoParamException(beanName);
		}

	}

	private DataFunction callBoFunction(String beanName) {
		//System.out.println("--data function--");
		try {
			DataFunction bo = this.applicationContext.getBean(beanName, DataFunction.class);
			return bo;

		} catch (NoSuchBeanDefinitionException e) {
			e.printStackTrace();
			throw new UnrecognizedBoParamException(beanName);
		}
	}

}
