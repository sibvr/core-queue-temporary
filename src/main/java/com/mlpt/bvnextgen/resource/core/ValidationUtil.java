package com.mlpt.bvnextgen.resource.core;

import com.mlpt.bvnextgen.resource.exception.InvalidParameterException;
import com.mlpt.bvnextgen.resource.exception.UnsupportedParameterException;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

public class ValidationUtil {

	// ================= Unsupport Parameter Checker===================
	private static List<String> unsupportedParameterList;

	// unsuport parameter Checker
	public static String checkForUnsupportedParameters(final JSONObject jsonInput,
			final List<String> supportedParams) {

		String errMsg = "";

		// null json
		if (jsonInput == null || jsonInput.isEmpty()) {
			errMsg = "JSON Must Contain Keys ".concat(supportedParams.toString()).concat(" and Cannot Be Empty");
		}

		// check json key
		List<String> keysJson = new ArrayList<String>(jsonInput.keySet());
		List<String> listParam = new ArrayList<String>(supportedParams);

		listParam.removeAll(keysJson);
		unsupportedParameterList = listParam;

		// any unsupport parameter?
		if (!unsupportedParameterList.isEmpty()) {
			errMsg = "JSON Must Contain Keys " + unsupportedParameterList.toString() + " and Cannot Be Empty";
		}

		return errMsg;
	}

	// unsupport Nested Param
	public static void checkForUnsupportedNestedParameters(final String parentPropertyName, final JSONObject jsonInput,
			final List<String> supportedParams) {

		try {
			checkForUnsupportedParameters(jsonInput, supportedParams);
		} catch (UnsupportedParameterException upex) {

			List<String> updatedUnsupportedParameters = new ArrayList<>();

			for (String unsupportedParameter : unsupportedParameterList) {
				String updatedUnsupportedParameter = parentPropertyName + "." + unsupportedParameter;
				updatedUnsupportedParameters.add(updatedUnsupportedParameter);
			}

			throw new UnsupportedParameterException(updatedUnsupportedParameters.toString());

		} catch (InvalidParameterException ipex) {

			List<String> keyJsonwithParent = new ArrayList<String>();
			for (int i = 0; i < supportedParams.size(); i++) {
				keyJsonwithParent.add(parentPropertyName.concat(".").concat(supportedParams.get(i)));
			}

			throw new InvalidParameterException(
					"JSON Must Contain Keys ".concat(keyJsonwithParent.toString()).concat(" and Cannot Be Empty"));

		}

	}

	// ================= Blank/Null Checker ===========================
	public static String valBlankOrNullList(JSONObject inputJson, List<String> keyIndex) {

		String errMsg = "";

		List<String> listField = new ArrayList<String>();
		for (int i = 0; i < keyIndex.size(); i++) {


			if (!inputJson.containsKey(keyIndex.get(i)) || String.valueOf(inputJson.get(keyIndex.get(i))).isEmpty()) {
				listField.add(keyIndex.get(i));
			}
		}

		if (!listField.isEmpty()) {
			errMsg = "Field ".concat(listField.toString()).concat(" cannot be empty");
		}

		return errMsg;

	}

	// =================cek parameter and val Blank/Null Checker
	// ===========================
	public static String cekParamAndValBlankOrNullList(JSONObject inputJson, List<String> fieldList) {

		String errMsg = "";

		errMsg = ValidationUtil.checkForUnsupportedParameters(inputJson, fieldList);
		if (errMsg.isEmpty()) {
			errMsg = ValidationUtil.valBlankOrNullList(inputJson, fieldList);
		}

		return errMsg;

	}

}
