package com.mlpt.bvnextgen.resource.core;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.mlpt.bvnextgen.model.ServiceOutput;

public class PrepareServiceOutput {

	private static Map<BigInteger, ServiceOutput> finalServiceOutput;
	static ServiceOutput serviceOutput = new ServiceOutput();

	public static Map<BigInteger, ServiceOutput> prepareServiceOutput(JSONObject serviceInput) {
	
		serviceOutput.setServiceContent(serviceInput);
		
		serviceOutput.setTransactionStatus((String) serviceInput.get("transactionStatus"));
		
        if (finalServiceOutput == null) {
        	finalServiceOutput = new HashMap<BigInteger, ServiceOutput>();
        }
        
        serviceOutput.setId(null);
        finalServiceOutput.put(serviceOutput.getId(), serviceOutput);
        // System.out.println("response to web : " + finalServiceOutput);
        return finalServiceOutput;
    }
	
}
