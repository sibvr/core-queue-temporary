package com.mlpt.bvnextgen.resource.core;

import org.json.simple.JSONObject;

public interface DataProcess {

	public abstract JSONObject processBo(JSONObject serviceInput);

}
