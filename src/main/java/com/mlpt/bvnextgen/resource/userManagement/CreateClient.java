package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.Client;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko
 * BO untuk create Client
 *
 */

@Service
public class CreateClient implements DataProcess {

	@Autowired
	ClientDao clientDao;

	@Override
    @SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		System.err.println("create client");

		//Validation
        List<String> validationFieldList = new ArrayList<String>();
		validationFieldList.add("clientId");
		validationFieldList.add("clientSecret");
		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
	    ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		// prepare data
		String clientId = String.valueOf(serviceInput.get("clientId"));
		String clientSecret = String.valueOf(serviceInput.get("clientSecret"));

		Long accessTokenValidity = new Long(30000);
		Long refreshTokenValidity = new Long(90000);

		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();

		try {
			Client newClient = new Client();

			newClient.setClientId(clientId);
			newClient.setClientSecret("{noop}"+clientSecret);
			newClient.setScope("read,write,trust");
			newClient.setAuthorizedGrantTypes("authorization_code,check_token,refresh_token,password");
			newClient.setAccessTokenValidity(accessTokenValidity);
			newClient.setRefreshTokenValidity(refreshTokenValidity);
			clientDao.add(newClient);
		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage=errorMessage.substring((errorMessage.indexOf(':')+1),errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}

		return jsonOutput;
	}

}
