package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.RefreshTokenEntity;
import com.mlpt.bvnextgen.model.userManagement.LoginUser;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.Md5Util;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko bo ini digunakan untuk auto logoutketika user masih
 *         login di browser lain
 *
 */

@Service
public class CekUserLogin implements DataProcess {

	@Autowired
	GetUserById getUserById;

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput = new JSONObject();

		JSONObject inputBO = new JSONObject();

		Gson gson = new Gson();
		String jsonStr = "";

		Field[] fields = RefreshTokenEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		jsonStr = gson.toJson(serviceInput);
		RefreshTokenEntity data = gson.fromJson(jsonStr, RefreshTokenEntity.class);
		String refreshToken = data.getRefreshToken();
		Long userId = data.getUserId();
		

		try {

			inputBO.put("userId", userId);
			inputBO.put("parameter", userId);
			JSONObject resultOutput = getUserById.processBo(inputBO);
			if (resultOutput.get("status").equals(MasterGeneralConstants.STATUS_SUCCESS)) {

				jsonStr = gson.toJson(resultOutput.get("serviceOutput"));
				UserEntity user = gson.fromJson(jsonStr, UserEntity.class);
				String refreshTokenResult = user.getStatusLogin();
				
				if (refreshTokenResult.equals(refreshToken)) {
					jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
				} else {
					jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
					jsonOutput.put("errorMessage", "invalid refresh token");

				}
			}

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);

		}
		return jsonOutput;

	}

}
