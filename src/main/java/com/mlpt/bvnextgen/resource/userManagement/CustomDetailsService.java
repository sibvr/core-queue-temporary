package com.mlpt.bvnextgen.resource.userManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.model.userManagement.LoginUser;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;

/**
 * 
 * @author agus.wijanarko convert user detail dari db ke format user oauth2
 */

@Service
public class CustomDetailsService implements UserDetailsService {
	@Autowired
	GetUserbyUsername login;

	@Override
	public CustomUser loadUserByUsername(final String username) {
		LoginUser loginUser = null;
		try {
			loginUser = login.getUserDetails(username);
			CustomUser customUser = new CustomUser(loginUser);
			return customUser;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsernameNotFoundException("User " + username
					+ " tidak terdaftar");
		}
	}
}