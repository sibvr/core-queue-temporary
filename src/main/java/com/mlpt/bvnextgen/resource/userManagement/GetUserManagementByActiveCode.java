package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.model.userManagement.UserRoleDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get user managemnent by active code
 */

@Service
public class GetUserManagementByActiveCode implements DataProcess {

	@Autowired
	RoleDao roleDao;

	@Autowired
	UserDao userDao;

	@Autowired
	UserRoleDao userRoleDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String status = String.valueOf(serviceInput.get("parameter"));
		Long userId = new Long(0);
		Long roleId = new Long(0);
		Long managerId = new Long(0);
		String managerName = new String();
		String usernameResult = new String();
		String userFullnameResult = new String();
		String email = new String();
		String role = new String();

		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();
		JSONObject jsonOutput = new JSONObject();

		StringBuilder queryStr = new StringBuilder();
		queryStr.append(
				" SELECT A.id, A.username, B.roleName, A.userFullname, A.active, A.email, A.userRole, A.managerId, ")
					.append(" ( SELECT userFullname from  ")
					.append(UserEntity.ENTITY_NAME)
					.append(" WHERE id = A.managerId ) ").append(" FROM ")
				.append(UserEntity.ENTITY_NAME).append(" A, ")
				.append(Role.ENTITY_NAME).append(" B ")
				.append(" WHERE A.userRole = B.id AND B.active = :YES ")
				.append(" AND A.userRole is not null AND A.active = :status");

		try {
			Query query = userDao.createQuery(queryStr.toString());
			query.setParameter("YES", MasterGeneralConstants.YES);
			query.setParameter("status", status);
			result = query.list();
			userDao.closeSessionCreateQuery();
			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();

				userId = null != obj[0] ? Long.valueOf(obj[0].toString())
						: null;
				usernameResult = null != obj[1] ? obj[1].toString() : null;
				role = null != obj[2] ? obj[2].toString() : null;
				userFullnameResult = null != obj[3] ? obj[3].toString() : null;
				status = null != obj[4] ? obj[4].toString() : null;
				email = null != obj[5] ? obj[5].toString() : null;
				roleId = null != obj[6] ? Long.valueOf(obj[6].toString())
						: null;
				managerId = null != obj[7] ? Long.valueOf(obj[7].toString())
						: null;
				managerName = null != obj[8] ? obj[8].toString() : null;

				JSONObject tempJO = new JSONObject();
				tempJO.put("userId", userId);
				tempJO.put("usernameResult", usernameResult);
				tempJO.put("role", role);
				tempJO.put("userFullnameResult", userFullnameResult);
				tempJO.put("activeCode", status);
				tempJO.put("email", email);
				tempJO.put("roleId", roleId);
				tempJO.put("managerId", managerId);
				tempJO.put("managerName", managerName);
				outputBo.add(tempJO);
			}

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("serviceOutput", outputBo);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
