package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.LoginApiEntity;
import com.mlpt.bvnextgen.model.userManagement.LoginUser;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.Md5Util;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko bo ini digunakan untuk auto logoutketika user masih
 *         login di browser lain
 *
 */

@Service
public class KickUserLogin implements DataProcess {

	@Autowired
	EditUserLoginStatus editUserLoginStatus;

	@Autowired
	GetUserbyUsername getUserbyUsername;

	@Autowired
	DataSource ds;

	@Bean
	public TokenStore tokenStore3() {
		return new JdbcTokenStore(ds);
	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput = new JSONObject();

		Gson gson = new Gson();
		String jsonStr = "";

		Field[] fields = LoginApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		jsonStr = gson.toJson(serviceInput);
		LoginApiEntity data = gson.fromJson(jsonStr, LoginApiEntity.class);
		String username = data.getUsername();
		String password = data.getPassword();

		try {

			LoginUser loginUser = getUserbyUsername.getUserDetails(username);
			String usernameResult = loginUser.getUsername();
			String passwordResult = loginUser.getPassword();

			if (username.equals(usernameResult) && Md5Util.getMD5(password).equals(passwordResult)) {

				Collection<OAuth2AccessToken> accessToken = tokenStore3().findTokensByClientId(username);

				//// System.out.println("masuk " + accessToken.size());

				for (OAuth2AccessToken token : accessToken) {
					tokenStore3().removeAccessToken(token);

					// remove refresh token
					OAuth2RefreshToken refreshToken = token.getRefreshToken();
					tokenStore3().removeRefreshToken(refreshToken);
				}

				// update status login
				jsonInput.put("username", username);
				jsonInput.put("randomString", MasterGeneralConstants.NO);
				editUserLoginStatus.processBo(jsonInput);

				jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			}

			else {
				jsonOutput = new JSONObject();
				jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				jsonOutput.put("errorMessage", "username atau password salah");

			}

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);

		}
		return jsonOutput;

	}

}
