package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get role task crud by active code
 */

@Service
public class GetRoleTaskCrudActiveByActive implements DataProcess {

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String status = String.valueOf(serviceInput.get("parameter"));
		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();
		JSONObject jsonOutput = new JSONObject();

		try {
			Query query = roleTaskCrudActiveDao
					.createQuery("SELECT e.id , e.roleId , e.taskCrudId FROM "
							+ RoleTaskCrudActive.ENTITY_NAME
							+ " e WHERE e.active = :status");
			query.setParameter("status", status);
			result = query.list();
			roleTaskCrudActiveDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();
				Long id = obj[0] != null ? Long.valueOf(obj[0].toString())
						: null;
				String roleCode = obj[1] != null ? obj[1].toString() : null;
				String roleName = obj[2] != null ? obj[2].toString() : null;
				tempJO.put("userId", id);
				tempJO.put("roleCode", roleCode);
				tempJO.put("roleName", roleName);
				outputBo.add(tempJO);
			}

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("serviceOutput", outputBo);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
