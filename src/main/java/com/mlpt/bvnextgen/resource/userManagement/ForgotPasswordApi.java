package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ForgotPasswordApiEntity;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.SendMailFunction;
import com.mlpt.bvnextgen.resource.core.AESUtil;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim digunakan untuk forgot password
 */

@Service
public class ForgotPasswordApi implements DataProcess {

	@Autowired
	UserDao userDao;
	
	@Autowired
	ClientDao clientDao;
	
	@Autowired
	SendMailFunction sendMailFunction;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {
		// System.out.println("login");

		// Validation
		ForgotPasswordApiEntity inputUser = new ForgotPasswordApiEntity();
		Field[] fields = ForgotPasswordApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String email = String.valueOf(serviceInput.get("email"));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject resultOutput = new JSONObject();

		List<Object> resultList = new ArrayList<Object>();
		try {
			
			String datetime = DateUtil.getDatetimeNow();
			
			Query checkPassword = userDao.createQuery("Select password, userFullname from " + UserEntity.ENTITY_NAME
					+ " where email = :email and active =:active and confirmationCode is not null");
			checkPassword.setParameter("email", email);
			checkPassword.setParameter("active", MasterGeneralConstants.YES);
			resultList = checkPassword.list();
			
			userDao.closeSessionCreateQuery();
			
			Iterator iter = resultList.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				
				String password = null != obj[0] ? obj[0].toString()
						: null;
				String userFullname = null != obj[1] ? obj[1].toString()
						: null;
				
				password = AESUtil.decrypt(password);
				
				JSONObject inputMail = new JSONObject();
				inputMail.put("emailTo", email);
				inputMail.put("subject", "Password Code Queueing App");
				inputMail.put("content", "<h1>Dear "+userFullname+",</h1> "
	            		+ "<br/>"
	            		+ "<p>Berikut adalah password anda untuk aplikasi antrian bank :</p>"
	            		+ "<h2><i>"+password+"</i></h2>"
	            		+ "<p>Mohon untuk merahasiakan password ini demi keamanan data anda</p>");
				
				
				sendMailFunction.processBo(inputMail);
			}
			
			
			
			resultOutput.put("status","S");
			
    		return resultOutput;
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);
		}

		return resultOutput;

	}


}
