package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.TaskCrud;
import com.mlpt.bvnextgen.model.userManagement.TaskCrudDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get task crud by active code
 */

@Service
public class GetTaskCrudByActive implements DataProcess {

	@Autowired
	TaskCrudDao taskCrudDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String status = String.valueOf(serviceInput.get("parameter"));

		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();
		JSONObject jsonOutput = new JSONObject();

		try {
			StringBuilder sb = new StringBuilder();
			sb.append(
					"SELECT e.id AS task_crud_id, e.taskCrudName AS task_crud_name, ")
					.append("'N' AS create_task_active, 'N' AS read_task_active, ")
					.append("'N' AS update_task_active, 'N' AS delete_task_active, ")
					.append("e.createTaskId, e.readTaskId, ")
					.append("e.updateTaskId, e.deleteTaskId ").append("FROM ")
					.append(TaskCrud.ENTITY_NAME).append(" e ")
					.append("WHERE e.active = :status ");

			Query query = taskCrudDao.createQuery(sb.toString());
			query.setParameter("status", status);
			result = query.list();
			taskCrudDao.closeSessionCreateQuery();
			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();
				Long taskCrudId = null != obj[0] ? Long.valueOf(obj[0]
						.toString()) : null;
				String taskCrudName = null != obj[1] ? obj[1].toString() : null;
				String createTaskActive = null != obj[2] ? obj[2].toString()
						: null;
				String readTaskActive = null != obj[3] ? obj[3].toString()
						: null;
				String updateTaskActive = null != obj[4] ? obj[4].toString()
						: null;
				String deleteTaskActive = null != obj[5] ? obj[5].toString()
						: null;
				Long createTaskId = null != obj[6] ? Long.valueOf(obj[6]
						.toString()) : null;
				Long readTaskId = null != obj[7] ? Long.valueOf(obj[7]
						.toString()) : null;
				Long updateTaskId = null != obj[8] ? Long.valueOf(obj[8]
						.toString()) : null;
				Long deleteTaskId = null != obj[9] ? Long.valueOf(obj[9]
						.toString()) : null;

				tempJO.put("taskCrudId", taskCrudId);
				tempJO.put("taskCrudName", taskCrudName);
				tempJO.put("createTaskActive", createTaskActive);
				tempJO.put("readTaskActive", readTaskActive);
				tempJO.put("updateTaskActive", updateTaskActive);
				tempJO.put("deleteTaskActive", deleteTaskActive);
				tempJO.put("createTaskId", createTaskId);
				tempJO.put("readTaskId", readTaskId);
				tempJO.put("updateTaskId", updateTaskId);
				tempJO.put("deleteTaskId", deleteTaskId);
				outputBo.add(tempJO);
			}

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			jsonOutput.put("serviceOutput", outputBo);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
