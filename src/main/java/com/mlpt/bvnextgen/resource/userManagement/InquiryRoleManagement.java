package com.mlpt.bvnextgen.resource.userManagement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;

/**
 * 
 * @author agus.wijanarko
 * bo untuk inquiry role management
 */

@Service
public class InquiryRoleManagement  implements DataProcess {

	@Autowired
	RoleDao roleDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();

		// prepare data
		String activeCode = MasterGeneralConstants.YES;

		JSONObject jsonOutput = new JSONObject();
		
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT e.id AS role_id, e.roleCode AS role_code, e.roleName AS role_name, ")
			.append( "e.createDatetime AS create_datetime, u.username AS create_user_id ")
			.append(" FROM ")
			.append(Role.ENTITY_NAME)
			.append(" e ")
			.append(",")
			.append(UserEntity.ENTITY_NAME)
			.append(" u ")
			.append(" WHERE e.active = :active AND u.id = e.createUserId ");
			
			Query query = roleDao.createQuery(sb.toString());
			query.setParameter("active", activeCode);
			result = query.list();
			roleDao.closeSessionCreateQuery();
			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();
				Long id = (Long) obj[0];
				String roleCode = (String) obj[1];
				String roleName = (String) obj[2];
				String createDatetime = (String) obj[3];
				String createUsername = (String) obj[4];
				
				tempJO.put("roleId", id);
				tempJO.put("roleCode", roleCode);
				tempJO.put("roleName", roleName);
				tempJO.put("createDatetime", createDatetime);
				tempJO.put("createUsername", createUsername);
				
				outputBo.add(tempJO);
			}
			
			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status",MasterGeneralConstants.STATUS_SUCCESS);
			jsonOutput.put("serviceOutput", outputBo);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
