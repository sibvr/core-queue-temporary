package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.LogAPIAccessByUserDao;
import com.mlpt.bvnextgen.model.userManagement.LogUserAccessDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditUserLoginStatus {

	@Autowired
	UserDao userDao;

	@Autowired
	LogUserAccessDao logUserAccessDao;

	@Autowired
	LogAPIAccessByUserDao logAPIAccessByUserDao;

	private Session session;

	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

	    //Validation
        List<String> validationFieldList = new ArrayList<String>();
        validationFieldList.add("randomString");
        validationFieldList.add("username");
        ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		// prepare data
		String username = String.valueOf(serviceInput.get("username"));
		String randomString = String.valueOf(serviceInput.get("randomString"));

		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();

		try {
			Query query = userDao
					.createQuery("UPDATE "
							+ UserEntity.ENTITY_NAME
							+ " e SET e.statusLogin = :randomString WHERE e.username = :username");
			query.setParameter("username", username);
			query.setParameter("randomString", randomString);
			query.executeUpdate();

			userDao.closeSessionCreateQuery();

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("serviceOutput", "OK");
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
