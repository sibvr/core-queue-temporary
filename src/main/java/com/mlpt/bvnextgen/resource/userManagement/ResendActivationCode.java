package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ResendCodeApiEntity;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.SendMailFunction;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim digunakan untuk Resend Activation Code
 */

@Service
public class ResendActivationCode implements DataProcess {

	@Autowired
	UserDao userDao;
	
	@Autowired
	ClientDao clientDao;
	
	@Autowired
	SendMailFunction sendMailFunction;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {
		// System.out.println("login");

		// Validation
		ResendCodeApiEntity inputUser = new ResendCodeApiEntity();
		Field[] fields = ResendCodeApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String email = String.valueOf(serviceInput.get("email"));
		String fullname = String.valueOf(serviceInput.get("fullname"));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject resultOutput = new JSONObject();

		List<Object> resultList = new ArrayList<Object>();
		try {
			
			String datetime = DateUtil.getDatetimeNow();
			Random rn = new Random();
			String activationCode = String.format("%04d", rn.nextInt(10000));
			
			Query updateCode = userDao.createQuery("Update " + UserEntity.ENTITY_NAME
					+ " SET confirmationCode = :confirmationCode where email = :email and active =:active and confirmationCode is not null");
			updateCode.setParameter("email", email);
			updateCode.setParameter("confirmationCode", activationCode);
			updateCode.setParameter("active", MasterGeneralConstants.NO);
			
			updateCode.executeUpdate();
			
			userDao.closeSessionCreateQuery();
			
				
			JSONObject inputMail = new JSONObject();
			inputMail.put("emailTo", email);
			inputMail.put("subject", "Activation Code Queueing App");
			inputMail.put("content", "<h1>Dear "+fullname+",</h1> "
            		+ "<br/>"
            		+ "<p>Berikut adalah kode konfirmasi anda untuk aplikasi antrian bank :</p>"
            		+ "<h2>"+activationCode+"</h2>"
            		+ "<p>Silahkan masukan kode tersebut pada aplikasi untuk melanjutkan registrasi anda</p>");
			
			
			sendMailFunction.processBo(inputMail);
			
			
			
			resultOutput.put("status","S");
			
    		return resultOutput;
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);
		}

		return resultOutput;

	}


}
