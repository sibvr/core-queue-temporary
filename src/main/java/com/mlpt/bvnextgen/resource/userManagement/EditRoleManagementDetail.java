package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditRoleManagementDetail.EditRoleManagementDetailEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserRoleDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko
 * edit role managament
 */

@Service
public class EditRoleManagementDetail implements DataProcess {

	@Autowired
	UserDao userDao;

	@Autowired
	UserRoleDao userRoleDao;

	@Autowired
	RoleDao roleDao;

	@Autowired
	RoleTaskDao roleTaskDao;

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Override
    @SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

	    //Validation
		Field[] fields = EditRoleManagementDetailEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}
        ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);


		Long roleId = Long.valueOf(String.valueOf( serviceInput.get("roleId")));
		String roleName = String.valueOf(serviceInput.get("roleName"));
		String roleCode = String.valueOf(serviceInput.get("roleCode"));
		Long updateUserId = Long.valueOf(String.valueOf( serviceInput.get("userId")));
		String datetime = DateUtil.getDatetimeNow();
		List<JSONObject> taskCrudActiveList = new ArrayList<JSONObject>();
		taskCrudActiveList = (List<JSONObject>) serviceInput.get("taskCrudActiveList");

		JSONObject serviceOutput = new JSONObject();
		JSONObject jsonOutput = new JSONObject();

		try {
			// 1. Update m_role
			StringBuilder sb = new StringBuilder();
			sb.append("UPDATE ")
					.append(Role.ENTITY_NAME)
					.append(" SET roleName = :roleName, roleCode = :roleCode, updateDatetime = :updateDatetime, updateUserId = :updateUserId ")
					.append(" WHERE id = :roleId ");

			Query query = roleDao.createQuery(sb.toString());
			query.setParameter("roleId", roleId);
			query.setParameter("roleName", roleName);
			query.setParameter("roleCode", roleCode);
			query.setParameter("updateUserId", updateUserId);
			query.setParameter("updateDatetime", datetime);

			query.executeUpdate();
			roleDao.closeSessionCreateQuery();

			// 2. Update m_role_task_crud_active
			if (taskCrudActiveList != null) {
				for (HashMap taskCrudActive : taskCrudActiveList) {
					Long taskCrudId = Long.valueOf(String.valueOf(  taskCrudActive.get("taskCrudId")));
					Long roleTaskCrudActiveId = Long.valueOf(String.valueOf( taskCrudActive.get("roleTaskCrudActiveId")));
					String createTaskActive = (String) taskCrudActive.get("createTaskActive");
					Long createTaskId = Long.valueOf(taskCrudActive.get("createTaskId").toString());
					String readTaskActive = (String) taskCrudActive.get("readTaskActive");
					Long readTaskId = Long.valueOf(taskCrudActive.get("readTaskId").toString());
					String updateTaskActive = (String) taskCrudActive.get("updateTaskActive");
					Long updateTaskId = Long.valueOf(taskCrudActive.get("updateTaskId").toString());
					String deleteTaskActive = (String) taskCrudActive.get("deleteTaskActive");
					Long deleteTaskId = Long.valueOf(taskCrudActive.get("deleteTaskId").toString());

					// update ke m_role_task_crud_active
					StringBuilder sbTaskCrudActiveList = new StringBuilder();
					sbTaskCrudActiveList
							.append("UPDATE ")
							.append(RoleTaskCrudActive.ENTITY_NAME)
							.append(" SET createTaskActive = :createTaskActive, readTaskActive = :readTaskActive, ")
							.append(" updateTaskActive = :updateTaskActive, deleteTaskActive = :deleteTaskActive, ")
							.append(" updateDatetime = :updateDatetime, updateUserId = :updateUserId ")
							.append(" WHERE roleId = :roleId AND taskCrudId = :taskCrudId AND id = :roleTaskCrudActiveId ");

					Query queryRoleTaskCrudActiveDao = roleTaskCrudActiveDao.createQuery(sbTaskCrudActiveList.toString());
					queryRoleTaskCrudActiveDao.setParameter("createTaskActive",createTaskActive);
					queryRoleTaskCrudActiveDao.setParameter("readTaskActive",readTaskActive);
					queryRoleTaskCrudActiveDao.setParameter("updateTaskActive",updateTaskActive);
					queryRoleTaskCrudActiveDao.setParameter("deleteTaskActive",deleteTaskActive);
					queryRoleTaskCrudActiveDao.setParameter("updateUserId",updateUserId);
					queryRoleTaskCrudActiveDao.setParameter("updateDatetime",datetime);
					queryRoleTaskCrudActiveDao.setParameter("roleId", roleId);
					queryRoleTaskCrudActiveDao.setParameter("taskCrudId",taskCrudId);
					queryRoleTaskCrudActiveDao.setParameter("roleTaskCrudActiveId", roleTaskCrudActiveId);

					queryRoleTaskCrudActiveDao.executeUpdate();
					roleTaskCrudActiveDao.closeSessionCreateQuery();

					if (createTaskActive.equals(MasterGeneralConstants.NO)
							&& !createTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> DELETING ROLETASK FOR CREATE << ");

						StringBuilder deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM t_approval_config ")
								.append(" WHERE EXISTS ( ")
								.append(" SELECT 1 FROM t_role_task ")
								.append(" WHERE t_role_task.role_id = :roleId AND t_role_task.task_id = :createTaskId  ")
								.append(" AND (t_approval_config.role_task_id_submit = t_role_task.role_task_id OR t_approval_config.role_task_id_approval = t_role_task.role_task_id) ")
								.append(" ) ");

						Query deleteQ = roleTaskDao
								.createSQLQuery(deleteTaskCrud.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("createTaskId", createTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

						deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM ")
								.append(RoleTask.TABLE_NAME)
								.append(" WHERE role_id = :roleId AND task_id = :createTaskId ");

						deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud
								.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("createTaskId", createTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					} else if (createTaskActive.equals(MasterGeneralConstants.YES)
							&& !createTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> INSERTING ROLETASK FOR CREATE << ");
						StringBuilder insertTaskCrud = new StringBuilder();
						insertTaskCrud
								.append(" INSERT INTO t_role_task (create_datetime, create_user_id, role_id, task_id, update_datetime, update_user_id) ")
								.append(" SELECT :datetime, :updateUserId, :roleId, :createTaskId, :datetime, :updateUserId ")
								.append(" FROM (SELECT 1) A ")
								.append(" WHERE NOT EXISTS (SELECT 1 FROM t_role_task WHERE role_id = :roleId AND task_id = :createTaskId) ");

						Query insertQ = roleTaskDao
								.createSQLQuery(insertTaskCrud.toString());
						insertQ.setParameter("datetime", datetime);
						insertQ.setParameter("updateUserId", updateUserId);
						insertQ.setParameter("roleId", roleId);
						insertQ.setParameter("createTaskId", createTaskId);
						insertQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					}

					if (readTaskActive.equals(MasterGeneralConstants.NO)
							&& !readTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> DELETING ROLETASK FOR READ << ");
						StringBuilder deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM t_approval_config ")
								.append(" WHERE EXISTS ( ")
								.append(" SELECT 1 FROM t_role_task ")
								.append(" WHERE t_role_task.role_id = :roleId AND t_role_task.task_id = :readTaskId  ")
								.append(" AND (t_approval_config.role_task_id_submit = t_role_task.role_task_id OR t_approval_config.role_task_id_approval = t_role_task.role_task_id) ")
								.append(" ) ");

						Query deleteQ = roleTaskDao
								.createSQLQuery(deleteTaskCrud.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("readTaskId", readTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

						deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM ")
								.append(RoleTask.TABLE_NAME)
								.append(" WHERE role_id = :roleId AND task_id = :readTaskId ");

						deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud
								.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("readTaskId", readTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					} else if (readTaskActive
							.equals(MasterGeneralConstants.YES)
							&& !readTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> INSERTING ROLETASK FOR READ << ");
						StringBuilder insertTaskCrud = new StringBuilder();
						insertTaskCrud
								.append(" INSERT INTO t_role_task (create_datetime, create_user_id, role_id, task_id, update_datetime, update_user_id) ")
								.append(" SELECT :datetime, :updateUserId, :roleId, :readTaskId, :datetime, :updateUserId ")
								.append(" FROM (SELECT 1) A ")
								.append(" WHERE NOT EXISTS (SELECT 1 FROM t_role_task WHERE role_id = :roleId AND task_id = :readTaskId) ");

						Query insertQ = roleTaskDao
								.createSQLQuery(insertTaskCrud.toString());
						insertQ.setParameter("datetime", datetime);
						insertQ.setParameter("updateUserId", updateUserId);
						insertQ.setParameter("roleId", roleId);
						insertQ.setParameter("readTaskId", readTaskId);
						insertQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					}

					if (updateTaskActive.equals(MasterGeneralConstants.NO)
							&& !updateTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> DELETING ROLETASK FOR UPDATE << ");
						StringBuilder deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM t_approval_config ")
								.append(" WHERE EXISTS ( ")
								.append(" SELECT 1 FROM t_role_task ")
								.append(" WHERE t_role_task.role_id = :roleId AND t_role_task.task_id = :updateTaskId  ")
								.append(" AND (t_approval_config.role_task_id_submit = t_role_task.role_task_id OR t_approval_config.role_task_id_approval = t_role_task.role_task_id) ")
								.append(" ) ");

						Query deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("updateTaskId", updateTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

						deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM ")
								.append(RoleTask.TABLE_NAME)
								.append(" WHERE role_id = :roleId AND task_id = :updateTaskId");

						deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud
								.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("updateTaskId", updateTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					} else if (updateTaskActive.equals(MasterGeneralConstants.YES)
							&& !updateTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						System.out
								.println(" >> INSERTING ROLETASK FOR DELETE << ");
						StringBuilder insertTaskCrud = new StringBuilder();
						insertTaskCrud
								.append(" INSERT INTO t_role_task (create_datetime, create_user_id, role_id, task_id, update_datetime, update_user_id) ")
								.append(" SELECT :datetime, :updateUserId, :roleId, :updateTaskId, :datetime, :updateUserId ")
								.append(" FROM (SELECT 1) A ")
								.append(" WHERE NOT EXISTS (SELECT 1 FROM t_role_task WHERE role_id = :roleId AND task_id = :updateTaskId) ");

						Query insertQ = roleTaskDao
								.createSQLQuery(insertTaskCrud.toString());
						insertQ.setParameter("datetime", datetime);
						insertQ.setParameter("updateUserId", updateUserId);
						insertQ.setParameter("roleId", roleId);
						insertQ.setParameter("updateTaskId", updateTaskId);
						insertQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					}

					if (deleteTaskActive.equals(MasterGeneralConstants.NO)
							&& !deleteTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> DELETING ROLETASK FOR DELETE << ");
						StringBuilder deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM t_approval_config ")
								.append(" WHERE EXISTS ( ")
								.append(" SELECT 1 FROM t_role_task ")
								.append(" WHERE t_role_task.role_id = :roleId AND t_role_task.task_id = :deleteTaskId  ")
								.append(" AND (t_approval_config.role_task_id_submit = t_role_task.role_task_id OR t_approval_config.role_task_id_approval = t_role_task.role_task_id) ")
								.append(" ) ");

						Query deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("deleteTaskId", deleteTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

						deleteTaskCrud = new StringBuilder();
						deleteTaskCrud
								.append(" DELETE FROM ")
								.append(RoleTask.TABLE_NAME)
								.append(" WHERE role_id = :roleId AND task_id = :deleteTaskId ");

						deleteQ = roleTaskDao.createSQLQuery(deleteTaskCrud.toString());
						deleteQ.setParameter("roleId", roleId);
						deleteQ.setParameter("deleteTaskId", deleteTaskId);
						deleteQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					} else if (deleteTaskActive.equals(MasterGeneralConstants.YES)
							&& !deleteTaskId.equals(MasterGeneralConstants.NULL_REF_VALUE_LONG)) {
						// System.out.println(" >> INSERTING ROLETASK FOR DELETE << ");
						StringBuilder insertTaskCrud = new StringBuilder();
						insertTaskCrud
								.append(" INSERT INTO t_role_task (create_datetime, create_user_id, role_id, task_id, update_datetime, update_user_id) ")
								.append(" SELECT :datetime, :updateUserId, :roleId, :deleteTaskId, :datetime, :updateUserId ")
								.append(" FROM (SELECT 1) A ")
								.append(" WHERE NOT EXISTS (SELECT 1 FROM t_role_task WHERE role_id = :roleId AND task_id = :deleteTaskId) ");

						Query insertQ = roleTaskDao
								.createSQLQuery(insertTaskCrud.toString());
						insertQ.setParameter("datetime", datetime);
						insertQ.setParameter("updateUserId", updateUserId);
						insertQ.setParameter("roleId", roleId);
						insertQ.setParameter("deleteTaskId", deleteTaskId);
						insertQ.executeUpdate();

						roleTaskDao.closeSessionCreateQuery();

					}

				}
			}
			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			jsonOutput.put("serviceOutput", serviceOutput);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}

		return jsonOutput;
	}

}