package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateRole.CreateRoleEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko
 * crete new role
 *
 */

@Service
public class CreateRole implements DataProcess {

	@Autowired
	RoleDao roleDao;

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Autowired
	RoleTaskDao roleTaskDao;

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput){

       	//Validation
    	Field[] fields = CreateRoleEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}
        ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
        ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

        //prepare data
        Role roleAddData = new Role();
        RoleTaskCrudActive roleTaskCrudActiveAddData = new RoleTaskCrudActive();

    	List<JSONObject> taskCrudActiveList = new ArrayList<JSONObject>();
    	taskCrudActiveList = (List<JSONObject>) serviceInput.get("taskCrudActiveList");
    	Long roleIdAdd = new Long(0);
    	String flagNewRole = (String) serviceInput.get("flagNewRole");
    	Long createUserId = Long.valueOf(String.valueOf(serviceInput.get("userId")));
    	Long updateUserId = Long.valueOf(String.valueOf(serviceInput.get("userId")));
    	String datetime = DateUtil.getDatetimeNow();
    	String newRoleCode = new String();
    	String newRoleName = new String();

        JSONObject jsonOutput = new JSONObject();

    	try{

    	//cek flag new role
    	if(MasterGeneralConstants.YES.equals(flagNewRole)){
    		//if Y, insert ke m_role

    		//additional data
    		newRoleCode = (String) serviceInput.get("roleCode");
    		newRoleName = (String) serviceInput.get("roleName");

    		Query checkData = roleDao
					.createQuery("Select count(1) from "+Role.ENTITY_NAME
							+ " where roleCode = :roleCode AND active = :active ");
	    	checkData.setParameter("roleCode", newRoleCode);
	    	checkData.setParameter("active", MasterGeneralConstants.YES);
			Long result = (Long) checkData.uniqueResult();

			roleDao.closeSessionCreateQuery();
			if(result<1){
	    		//mapping data
	    		roleAddData.setId(null);
	    		roleAddData.setRoleCode(newRoleCode);
	    		roleAddData.setRoleName(newRoleName);
	    		roleAddData.setActive(MasterGeneralConstants.YES);
	    		roleAddData.setCreateUserId(createUserId);
	    		roleAddData.setCreateDatetime(datetime);
	    		roleAddData.setUpdateUserId(updateUserId);
	    		roleAddData.setUpdateDatetime(datetime);

	    		//insert ke m_role
	    		roleDao.add(roleAddData);


	    		if(taskCrudActiveList != null){
	    			for(HashMap taskCrudActive : taskCrudActiveList){

	    				roleIdAdd = roleAddData.getId();
	    				Long taskCrudId = Long.valueOf(String.valueOf(taskCrudActive.get("taskCrudId")));
	    				String createTaskActive = (String) taskCrudActive.get("createTaskActive");
	    				String readTaskActive = (String) taskCrudActive.get("readTaskActive");
	    				String updateTaskActive = (String) taskCrudActive.get("updateTaskActive");
	    				String deleteTaskActive = (String) taskCrudActive.get("deleteTaskActive");

	    				Long createTaskId = Long.valueOf(taskCrudActive.get("createTaskId").toString());
						Long readTaskId = Long.valueOf(taskCrudActive.get("readTaskId").toString());
						Long updateTaskId = Long.valueOf(taskCrudActive.get("updateTaskId").toString());
						Long deleteTaskId = Long.valueOf(taskCrudActive.get("deleteTaskId").toString());

	    				//insert ke m_role_task_crud_active
	    				//mapping data
	    				roleTaskCrudActiveAddData.setId(null);
	    				roleTaskCrudActiveAddData.setRoleId(roleIdAdd);
	    				roleTaskCrudActiveAddData.setTaskCrudId(taskCrudId);
	    				roleTaskCrudActiveAddData.setCreateTaskActive(createTaskActive);
	    				roleTaskCrudActiveAddData.setReadTaskActive(readTaskActive);
	    				roleTaskCrudActiveAddData.setUpdateTaskActive(updateTaskActive);
	    				roleTaskCrudActiveAddData.setDeleteTaskActive(deleteTaskActive);
	    				roleTaskCrudActiveAddData.setActive(MasterGeneralConstants.YES);
	    				roleTaskCrudActiveAddData.setCreateDatetime(datetime);
	    				roleTaskCrudActiveAddData.setCreateUserId(createUserId);
	    				roleTaskCrudActiveAddData.setUpdateDatetime(datetime);
	    				roleTaskCrudActiveAddData.setUpdateUserId(updateUserId);

	    				//insert ke m_role_task_crud_active
	    				roleTaskCrudActiveDao.add(roleTaskCrudActiveAddData);

	    				if(createTaskActive.equals(MasterGeneralConstants.YES)){
	    					RoleTask newCreateRoleTask = new RoleTask();
	    					newCreateRoleTask.setRoleId(roleIdAdd);
	    					newCreateRoleTask.setTaskId(createTaskId);
	    					newCreateRoleTask.setCreateDatetime(datetime);
	    					newCreateRoleTask.setCreateUserId(createUserId);
	    					newCreateRoleTask.setUpdateDatetime(datetime);
	    					newCreateRoleTask.setUpdateUserId(updateUserId);

	    					roleTaskDao.add(newCreateRoleTask);
	    				}

						if(readTaskActive.equals(MasterGeneralConstants.YES)){
							RoleTask newReadRoleTask = new RoleTask();
							newReadRoleTask.setRoleId(roleIdAdd);
							newReadRoleTask.setTaskId(readTaskId);
							newReadRoleTask.setCreateDatetime(datetime);
							newReadRoleTask.setCreateUserId(createUserId);
							newReadRoleTask.setUpdateDatetime(datetime);
							newReadRoleTask.setUpdateUserId(updateUserId);

	    					roleTaskDao.add(newReadRoleTask);
						}

						if(updateTaskActive.equals(MasterGeneralConstants.YES)){
							RoleTask newUpdateRoleTask = new RoleTask();
							newUpdateRoleTask.setRoleId(roleIdAdd);
							newUpdateRoleTask.setTaskId(updateTaskId);
							newUpdateRoleTask.setCreateDatetime(datetime);
							newUpdateRoleTask.setCreateUserId(createUserId);
							newUpdateRoleTask.setUpdateDatetime(datetime);
							newUpdateRoleTask.setUpdateUserId(updateUserId);

	    					roleTaskDao.add(newUpdateRoleTask);
						}

						if(deleteTaskActive.equals(MasterGeneralConstants.YES)){
							RoleTask newDeleteRoleTask = new RoleTask();
							newDeleteRoleTask.setRoleId(roleIdAdd);
							newDeleteRoleTask.setTaskId(deleteTaskId);
							newDeleteRoleTask.setCreateDatetime(datetime);
							newDeleteRoleTask.setCreateUserId(createUserId);
							newDeleteRoleTask.setUpdateDatetime(datetime);
							newDeleteRoleTask.setUpdateUserId(updateUserId);

	    					roleTaskDao.add(newDeleteRoleTask);
						}

	    			}
	    		}
	    		jsonOutput.put("serviceInput", serviceInput);
	            jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
	            jsonOutput.put("serviceOutput", "OK");
			}
			else{
				jsonOutput.put("serviceInput", serviceInput);
	            jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
	            jsonOutput.put("serviceOutput", "Failed");
			}
    	}else{
    		jsonOutput.put("serviceInput", serviceInput);
            jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
            jsonOutput.put("serviceOutput", "Failed");
    	}



	} catch (Exception e) {
		e.printStackTrace();

		String errorMessage = e.getCause().toString();
		errorMessage=errorMessage.substring((errorMessage.indexOf(':')+1),errorMessage.length());
		errorMessage = errorMessage.trim();

		jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
		jsonOutput.put("errorMessage", errorMessage);
	}
	return jsonOutput;

    }

}
