package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ActivationEntity;
import com.mlpt.bvnextgen.model.userManagement.Client;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko bo ini digunkan untuk mengaktifkan kembali user
 *
 */

@Service
public class ActivationUserById implements DataProcess {

	@Autowired
	UserDao userDao;

	@Autowired
	ClientDao clientDao;

	private Session session;

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = ActivationEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		Gson gson = new Gson();
		String jsonStr = gson.toJson(serviceInput);
		ActivationEntity data = gson.fromJson(jsonStr, ActivationEntity.class);
		String datetime = DateUtil.getDatetimeNow();

		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();

		try {

			// get data
			UserEntity userRemoveData = userDao.find(data.getId());

			Query query = userDao.createQuery("UPDATE " + UserEntity.ENTITY_NAME + " SET active = :active "
					+ " ,updateUserId = :updateUserId" + " ,updateDatetime = :updateDatetime" + " WHERE id = :userId");
			query.setParameter("userId", data.getId());
			query.setParameter("updateUserId", data.getUpdateUserId());
			query.setParameter("updateDatetime", datetime);
			query.setParameter("active", data.getStatus());
			query.executeUpdate();

			userDao.closeSessionCreateQuery();

			// delete data client
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ").append(Client.ENTITY_NAME).append(" WHERE clientId = :clientId ");

			Query delClient = clientDao.createQuery(sb.toString());
			delClient.setParameter("clientId", userRemoveData.getUsername());

			delClient.executeUpdate();
			clientDao.closeSessionCreateQuery();

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
