package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.Task;
import com.mlpt.bvnextgen.model.userManagement.TaskDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get task ID by name task
 */

@Service
public class GetTaskIdByName implements DataProcess {

	@Autowired
	TaskDao taskDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String taskName = String.valueOf(serviceInput.get("parameter"));
		String activeCode = MasterGeneralConstants.YES;
		Long taskId = null;

		JSONObject jsonOutput = new JSONObject();

		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT e.id AS task_id ").append(" FROM ")
					.append(Task.ENTITY_NAME).append(" e ")
					.append("WHERE e.active = :active ")
					.append(" AND e.taskName = :taskName ");

			Query query = taskDao.createQuery(sb.toString());
			query.setParameter("active", activeCode);
			query.setParameter("taskName", taskName);
			taskId = (Long) query.uniqueResult();

			taskDao.closeSessionCreateQuery();

			jsonOutput.put("taskId", taskId);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
