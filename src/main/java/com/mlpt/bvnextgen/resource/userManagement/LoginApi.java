package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.LoginApiEntity;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.Md5Util;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko digunakan untuk login sekali gus untuk generate token
 */

@Service
public class LoginApi implements DataProcess {

	@Autowired
	GetUserbyUsername login;

	@Autowired
	EditUserLoginStatus editUserLoginStatus;

	@Autowired
	GetRoleTaskByRoleId getRoleTaskByRoleId;

	@Autowired
	UserDao userDao;

	@Autowired
	RoleDao roleDao;
	
	

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {
		// System.out.println("login");

		// Validation
		LoginApiEntity inputUser = new LoginApiEntity();
		Field[] fields = LoginApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String username = String.valueOf(serviceInput.get("username"));
		String password = String.valueOf(serviceInput.get("password"));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject resultOutput = new JSONObject();

		List<Object> resultList = new ArrayList<Object>();
		try {

			StringBuilder queryStr = new StringBuilder();
			queryStr = new StringBuilder();
			queryStr.append(" SELECT password ")
			.append(" FROM ").append(UserEntity.ENTITY_NAME)
			.append(" WHERE username = :username AND active = :status and confirmationCode is null");
			
			Query query = userDao.createQuery(queryStr.toString());
			query.setParameter("username", username);
			query.setParameter("status", MasterGeneralConstants.YES);
			
			resultList = query.list();
			
			userDao.closeSessionCreateQuery();
			if(resultList.size()<1){
				resultOutput.put("status","F");
				resultOutput.put("errorMessage","username doesn't exist !!");
			}else if(resultList.size()>1){
				resultOutput.put("status","F");
				resultOutput.put("errorMessage","duplicate username !!");
			}else{
				String passwordDB = (String) resultList.get(0);
//				String passwordDecrypt = AESUtil.decrypt(passwordDB);
				password = Md5Util.getMD5(password);
				if(password.equalsIgnoreCase(passwordDB)){
					Map<?, ?> map = getAccessToken(username,password);
					resultOutput = new JSONObject(map);
					String randomString = resultOutput.get("refresh_token").toString();
					resultOutput.put("randomString", randomString);
					resultOutput.put("status","S");
				}else{
					resultOutput.put("status","F");
					resultOutput.put("errorMessage","invalid passsword !!");
				}
				
			}  
			
    		return resultOutput;
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);
		}

		return resultOutput;

	}

	private Map<?, ?> getAccessToken(String username, String password)
			throws Exception {

		// Hit Auth2 Server
		RestTemplate restTemplate = new RestTemplate();

		// client credential
		String clientCredentials = username + ":" + password;
		// String clientCredentials = "new-core3:new-core3";
		String encodedCredentials = new String(
				Base64.encodeBase64(clientCredentials.getBytes()));

		// header
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Authorization", "Basic " + encodedCredentials);

		// request
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = null;
		
		Properties prop = new Properties();
		prop.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
		String siteURL = prop.getProperty("application.site");
		String url = siteURL+"/oauth/token";
		System.out.println("bearer : "+encodedCredentials);
		String grantPassword = "?grant_type=password&username=" + username
				+ "&password=" + password;

		response = restTemplate.exchange(url + grantPassword, HttpMethod.POST,
				request, String.class);

		Map<?, ?> mapResponse = new ObjectMapper().readValue(
				response.getBody(), Map.class);

		return mapResponse;
	}

}
