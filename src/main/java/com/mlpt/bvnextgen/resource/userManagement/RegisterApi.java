package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.RegisterApiEntity;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.SendMailFunction;
import com.mlpt.bvnextgen.resource.core.AESUtil;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim digunakan untuk login sekali gus untuk generate token
 */

@Service
public class RegisterApi implements DataProcess {

	@Autowired
	GetUserbyUsername login;

	@Autowired
	EditUserLoginStatus editUserLoginStatus;

	@Autowired
	GetRoleTaskByRoleId getRoleTaskByRoleId;

	@Autowired
	UserDao userDao;
	
	@Autowired
	ClientDao clientDao;

	@Autowired
	RoleDao roleDao;
	
	@Autowired
	SendMailFunction sendMailFunction;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {
		// System.out.println("login");

		// Validation
		RegisterApiEntity inputUser = new RegisterApiEntity();
		Field[] fields = RegisterApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String username = String.valueOf(serviceInput.get("username"));
		String password = String.valueOf(serviceInput.get("password"));
		String email = String.valueOf(serviceInput.get("email"));
		String fullname = String.valueOf(serviceInput.get("fullname"));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject resultOutput = new JSONObject();

		List<Object> resultList = new ArrayList<Object>();
		try {
			
			String datetime = DateUtil.getDatetimeNow();
			
			Query checkUser = userDao.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME
					+ " where username = :username and active =:active");
			checkUser.setParameter("username", username);
			checkUser.setParameter("active", MasterGeneralConstants.YES);
			Long resultUsername = (Long) checkUser.uniqueResult();

			userDao.closeSessionCreateQuery();
			
			Query checkEmail = userDao.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME
					+ " where email = :email and active =:active and confirmationCode is not null");
			checkEmail.setParameter("email", email);
			checkEmail.setParameter("active", MasterGeneralConstants.YES);
			Long resultEmail = (Long) checkEmail.uniqueResult();

			userDao.closeSessionCreateQuery();
			
			if (resultUsername == 0 && resultEmail ==0) {
				Random rn = new Random();
				
				Query deleteUserNotActive = userDao.createQuery("delete from " + UserEntity.ENTITY_NAME
						+ " where email = :email or username = :username and active = :active ");
				deleteUserNotActive.setParameter("email", email);
				deleteUserNotActive.setParameter("username", username);
				deleteUserNotActive.setParameter("active", MasterGeneralConstants.NO);
				
				deleteUserNotActive.executeUpdate();
				
				userDao.closeSessionCreateQuery();
				
				String activationCode = String.format("%04d", rn.nextInt(10000));
				
				// add m_user
				UserEntity userAddData = new UserEntity();
				// mapping data
				userAddData.setUsername(username);
				userAddData.setPassword(AESUtil.encrypt(password));
				userAddData.setEmail(email);
				userAddData.setUserFullname(fullname);
				userAddData.setCreateUserId(-1L);
				userAddData.setUserRole("10");
				userAddData.setPhone(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setAddress(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setVillage(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setDistrict(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setCity(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setProvince(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setUserImage(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setActive(MasterGeneralConstants.NO);
				userAddData.setCreateDatetime(datetime);
				userAddData.setStatusLogin(MasterGeneralConstants.NO);
				userAddData.setConfirmationCode(activationCode);
				userDao.add(userAddData);
				
//				Client clientdetails = new Client();
//				clientdetails.setClientId(username);
//				clientdetails.setClientSecret("{noop}"+Md5Util.getMD5(password));
//				clientdetails.setScope("read,write,trust");
//				clientdetails.setAuthorizedGrantTypes("authorization_code,check_token,refresh_token,password");
//				clientdetails.setAccessTokenValidity(1000L);
//				clientdetails.setRefreshTokenValidity(100000L);
//				clientDao.add(clientdetails);
				
				JSONObject inputMail = new JSONObject();
				inputMail.put("emailTo", email);
				inputMail.put("subject", "Activation Code Queueing App");
				inputMail.put("content", "<h1>Dear "+fullname+",</h1> "
	            		+ "<br/>"
	            		+ "<p>Berikut adalah kode konfirmasi anda untuk aplikasi antrian bank :</p>"
	            		+ "<h2>"+activationCode+"</h2>"
	            		+ "<p>Silahkan masukan kode tersebut pada aplikasi untuk melanjutkan registrasi anda</p>");
				
				
				sendMailFunction.processBo(inputMail);
				
				resultOutput.put("status","S");
			}else{
				resultOutput.put("status","F");
				resultOutput.put("errorMessage","username or email already exist !");
			}

			
			
    		return resultOutput;
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);
		}

		return resultOutput;

	}


}
