package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ActivationEntity;
import com.mlpt.bvnextgen.model.approvalManagement.ApprovalConfigDao;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

@Service
public class DeactivateRoleById implements DataProcess {

	@Autowired
	RoleDao roleDao;

	@Autowired
	RoleTaskDao roleTaskDao;

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Autowired
	ApprovalConfigDao approvalConfigDao;

	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = ActivationEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);


		
		Gson gson = new Gson();
		String jsonStr = gson.toJson(serviceInput);
		ActivationEntity data = gson.fromJson(jsonStr, ActivationEntity.class);
		

		String datetime = DateUtil.getDatetimeNow();

		JSONObject jsonOutput = new JSONObject();
		try {

			StringBuilder deleteRole = new StringBuilder();
			deleteRole.append(" UPDATE ").append(Role.ENTITY_NAME)
					.append(" SET active = :active  ")
					.append(" ,updateDatetime = :updateDatetime  ")
					.append(" ,updateUserId = :updateUserId  ")
					.append(" WHERE id = :roleId ");
			Query query = roleDao.createQuery(deleteRole.toString());
			query.setParameter("roleId", data.getId());
			query.setParameter("updateDatetime", datetime);
			query.setParameter("updateUserId", data.getUpdateUserId());
			query.setParameter("active", MasterGeneralConstants.NO);
			query.executeUpdate();
			roleDao.closeSessionCreateQuery();

			StringBuilder deleteApprovalConfig = new StringBuilder();
			deleteApprovalConfig
					.append(" DELETE FROM t_approval_config ")
					.append(" WHERE EXISTS ( ")
					.append(" SELECT 1 FROM t_role_task ")
					.append(" WHERE t_role_task.role_id = :roleId ")
					.append(" AND (t_approval_config.role_task_id_submit = t_role_task.role_task_id OR t_approval_config.role_task_id_approval = t_role_task.role_task_id) ")
					.append(" ) ");

			Query deleteApprovalConfigQuery = approvalConfigDao
					.createSQLQuery(deleteApprovalConfig.toString());
			deleteApprovalConfigQuery.setParameter("roleId", data.getId());
			deleteApprovalConfigQuery.executeUpdate();

			approvalConfigDao.closeSessionCreateQuery();

			StringBuilder deleteRoleTask = new StringBuilder();
			deleteRoleTask.append(" DELETE FROM ").append(RoleTask.TABLE_NAME)
					.append(" WHERE role_id = :roleId ");

			Query deleteQuery = roleTaskDao.createSQLQuery(deleteRoleTask
					.toString());
			deleteQuery.setParameter("roleId", data.getId());
			deleteQuery.executeUpdate();

			roleTaskDao.closeSessionCreateQuery();

			StringBuilder deleteRoleTaskCrudAct = new StringBuilder();
			deleteRoleTaskCrudAct.append(" DELETE FROM ")
					.append(RoleTaskCrudActive.TABLE_NAME)
					.append(" WHERE role_id = :roleId ");

			Query deleteRoleTaskCrudQuery = roleTaskCrudActiveDao
					.createSQLQuery(deleteRoleTaskCrudAct.toString());
			deleteRoleTaskCrudQuery.setParameter("roleId", data.getId());
			deleteRoleTaskCrudQuery.executeUpdate();

			roleTaskCrudActiveDao.closeSessionCreateQuery();

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			jsonOutput.put("serviceOutput", "OK");

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
