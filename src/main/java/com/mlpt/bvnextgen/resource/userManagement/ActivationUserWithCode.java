package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.ActivationUserWithCodeApiEntity;
import com.mlpt.bvnextgen.model.userManagement.Client;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.AESUtil;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim digunakan untuk login sekali gus untuk generate token
 */

@Service
public class ActivationUserWithCode implements DataProcess {

	@Autowired
	GetUserbyUsername login;

	@Autowired
	EditUserLoginStatus editUserLoginStatus;

	@Autowired
	GetRoleTaskByRoleId getRoleTaskByRoleId;

	@Autowired
	UserDao userDao;
	
	@Autowired
	ClientDao clientDao;

	@Autowired
	RoleDao roleDao;
	
	

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {
		// System.out.println("login");

		// Validation
		ActivationUserWithCodeApiEntity inputUser = new ActivationUserWithCodeApiEntity();
		Field[] fields = ActivationUserWithCodeApiEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String username = String.valueOf(serviceInput.get("username"));
		String password = String.valueOf(serviceInput.get("password"));
		String confirmationCodeInput = String.valueOf(serviceInput.get("confirmationCode"));

		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject resultOutput = new JSONObject();

		List<Object> resultList = new ArrayList<Object>();
		try {
			
			String datetime = DateUtil.getDatetimeNow();
			
			Query checkUser = userDao.createQuery("Select confirmationCode from " + UserEntity.ENTITY_NAME
					+ " where username = :username and password = :password");
			checkUser.setParameter("username", username);
			checkUser.setParameter("password", AESUtil.encrypt(password));
			String confirmationCode = (String) checkUser.uniqueResult();

			userDao.closeSessionCreateQuery();
			System.out.println("code input : "+confirmationCodeInput);
			System.out.println("code : "+confirmationCode);
			if (confirmationCodeInput.equalsIgnoreCase(confirmationCode)) {
				
				Client clientdetails = new Client();
				clientdetails.setClientId(username);
				clientdetails.setClientSecret("{noop}"+AESUtil.encrypt(password));
				clientdetails.setScope("read,write,trust");
				clientdetails.setAuthorizedGrantTypes("authorization_code,check_token,refresh_token,password");
				clientdetails.setAccessTokenValidity(100000L);
				clientdetails.setRefreshTokenValidity(100000L);
				clientDao.add(clientdetails);
				
				Query updateUser = userDao.createQuery("Update " + UserEntity.ENTITY_NAME
						+ " set active =:active where username = :username and password = :password");
				updateUser.setParameter("username", username);
				updateUser.setParameter("password", AESUtil.encrypt(password));
				updateUser.setParameter("active", MasterGeneralConstants.YES);
				
				updateUser.executeUpdate();
				userDao.closeSessionCreateQuery();
				
				resultOutput.put("status","S");
			}else{
				resultOutput.put("status","F");
				resultOutput.put("errorMessage","Confirmation Code doesn't match !");
			}

			
			
    		return resultOutput;
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage", errorMessage);
		}

		return resultOutput;

	}


}
