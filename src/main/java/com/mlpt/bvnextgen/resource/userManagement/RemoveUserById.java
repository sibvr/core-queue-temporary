package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.DeleteEntity;
import com.mlpt.bvnextgen.model.userManagement.Client;
import com.mlpt.bvnextgen.model.userManagement.ClientDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

@Service
public class RemoveUserById implements DataProcess {

	@Autowired
	UserDao userDao;

	@Autowired
	ClientDao clientDao;

	@Autowired
	GetUserById getUserById;

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		UserEntity userRemoveData = new UserEntity();
		JSONObject jsonOutput = new JSONObject();

		Gson gson = new Gson();
		String jsonStr = "";

		// Validation
		Field[] fields = DeleteEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		jsonStr = gson.toJson(serviceInput);
		DeleteEntity dataDelete = gson.fromJson(jsonStr, DeleteEntity.class);

		try {

			// get data
			userRemoveData = userDao.find(dataDelete.getDeleteId());


			// delete data client
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ").append(Client.ENTITY_NAME).append(" WHERE clientId = :clientId ");

			Query query = clientDao.createQuery(sb.toString());
			query.setParameter("clientId", userRemoveData.getUsername());

			query.executeUpdate();
			clientDao.closeSessionCreateQuery();

			// remove dari dalam table
			userDao.remove(userRemoveData);

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("errorMessage", errorMessage);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
		}
		return jsonOutput;
	}

}
