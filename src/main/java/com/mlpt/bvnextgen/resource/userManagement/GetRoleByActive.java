package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get role by actie code
 */

@Service
public class GetRoleByActive implements DataProcess {

	@Autowired
	RoleDao roleDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String status = String.valueOf(serviceInput.get("parameter"));

		JSONObject jsonOutput = new JSONObject();
		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();

		try {
			Query query = roleDao
					.createQuery("SELECT e.id AS role_id, e.roleCode AS role_code, e.roleName AS role_name FROM "
							+ Role.ENTITY_NAME + " e WHERE e.active = :status");
			query.setParameter("status", status);
			result = query.list();

			Iterator iter = result.iterator();
			roleDao.closeSessionCreateQuery();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();
				Long id = null != obj[0] ? Long.valueOf(obj[0].toString())
						: null;
				String roleCode = null != obj[1] ? obj[1].toString() : null;
				String roleName = null != obj[2] ? obj[2].toString() : null;
				tempJO.put("roleId", id);
				tempJO.put("roleCode", roleCode);
				tempJO.put("roleName", roleName);
				outputBo.add(tempJO);
			}

			jsonOutput.put("serviceInput", serviceInput);
			jsonOutput.put("serviceOutput", outputBo);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
