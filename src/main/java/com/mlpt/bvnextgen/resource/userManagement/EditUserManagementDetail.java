package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditUserManagementDetail.EditUserManagementDetailEntity;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.EditUserManagementDetail.EditUserManagementTaskCrudActiveEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.model.userManagement.UserRole;
import com.mlpt.bvnextgen.model.userManagement.UserRoleDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko bo untuk update data user ini juga akan melakukan
 *         update data client (client ID)
 */
@Service
public class EditUserManagementDetail implements DataProcess {

	@Autowired
	UserDao userDao;

	@Autowired
	EditClient editClient;

	@Autowired
	GetUserById getUserById;

	@Autowired
	RoleDao roleDao;

	@Autowired
	UserRoleDao userRoleDao;

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Autowired
	Logout logout;

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		// System.out.println("EditUserManagementDetail");

		List<EditUserManagementTaskCrudActiveEntity> taskCrudActiveList = new ArrayList<EditUserManagementTaskCrudActiveEntity>();

		// Validation
		Field[] fields = EditUserManagementDetailEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data

		Gson gson = new Gson();
		String jsonStr = gson.toJson(serviceInput);
		EditUserManagementDetailEntity UpdateData = gson.fromJson(jsonStr, EditUserManagementDetailEntity.class);

		Long roleId = UpdateData.getRoleId();
		String username = UpdateData.getUsername();
		String email = UpdateData.getEmail();
		Long userIdForUpdate = UpdateData.getId();
		Long updateUserId = UpdateData.getUpdateUserId();
		String datetime = DateUtil.getDatetimeNow();
		taskCrudActiveList = UpdateData.getTaskCrudActiveList();
		String flagNewRole = UpdateData.getFlagNewRole();
		String userFullname = UpdateData.getUserFullname();
		Long managerId = UpdateData.getManagerId();

		String errorMessage = "";
		JSONObject serviceOutput = new JSONObject();
		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput = new JSONObject();

		try {
			Query checkData = userDao.createQuery(
					"Select count(1) from " + UserEntity.ENTITY_NAME + " where active = :active AND id = :id ");
			checkData.setParameter("active", MasterGeneralConstants.YES);
			checkData.setParameter("id", userIdForUpdate);
			Long result = (Long) checkData.uniqueResult();

			userDao.closeSessionCreateQuery();
			if (result == 1) {

				Query checkUser = userDao
						.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME + " where username = :username ");
				checkUser.setParameter("username", username);
				Long resultUsername = (Long) checkUser.uniqueResult();

				userDao.closeSessionCreateQuery();

				Query checkEmail = userDao
						.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME + " where email = :email ");
				checkEmail.setParameter("email", email);
				Long resultEmail = (Long) checkEmail.uniqueResult();

				userDao.closeSessionCreateQuery();

				if (resultUsername == 0 && resultEmail == 0) {

					// 1.cek flag new role
					if (MasterGeneralConstants.YES.equals(flagNewRole)) {
						// if Y, insert ke m_role
						Role roleAddData = new Role();
						String newRoleCode = new String();
						String newRoleName = new String();
						Long createUserId = Long.valueOf((Integer) serviceInput.get("userId"));

						// additional data
						newRoleCode = String.valueOf(serviceInput.get("roleCode"));
						newRoleName = String.valueOf(serviceInput.get("roleName"));

						// mapping data
						roleAddData.setId(null);
						roleAddData.setRoleCode(newRoleCode);
						roleAddData.setRoleName(newRoleName);
						roleAddData.setActive(MasterGeneralConstants.YES);
						roleAddData.setCreateUserId(createUserId);
						roleAddData.setCreateDatetime(datetime);
						roleAddData.setUpdateUserId(updateUserId);
						roleAddData.setUpdateDatetime(datetime);

						// insert ke m_role
						roleDao.add(roleAddData);

						// baca template m_task_crud yg dapet dari UI
						// parse dulu

						if (taskCrudActiveList != null) {
							for (EditUserManagementTaskCrudActiveEntity taskCrudActive : taskCrudActiveList) {

								RoleTaskCrudActive roleTaskCrudActiveAddData = new RoleTaskCrudActive();

								roleId = roleAddData.getId();
								Long taskCrudId = taskCrudActive.getTaskCrudId();
								String createTaskActive = taskCrudActive.getCreateTaskActive();
								String readTaskActive = taskCrudActive.getReadTaskActive();
								String updateTaskActive = taskCrudActive.getUpdateTaskActive();
								String deleteTaskActive = taskCrudActive.getDeleteTaskActive();

								// insert ke m_role_task_crud_active
								// mapping data
								roleTaskCrudActiveAddData.setId(null);
								roleTaskCrudActiveAddData.setRoleId(roleId);
								roleTaskCrudActiveAddData.setTaskCrudId(taskCrudId);
								roleTaskCrudActiveAddData.setCreateTaskActive(createTaskActive);
								roleTaskCrudActiveAddData.setReadTaskActive(readTaskActive);
								roleTaskCrudActiveAddData.setUpdateTaskActive(updateTaskActive);
								roleTaskCrudActiveAddData.setDeleteTaskActive(deleteTaskActive);
								roleTaskCrudActiveAddData.setActive(MasterGeneralConstants.YES);
								roleTaskCrudActiveAddData.setCreateDatetime(datetime);
								roleTaskCrudActiveAddData.setCreateUserId(createUserId);
								roleTaskCrudActiveAddData.setUpdateDatetime(datetime);
								roleTaskCrudActiveAddData.setUpdateUserId(updateUserId);

								// insert ke m_role_task_crud_active
								roleTaskCrudActiveDao.add(roleTaskCrudActiveAddData);
							}
						}

					}

					// update data client
					UserEntity getUser = userDao.find(userIdForUpdate);

					if (!getUser.getUsername().equals(username)) {
						if (userIdForUpdate == updateUserId) {
							// logout untuk clear token
							logout.processBo(null);
						}
						jsonInput.put("clientId", getUser.getUsername());
						jsonInput.put("newClientId", username);
						jsonInput.put("newClientSecret", getUser.getPassword());
						editClient.processBo(jsonInput);
					}

					// update data user (m_user)
					StringBuilder sb = new StringBuilder();
					sb.append("UPDATE ").append(UserEntity.ENTITY_NAME)
							.append(" SET username = :userName, email = :email, "
									+ "updateDatetime = :updateDatetime, updateUserId = :updateUserId, "
									+ "userFullname = :userFullname, statusLogin =:statusLogin "
									+ ", managerId = :managerId")
							.append(" WHERE id = :userId ");

					Query query = userDao.createQuery(sb.toString());
					query.setParameter("userId", userIdForUpdate);
					query.setParameter("userName", username);
					query.setParameter("email", email);
					query.setParameter("updateUserId", updateUserId);
					query.setParameter("updateDatetime", datetime);
					query.setParameter("userFullname", userFullname);
					query.setParameter("managerId", managerId);
					query.setParameter("statusLogin", MasterGeneralConstants.NO);

					query.executeUpdate();
					userDao.closeSessionCreateQuery();

					// 3. Update m_user_role
					StringBuilder sbUserRole = new StringBuilder();
					sbUserRole.append("UPDATE ").append(UserRole.ENTITY_NAME).append(" SET roleId = :roleId ")
							.append(" WHERE userId = :userId");

					Query queryUserRoleDao = userRoleDao.createQuery(sbUserRole.toString());
					queryUserRoleDao.setParameter("roleId", roleId);
					queryUserRoleDao.setParameter("userId", userIdForUpdate);

					queryUserRoleDao.executeUpdate();
					userRoleDao.closeSessionCreateQuery();

					jsonOutput.put("serviceInput", serviceInput);
					jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
				}else {
					jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
					jsonOutput.put("errorMessage", "username or email already use");
				}
			} else {
				jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				jsonOutput.put("errorMessage", "user not found");
			}

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}

		return jsonOutput;
	}

}