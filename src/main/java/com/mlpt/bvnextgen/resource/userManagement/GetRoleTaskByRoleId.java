package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.RoleTask;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.model.userManagement.Task;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko get role task by role ID
 */

@Service
public class GetRoleTaskByRoleId implements DataProcess {

	@Autowired
	RoleTaskDao roleTaskDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		Long roleId = Long.valueOf(serviceInput.get("parameter").toString());

		JSONObject outputJson = new JSONObject();
		List<Object> resultList = new ArrayList<Object>();
		List<JSONObject> roleTaskList = new ArrayList<JSONObject>();
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT A.id, A.taskId, B.taskName, B.taskCode ")
					.append(" FROM ").append(RoleTask.ENTITY_NAME)
					.append(" A, ").append(Task.ENTITY_NAME).append(" B ")
					.append(" WHERE A.taskId = B.id AND A.roleId = :roleId ");

			Query roleTaskListQuery = roleTaskDao.createQuery(sb.toString());
			roleTaskListQuery.setParameter("roleId", roleId);
			resultList = roleTaskListQuery.list();

			roleTaskDao.closeSessionCreateQuery();

			Iterator iter = resultList.iterator();

			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject roleTaskJson = new JSONObject();

				Long roleTaskId = obj[0] != null ? Long.valueOf(obj[0]
						.toString()) : null;
				Long taskId = obj[1] != null ? Long.valueOf(obj[1].toString())
						: null;
				String taskName = obj[2] != null ? obj[2].toString() : null;
				String taskCode = obj[3] != null ? obj[3].toString() : null;

				roleTaskJson.put("roleTaskId", roleTaskId);
				roleTaskJson.put("taskId", taskId);
				roleTaskJson.put("taskName", taskName);
				roleTaskJson.put("taskCode", taskCode);

				roleTaskList.add(roleTaskJson);
			}

			outputJson.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			outputJson.put("roleTaskList", roleTaskList);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			outputJson = new JSONObject();
			outputJson.put("status", MasterGeneralConstants.STATUS_FAILED);
			outputJson.put("errorMessage", errorMessage);
			return outputJson;
		}

		return outputJson;

	}

}
