package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.Task;
import com.mlpt.bvnextgen.model.userManagement.TaskCrud;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko bo untuk inquiry detail role management berdasarkan ID
 */

@Service
public class InquiryRoleManagementDetail implements DataProcess {

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput,
				validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();

		// prepare data
		String activeCode = MasterGeneralConstants.YES;
		Long roleIdReq = Long.valueOf(String.valueOf(serviceInput
				.get("parameter")));

		JSONObject jsonOutput = new JSONObject();

		String roleCode = "";
		String roleName = "";
		Long roleIdResult = new Long(0);

		try {
			StringBuilder sb = new StringBuilder();
			sb.append(
					" SELECT A.role_task_crud_active_id, A.task_crud_id, A.role_id, G.role_name, ")
					.append(" B.create_task_id, A.create_task_active, COALESCE(C.task_code, :EMPTY), COALESCE(C.task_name, :EMPTY), ")
					.append(" B.read_task_id, A.read_task_active, COALESCE(D.task_code, :EMPTY), COALESCE(D.task_name, :EMPTY), ")
					.append(" B.update_task_id, A.update_task_active, COALESCE(E.task_code, :EMPTY), COALESCE(E.task_name, :EMPTY), ")
					.append(" B.delete_task_id, A.delete_task_active, COALESCE(F.task_code, :EMPTY), COALESCE(F.task_name, :EMPTY), ")
					.append(" B.task_crud_name, G.role_code ")
					.append(" FROM ")
					.append(RoleTaskCrudActive.TABLE_NAME)
					.append(" A ")
					.append(" INNER JOIN ")
					.append(TaskCrud.TABLE_NAME)
					.append(" B ON A.task_crud_id = B.task_crud_id ")
					.append(" INNER JOIN ")
					.append(Role.TABLE_NAME)
					.append(" G ON A.role_id = G.role_id ")
					.append(" LEFT JOIN ")
					.append(Task.TABLE_NAME)
					.append(" C ON B.create_task_id = C.task_id ")
					.append(" LEFT JOIN ")
					.append(Task.TABLE_NAME)
					.append(" D ON B.read_task_id = D.task_id ")
					.append(" LEFT JOIN ")
					.append(Task.TABLE_NAME)
					.append(" E ON B.update_task_id = E.task_id ")
					.append(" LEFT JOIN ")
					.append(Task.TABLE_NAME)
					.append(" F ON B.delete_task_id = F.task_id ")
					.append(" WHERE A.role_id = :roleId AND A.active = :active ");

			Query query = roleTaskCrudActiveDao.createSQLQuery(sb.toString());
			query.setParameter("active", activeCode);
			query.setParameter("roleId", roleIdReq);
			query.setParameter("EMPTY", MasterGeneralConstants.EMPTY_VALUE);
			result = query.list();

			roleTaskCrudActiveDao.closeSessionCreateQuery();

			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();

				Long roleTaskCrudActiveId = null != obj[0] ? Long
						.valueOf(obj[0].toString()) : null;
				Long taskCrudId = null != obj[1] ? Long.valueOf(obj[1]
						.toString()) : null;
				roleIdResult = null != obj[2] ? Long.valueOf(obj[2].toString())
						: null;
				roleName = null != obj[3] ? obj[3].toString() : null;
				Long createTaskId = null != obj[4] ? Long.valueOf(obj[4]
						.toString()) : null;
				String createTaskActive = null != obj[5] ? obj[5].toString()
						: null;
				String createTaskCode = null != obj[6] ? obj[6].toString()
						: null;
				String createTaskName = null != obj[7] ? obj[7].toString()
						: null;
				Long readTaskId = null != obj[8] ? Long.valueOf(obj[8]
						.toString()) : null;
				String readTaskActive = null != obj[9] ? obj[9].toString()
						: null;
				String readTaskIdCode = null != obj[10] ? obj[10].toString()
						: null;
				String readTaskName = null != obj[11] ? obj[11].toString()
						: null;
				Long updateTaskId = null != obj[12] ? Long.valueOf(obj[12]
						.toString()) : null;
				String updateTaskActive = null != obj[13] ? obj[13].toString()
						: null;
				String updateTaskCode = null != obj[14] ? obj[14].toString()
						: null;
				String updateTaskName = null != obj[15] ? obj[15].toString()
						: null;
				Long deleteTaskId = null != obj[16] ? Long.valueOf(obj[16]
						.toString()) : null;
				String deleteTaskActive = null != obj[17] ? obj[17].toString()
						: null;
				String deleteTaskCode = null != obj[18] ? obj[18].toString()
						: null;
				String deleteTaskName = null != obj[19] ? obj[19].toString()
						: null;
				String taskCrudName = null != obj[20] ? obj[20].toString()
						: null;
				roleCode = null != obj[21] ? obj[21].toString() : null;

				tempJO.put("roleTaskCrudActiveId", roleTaskCrudActiveId);
				tempJO.put("taskCrudId", taskCrudId);
				tempJO.put("taskCrudName", taskCrudName);
				tempJO.put("createTaskId", createTaskId);
				tempJO.put("createTaskActive", createTaskActive);
				tempJO.put("createTaskCode", createTaskCode);
				tempJO.put("createTaskName", createTaskName);
				tempJO.put("readTaskId", readTaskId);
				tempJO.put("readTaskActive", readTaskActive);
				tempJO.put("readTaskIdCode", readTaskIdCode);
				tempJO.put("readTaskName", readTaskName);
				tempJO.put("updateTaskId", updateTaskId);
				tempJO.put("updateTaskActive", updateTaskActive);
				tempJO.put("updateTaskCode", updateTaskCode);
				tempJO.put("updateTaskName", updateTaskName);
				tempJO.put("deleteTaskId", deleteTaskId);
				tempJO.put("deleteTaskActive", deleteTaskActive);
				tempJO.put("deleteTaskCode", deleteTaskCode);
				tempJO.put("deleteTaskName", deleteTaskName);


				outputBo.add(tempJO);
			}

			jsonOutput.put("roleId", roleIdResult);
			jsonOutput.put("roleName", roleName);
			jsonOutput.put("roleCode", roleCode);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			jsonOutput.put("serviceOutput", outputBo);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}

		return jsonOutput;
	}

}
