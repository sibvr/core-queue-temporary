package com.mlpt.bvnextgen.resource.userManagement;

import org.springframework.security.core.userdetails.User;

import com.mlpt.bvnextgen.model.userManagement.LoginUser;

/**
 * 
 * @author agus.wijanarko
 * ambil info user yg akan digunakan oleh config user oauth2
 */

public class CustomUser extends User {
   private static final long serialVersionUID = 1L;
   public CustomUser(LoginUser loginUser) {
      super(loginUser.getUsername(), loginUser.getOauthPassword(), loginUser.getGrantedAuthoritiesList());
   }
}