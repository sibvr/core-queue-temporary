package com.mlpt.bvnextgen.resource.userManagement;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateUser.CreateUserEntity;
import com.mlpt.bvnextgen.controller.entitySwagger.userManagement.CreateUser.CreateUserTaskCrudActiveEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.RoleDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActiveDao;
import com.mlpt.bvnextgen.model.userManagement.RoleTaskDao;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.model.userManagement.UserRole;
import com.mlpt.bvnextgen.model.userManagement.UserRoleDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.DateUtil;
import com.mlpt.bvnextgen.resource.core.Md5Util;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.wijanarko BO untuk create user termasuk create client
 */

@Service
public class CreateUser implements DataProcess {

	@Autowired
	UserDao userDao;

	@Autowired
	CreateClient createClient;

	@Autowired
	UserRoleDao userRoleDao;

	@Autowired
	RoleDao roleDao;

	@Autowired
	RoleTaskCrudActiveDao roleTaskCrudActiveDao;

	@Autowired
	RoleTaskDao roleTaskDao;

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject processBo(JSONObject serviceInput) {

		// declaration
		Gson gson = new Gson();
		String jsonStr = "";

		UserEntity userAddData = new UserEntity();
		UserRole userRoleAddData = new UserRole();
		Role roleAddData = new Role();
		RoleTaskCrudActive roleTaskCrudActiveAddData = new RoleTaskCrudActive();
		
		List<CreateUserTaskCrudActiveEntity> taskCrudActiveList = new ArrayList<CreateUserTaskCrudActiveEntity>();

		// Validation

		Field[] fields = CreateUserEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data

		jsonStr = gson.toJson(serviceInput);
		CreateUserEntity data = gson.fromJson(jsonStr, CreateUserEntity.class);

		String username = data.getUsername();
		String password = data.getPassword();
		String email = data.getEmail();
		
		Long roleIdAdd = data.getRoleId();
		String flagNewRole =data.getFlagNewRole();
		Long createUserId =data.getCreateUserId();
		Long managerId =data.getManagerId();
		String userFullname = data.getUserFullname();
		
		
		
		taskCrudActiveList = data.getTaskCrudActiveList();
		
		String datetime = DateUtil.getDatetimeNow();
		String newRoleCode = new String();
		String newRoleName = new String();
		String errorMessage;

		JSONObject jsonOutput = new JSONObject();
		JSONObject jsonInput = new JSONObject();
		JSONObject serviceOutput = new JSONObject();

		System.out.println("masuk");

		try {
			Query checkUser = userDao.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME
					+ " where username = :username ");
			checkUser.setParameter("username", username);
			Long resultUsername = (Long) checkUser.uniqueResult();

			userDao.closeSessionCreateQuery();
			
			Query checkEmail = userDao.createQuery("Select count(1) from " + UserEntity.ENTITY_NAME
					+ " where email = :email ");
			checkEmail.setParameter("email", email);
			Long resultEmail = (Long) checkEmail.uniqueResult();

			userDao.closeSessionCreateQuery();
			
			if (resultUsername == 0 && resultEmail ==0) {

				// cek flag new role
				if (MasterGeneralConstants.YES.equals(flagNewRole)) {
					// System.out.println("flagNewRole " + flagNewRole);
					// if Y, insert ke m_role

					// additional data
					newRoleCode = String.valueOf(serviceInput.get("roleCode"));
					newRoleName = String.valueOf(serviceInput.get("roleName"));

					// mapping data
					roleAddData.setId(null);
					roleAddData.setRoleCode(newRoleCode);
					roleAddData.setRoleName(newRoleName);
					roleAddData.setActive(MasterGeneralConstants.YES);
					roleAddData.setCreateUserId(createUserId);
					roleAddData.setCreateDatetime(datetime);

					// insert ke m_role
					roleDao.add(roleAddData);

					// baca template m_task_crud yg dapet dari UI
					// parse dulu

					if (taskCrudActiveList != null) {
						for (CreateUserTaskCrudActiveEntity taskCrudActive : taskCrudActiveList) {

							roleIdAdd = roleAddData.getId();
							Long taskCrudId = taskCrudActive.getTaskCrudId();
							String createTaskActive = taskCrudActive.getCreateTaskActive();
							String readTaskActive = taskCrudActive.getReadTaskActive();
							String updateTaskActive =taskCrudActive.getUpdateTaskActive();
							String deleteTaskActive = taskCrudActive.getDeleteTaskActive();

							// insert ke m_role_task_crud_active
							// mapping data
							roleTaskCrudActiveAddData.setId(null);
							roleTaskCrudActiveAddData.setRoleId(roleIdAdd);
							roleTaskCrudActiveAddData.setTaskCrudId(taskCrudId);
							roleTaskCrudActiveAddData.setCreateTaskActive(createTaskActive);
							roleTaskCrudActiveAddData.setReadTaskActive(readTaskActive);
							roleTaskCrudActiveAddData.setUpdateTaskActive(updateTaskActive);
							roleTaskCrudActiveAddData.setDeleteTaskActive(deleteTaskActive);
							roleTaskCrudActiveAddData.setActive(MasterGeneralConstants.YES);
							roleTaskCrudActiveAddData.setCreateDatetime(datetime);
							roleTaskCrudActiveAddData.setCreateUserId(createUserId);

						}
					}

				}

				// ada client
				jsonInput.put("clientId", username);
				jsonInput.put("clientSecret", Md5Util.getMD5(password));
				createClient.processBo(jsonInput);

				// add m_user
				// mapping data
//				userAddData.setManagerId(managerId);
				userAddData.setUsername(username);
				userAddData.setPassword(Md5Util.getMD5(password));
				userAddData.setEmail(email);
				userAddData.setUserFullname(userFullname);
				userAddData.setPhone(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setAddress(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setVillage(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setDistrict(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setCity(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setProvince(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setUserImage(MasterGeneralConstants.EMPTY_VALUE);
				userAddData.setActive(MasterGeneralConstants.YES);
				userAddData.setUserRole(String.valueOf(roleIdAdd));
				userAddData.setCreateUserId(createUserId);
				userAddData.setCreateDatetime(datetime);
				userAddData.setStatusLogin(MasterGeneralConstants.NO);

				// tes
				// System.out.println("userAddData :: " + userAddData);

				// insert ke dalam table
				userDao.add(userAddData);

				// get user id hasil insert
				Long userIdAdd = userAddData.getId();
				
				userAddData.setId(userIdAdd);

				// add user role
				// mapping data
				userRoleAddData.setId(null);
				userRoleAddData.setUserId(userIdAdd);
				userRoleAddData.setRoleId(roleIdAdd);
				userRoleAddData.setCreateUserId(createUserId);
				userRoleAddData.setCreateDatetime(datetime);
				userRoleAddData.setFlgDefaultRole(MasterGeneralConstants.YES);

				// insert ke dalam table
				userRoleDao.add(userRoleAddData);

				jsonOutput.put("userAddData", userAddData);
				jsonOutput.put("userRoleAddData", userRoleAddData);

				jsonOutput.put("serviceInput", serviceInput);
				jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
				// jsonOutput.put("serviceOutput", "OK");
			} else {
				jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				jsonOutput.put("errorMessage", "username or email already use");
			}

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}

		return jsonOutput;
	}

}
