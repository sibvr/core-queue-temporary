package com.mlpt.bvnextgen.resource.userManagement;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.InquiryEntity;
import com.mlpt.bvnextgen.model.userManagement.Role;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko get user by Actie Code
 */

@Service
public class GetUserByActive implements DataProcess {

	@Autowired
	UserDao userDao;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject processBo(JSONObject serviceInput) {

		// Validation
		Field[] fields = InquiryEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for (Field field : fields) {

			field.setAccessible(true);
			validationFieldList.add(field.getName());
		}
		ValidationUtil.checkForUnsupportedParameters(serviceInput, validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput, validationFieldList);

		// prepare data
		String status = String.valueOf(serviceInput.get("parameter"));

		List<Object[]> result = null;
		List<JSONObject> outputBo = new ArrayList<JSONObject>();
		JSONObject jsonOutput = new JSONObject();

		try {
			Query query = userDao
					.createQuery("SELECT A.id AS user_id, A.username AS username FROM " + UserEntity.ENTITY_NAME + " A, "
							+ Role.ENTITY_NAME + " B " + " WHERE A.userRole = B.id AND B.active = :active "
							+ " AND A.userRole is not null " + " and A.active = :status ");
			query.setParameter("status", status);
			query.setParameter("active", MasterGeneralConstants.YES);
			result = query.list();
			userDao.closeSessionCreateQuery();
			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				JSONObject tempJO = new JSONObject();
				Long id = null != obj[0] ? Long.valueOf(obj[0].toString()) : null;
				String username = null != obj[1] ? obj[1].toString() : null;
				tempJO.put("userId", id);
				tempJO.put("username", username);
				outputBo.add(tempJO);
			}

			jsonOutput.put("serviceOutput", outputBo);
			jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
			String errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			jsonOutput = new JSONObject();
			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
		}
		return jsonOutput;
	}

}
