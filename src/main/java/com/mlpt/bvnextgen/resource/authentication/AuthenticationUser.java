package com.mlpt.bvnextgen.resource.authentication;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.model.userManagement.UserDao;
import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import javax.sql.DataSource;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;

/**
 *
 * @author agus.wijanarko bo ini digunakan untuk authentikasi user yg mengakses
 *         API. cek id dengan token
 *
 */

@Service
public class AuthenticationUser {

	@Autowired
	UserDao userDao;

	@Autowired
	DataSource ds;

	@Bean
	public TokenStore tokenStore4() {
		return new JdbcTokenStore(ds);
	}

	public JSONObject AuthenticationUser(Long userID) {

		UserEntity user = new UserEntity();
		String errorMessage = "";
		JSONObject jsonOutput = new JSONObject();
		try {
			// get the current authentication
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			OAuth2Authentication oAuth2 = ((OAuth2Authentication) authentication);

			String username = oAuth2.getName();

			user = userDao.find(userID);

			if (user == null) {
				jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				jsonOutput.put("errorMessage", "userID not found");
			}

			else if (user.getUsername().equals(username)) {
				jsonOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
			}

			else {
				jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				jsonOutput.put("errorMessage", "invalid token access or userID");
			}

			return jsonOutput;

		} catch (Exception e) {
			e.printStackTrace();

			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring((errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();

			jsonOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			jsonOutput.put("errorMessage", errorMessage);
			return jsonOutput;
		}

	}

}
