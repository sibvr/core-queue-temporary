package com.mlpt.bvnextgen.resource.queueNumber;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueNumber.ConsumeQueueNumberEntity;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini akan mengupdate nomor antrian
 *
 */


@Service
public class ConsumeQueueNumber implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = ConsumeQueueNumberEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			String userId = serviceInput.get("userId").toString();
			String type = serviceInput.get("type").toString().toLowerCase();
			String counter = serviceInput.get("counter").toString();
			
			Query checkData = queueNumberTellerDao.createQuery
					("Select max(queueingNumber) from " + QueueNumberTeller.ENTITY_NAME
					+ " where consumeStatus = :consumeStatus and queueingDate = :queueingDate"
					+ " and type = :type");
			checkData.setParameter("consumeStatus", MasterGeneralConstants.YES);
			checkData.setParameter("queueingDate", formatter.format(date));
			checkData.setParameter("type", type);
			Long queueingNumber = (Long) checkData.uniqueResult();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			
			if(queueingNumber != null){
					
				checkData = queueNumberTellerDao.createQuery
						("update " + QueueNumberTeller.ENTITY_NAME
						+ " set consumeId = :consumeId, "
						+ " consumeStatus = :consumeStatus, "
						+ " counter = :counter "
						+ " where queueingDate = :queueingDate "
						+ " and queueingNumber = :queueingNumber"
						+ " and type = :type");
				checkData.setParameter("consumeId", userId);
				checkData.setParameter("consumeStatus", MasterGeneralConstants.YES);
				checkData.setParameter("counter", counter);
				checkData.setParameter("queueingDate", formatter.format(date));
				checkData.setParameter("queueingNumber", queueingNumber+1);
				checkData.setParameter("type", type);
				checkData.executeUpdate();
				
				queueNumberTellerDao.closeSessionCreateQuery();
			}else{
				
				checkData = queueNumberTellerDao.createQuery
						("update " + QueueNumberTeller.ENTITY_NAME
						+ " set consumeId = :consumeId, "
						+ " consumeStatus = :consumeStatus, "
						+ " counter = :counter "
						+ " where queueingDate = :queueingDate "
						+ " and queueingNumber = :queueingNumber"
						+ " and type = :type");
				checkData.setParameter("consumeId", userId);
				checkData.setParameter("consumeStatus", MasterGeneralConstants.YES);
				checkData.setParameter("counter", counter);
				checkData.setParameter("queueingDate", formatter.format(date));
				checkData.setParameter("queueingNumber", 1L);
				checkData.setParameter("type", type);
				checkData.executeUpdate();
				
				queueNumberTellerDao.closeSessionCreateQuery();
			}
			
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	

}
