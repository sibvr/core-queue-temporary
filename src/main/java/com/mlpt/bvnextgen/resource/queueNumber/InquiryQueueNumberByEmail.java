package com.mlpt.bvnextgen.resource.queueNumber;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueNumber.InquiryQueueNumberByEmailEntity;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini akan melihat nomor antrian berdasarkan email
 *
 */


@Service
public class InquiryQueueNumberByEmail implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = InquiryQueueNumberByEmailEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			String email = serviceInput.get("email").toString();
			String type = serviceInput.get("type").toString().toLowerCase();
			
			Query checkData = queueNumberTellerDao.createQuery
					("Select queueingNumber from " + QueueNumberTeller.ENTITY_NAME
					+ " where queueingDate = :queueingDate and accountName = :accountName"
					+ " and type =:type");
			checkData.setParameter("accountName", email);
			checkData.setParameter("queueingDate", formatter.format(date));
			checkData.setParameter("type", type);
			Long queueingNumber = (Long) checkData.uniqueResult();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			
			if(queueingNumber != null){
				resultOutput.put("queueNumberTeller", String.format("%03d", queueingNumber));
				
			}else{
				resultOutput.put("queueNumberTeller", String.format("%03d", 0));
			}
			
			
//			checkData = queueNumberTellerDao.createQuery
//					("Select queueingNumber, comsumeStatus from " + QueueNumberTeller.ENTITY_NAME
//					+ " where queueingDate = :queueingDate and accountName = :accountName");
//			checkData.setParameter("accountName", email);
//			checkData.setParameter("queueingDate", formatter.format(date));
//			Long queueingNumber = (Long) checkData.uniqueResult();
//			
//			queueNumberTellerDao.closeSessionCreateQuery();
//			
//			if(queueingNumber != null){
//				resultOutput.put("queueNumberTeller", String.format("%03d", queueingNumber));
//				
//			}else{
//				resultOutput.put("queueNumberTeller", String.format("%03d", 0));
//			}
			
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	

}
