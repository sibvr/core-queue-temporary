package com.mlpt.bvnextgen.resource.queueNumber;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueNumber.GenerateQueueNumberEntity;
import com.mlpt.bvnextgen.model.configuration.Configuration;
import com.mlpt.bvnextgen.model.configuration.ConfigurationDao;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 14.12.2020
 * BO ini akan membuat nomor antrian baru
 *
 */


@Service
public class GenerateQueueNumber implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;

	@Autowired
	ConfigurationDao configurationDao;
	
	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = GenerateQueueNumberEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			String email = serviceInput.get("email").toString();
			String type = serviceInput.get("type").toString().toLowerCase();
			
			Query checkSetting = configurationDao.createQuery("Select configValue from " + Configuration.ENTITY_NAME
					+ " where configMenu = :type and subconfig = :subconfig ");
			checkSetting.setParameter("type", type.toUpperCase());
			checkSetting.setParameter("subconfig", "statusQueue");
			String statusQueue = (String) checkSetting.uniqueResult();
			
			configurationDao.closeSessionCreateQuery();
			
			if(statusQueue.equalsIgnoreCase("ON")){
				String status = "S";
				Query checkData = queueNumberTellerDao.createQuery("Select count(1) from " + QueueNumberTeller.ENTITY_NAME
						+ " where type = :type and queueingDate = :queueingDate");
				checkData.setParameter("type", type);
				checkData.setParameter("queueingDate", formatter.format(date));
				Long totalData = (Long) checkData.uniqueResult();
				
				queueNumberTellerDao.closeSessionCreateQuery();
				
				if(totalData==0){
					QueueNumberTeller queueNumberTeller = new QueueNumberTeller();
					queueNumberTeller.setQueueingNumber(1L);
					queueNumberTeller.setAccountName(email);
					queueNumberTeller.setConsumeStatus("N");
					queueNumberTeller.setQueueingDate(formatter.format(date));
					queueNumberTeller.setType(type);
					
					queueNumberTellerDao.add(queueNumberTeller);
				}else{
					checkData = queueNumberTellerDao.createQuery
							("Select queueingNumber,1 from " + QueueNumberTeller.ENTITY_NAME
							+ " where accountName = :accountName and queueingDate = :queueingDate "
							+ " and type = :type");
					checkData.setParameter("accountName", email);
					checkData.setParameter("queueingDate", formatter.format(date));
					checkData.setParameter("type", type);
					result = checkData.list();
					
					queueNumberTellerDao.closeSessionCreateQuery();
					
					if(result.size()>0){
						Iterator iter = result.iterator();
						while (iter.hasNext()) {
							Object[] obj = (Object[]) iter.next();
							Long queueingNumber = obj[0] != null ? Long.valueOf(obj[0].toString()): null;
							
							checkData = queueNumberTellerDao.createQuery
									("Select queueingNumber,1 from " + QueueNumberTeller.ENTITY_NAME
									+ " where accountName = :accountNameBlank and queueingDate = :queueingDate "
									+ " and queueingNumber > :queueingNumberAwal"
									+ " and consumeStatus = :consumeStatus"
									+ " and type = :type order by queueingNumber ASC");
							checkData.setParameter("accountNameBlank", "");
							checkData.setParameter("queueingDate", formatter.format(date));
							checkData.setParameter("queueingNumberAwal", queueingNumber);
							checkData.setParameter("consumeStatus", MasterGeneralConstants.NO);
							checkData.setParameter("type", type);
							checkData.setMaxResults(1);
							result = checkData.list();
							
							queueNumberTellerDao.closeSessionCreateQuery();
							
							status = generateNumber(result,email,formatter.format(date),type);
							
							//update last
							if(status.equalsIgnoreCase(MasterGeneralConstants.STATUS_SUCCESS)){
								checkData = queueNumberTellerDao.createQuery
										("update " + QueueNumberTeller.ENTITY_NAME
										+ " set accountName = :accountNameBlank "
										+ " where queueingDate = :queueingDate "
										+ " and queueingNumber = :queueingNumberAwal"
										+ " and type = :type");
								checkData.setParameter("accountNameBlank", "");
								checkData.setParameter("queueingDate", formatter.format(date));
								checkData.setParameter("queueingNumberAwal", queueingNumber);
								checkData.setParameter("type", type);
								checkData.executeUpdate();
								
								queueNumberTellerDao.closeSessionCreateQuery();
							}
						}
					}else{
						checkData = queueNumberTellerDao.createQuery
								("Select queueingNumber,1 from " + QueueNumberTeller.ENTITY_NAME
								+ " where accountName = :accountName and queueingDate = :queueingDate"
								+ " and consumeStatus = :consumeStatus"
								+ " and type = :type order by queueingNumber ASC");
						checkData.setParameter("accountName", "");
						checkData.setParameter("queueingDate", formatter.format(date));
						checkData.setParameter("consumeStatus", MasterGeneralConstants.NO);
						checkData.setParameter("type", type);
						checkData.setMaxResults(1);
						result = checkData.list();
						
						queueNumberTellerDao.closeSessionCreateQuery();
						
						status = generateNumber(result,email,formatter.format(date),type);
					}
				}
				
				
				if(status.equalsIgnoreCase(MasterGeneralConstants.STATUS_SUCCESS)){
					resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);
				}else{
					resultOutput.put("errorMessage", "Antrian sudah penuh !");
					resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
				}
				
			
			}else{
				resultOutput.put("errorMessage", "Antrian belum dibuka !");
				resultOutput.put("status", MasterGeneralConstants.STATUS_FAILED);
			}

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	
	public String generateNumber(List<Object[]> result, String email, String date, String type){
		if(result.size()>0){
			Iterator iter = result.iterator();
			while (iter.hasNext()) {
				Object[] obj = (Object[]) iter.next();
				Long queueingNumber = obj[0] != null ? Long.valueOf(obj[0].toString()): null;
				
				Query updateData = queueNumberTellerDao.createQuery
						("update " + QueueNumberTeller.ENTITY_NAME
						+ " set accountName = :accountName "
						+ " where queueingDate = :queueingDate and queueingNumber = :queueingNumber"
						+ " and type = :type");
				updateData.setParameter("accountName", email);
				updateData.setParameter("queueingDate", date);
				updateData.setParameter("queueingNumber", queueingNumber);
				updateData.setParameter("type", type);
				
				updateData.executeUpdate();
				
				queueNumberTellerDao.closeSessionCreateQuery();
				
				
			}
			
			return "S";
		}else{
			Query checkQueue = configurationDao.createQuery("Select configValue from " + Configuration.ENTITY_NAME
					+ " where configMenu = :type and subconfig = :subconfig ");
			checkQueue.setParameter("type", type.toUpperCase());
			checkQueue.setParameter("subconfig", "maxQueue");
			String maxQueue = (String) checkQueue.uniqueResult();
			
			configurationDao.closeSessionCreateQuery();
			
			Query checkData = queueNumberTellerDao.createQuery
					("Select max(queueingNumber) from " + QueueNumberTeller.ENTITY_NAME
					+ " where queueingDate = :queueingDate "
					+ " and type = :type");
			checkData.setParameter("queueingDate", date);
			checkData.setParameter("type", type);
			Long maxNumber = (Long) checkData.uniqueResult();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			
			if(Long.valueOf(maxQueue) > maxNumber+1){
				QueueNumberTeller queueNumberTeller = new QueueNumberTeller();
				queueNumberTeller.setQueueingNumber(maxNumber+1);
				queueNumberTeller.setAccountName(email);
				queueNumberTeller.setConsumeStatus("N");
				queueNumberTeller.setQueueingDate(date);
				queueNumberTeller.setType(type);
				
				queueNumberTellerDao.add(queueNumberTeller);
				
				return "S";
			}else{
				return "F";
			}
		}
		
		
	}

}
