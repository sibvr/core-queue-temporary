package com.mlpt.bvnextgen.resource.queueNumber;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.query.Query;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlpt.bvnextgen.config.MasterGeneralConstants;
import com.mlpt.bvnextgen.controller.entitySwagger.queueNumber.GetCurrentQueueNumberEntity;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTeller;
import com.mlpt.bvnextgen.model.queueModel.QueueNumberTellerDao;
import com.mlpt.bvnextgen.resource.core.DataProcess;
import com.mlpt.bvnextgen.resource.core.ValidationUtil;

/**
 *
 * @author agus.nurkhakim 15.12.2020
 * BO ini akan melihat nomor antrian berdasarkan email
 *
 */


@Service
public class GetCurrentQueueNumber implements DataProcess{

	@Autowired
	QueueNumberTellerDao queueNumberTellerDao;


	@Override
	public JSONObject processBo(JSONObject serviceInput) {
		JSONObject resultOutput = new JSONObject();

		//Validation
		Field[] fields = GetCurrentQueueNumberEntity.class.getDeclaredFields();
		List<String> validationFieldList = new ArrayList<String>();

		for(Field field : fields) {                           

		    field.setAccessible(true);                       
		    validationFieldList.add(field.getName());
		}

		ValidationUtil.checkForUnsupportedParameters(serviceInput,validationFieldList);
		ValidationUtil.valBlankOrNullList(serviceInput,validationFieldList);

		//main
		try{
			List<Object[]> result = null;
			Date date = new Date();
			List<JSONObject> ListResult = new ArrayList<JSONObject>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			
			String type = serviceInput.get("type").toString().toLowerCase();
			
			Query checkData = queueNumberTellerDao.createQuery
					("Select max(queueingNumber) from " + QueueNumberTeller.ENTITY_NAME
					+ " where queueingDate = :queueingDate"
					+ " and type =:type and consumeStatus = :consumeStatus");
			checkData.setParameter("queueingDate", formatter.format(date));
			checkData.setParameter("type", type);
			checkData.setParameter("consumeStatus", MasterGeneralConstants.YES);
			Long queueingNumber = (Long) checkData.uniqueResult();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			
			if(queueingNumber != null){
				resultOutput.put("currentQueueNumber", String.format("%03d", queueingNumber));
				
			}else{
				resultOutput.put("currentQueueNumber", String.format("%03d", 0));
			}
			
			checkData = queueNumberTellerDao.createQuery
					("Select max(queueingNumber) from " + QueueNumberTeller.ENTITY_NAME
					+ " where queueingDate = :queueingDate"
					+ " and type =:type");
			checkData.setParameter("queueingDate", formatter.format(date));
			checkData.setParameter("type", type);
			Long maxNumber = (Long) checkData.uniqueResult();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			
			if(maxNumber != null){
				resultOutput.put("totalQueueNumber", String.format("%03d", maxNumber));
				
			}else{
				resultOutput.put("totalQueueNumber", String.format("%03d", 0));
			}
			
			checkData = queueNumberTellerDao.createQuery
					("Select max(queueingNumber),counter from " + QueueNumberTeller.ENTITY_NAME
					+ " where queueingDate = :queueingDate"
					+ " and type =:type and consumeStatus = :consumeStatus group by counter ");
			checkData.setParameter("queueingDate", formatter.format(date));
			checkData.setParameter("type", type);
			checkData.setParameter("consumeStatus", MasterGeneralConstants.YES);
			result = checkData.list();
			
			queueNumberTellerDao.closeSessionCreateQuery();
			Iterator iter = result.iterator();
			while(iter.hasNext()){
				Object[] obj = (Object[]) iter.next();
				
				Long counterNumber = null != obj[0] ? Long.valueOf(obj[0].toString())
						: null;
				String counter = null != obj[1] ? obj[1].toString()
						: null;
				
				JSONObject tempJo = new JSONObject();
				
				tempJo.put("counterNumber", String.format("%03d",counterNumber));
				tempJo.put("counter", counter);
				
				ListResult.add(tempJo);
			}
			
			resultOutput.put("counterQueue", ListResult);
			resultOutput.put("status", MasterGeneralConstants.STATUS_SUCCESS);

			return resultOutput;
		}catch(Exception e){
			e.printStackTrace();
			String errorMessage = new String();
			errorMessage = e.getCause().toString();
			errorMessage = errorMessage.substring(
					(errorMessage.indexOf(':') + 1), errorMessage.length());
			errorMessage = errorMessage.trim();
			resultOutput = new JSONObject();
			resultOutput.put("status",MasterGeneralConstants.STATUS_FAILED);
			resultOutput.put("errorMessage",errorMessage);

			e.printStackTrace();

			return resultOutput;
		}
	}
	

}
