package com.mlpt.bvnextgen.model.customLog;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface CustomLogDao extends BaseCoreDao<CustomLog, Long> {

}