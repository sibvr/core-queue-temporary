package com.mlpt.bvnextgen.model.customLog;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class CustomLogDaoImpl extends BaseCoreDaoImpl<CustomLog, Long> implements CustomLogDao {

}