package com.mlpt.bvnextgen.model.queueModel;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface QueueNumberTellerDao extends BaseCoreDao<QueueNumberTeller, Long> {

}