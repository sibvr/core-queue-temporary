package com.mlpt.bvnextgen.model.queueModel;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;


@Repository
public class QueueNumberTellerDaoImpl extends BaseCoreDaoImpl<QueueNumberTeller, Long> implements QueueNumberTellerDao {

}