package com.mlpt.bvnextgen.model.queueModel;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name=QueueNumberTeller.ENTITY_NAME)
@Table(name=QueueNumberTeller.TABLE_NAME)
public class QueueNumberTeller implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "queueing_number_teller";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.QueueingNumberTeller";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="queueing_id")
	private Long queueingId;	

	@Column(name="queueing_number")
	private Long queueingNumber;

	@Column(name="account_name")
	private String accountName;
	
	@Column(name="consume_status")
	private String consumeStatus;
	
	@Column(name="consume_id")
	private String consumeId;
	
	@Column(name="queueing_date")
	private String queueingDate;
	
	
	@Column(name="type")
	private String type;
	
	@Column(name="counter")
	private String counter;

	public Long getQueueingId() {
		return queueingId;
	}

	public void setQueueingId(Long queueingId) {
		this.queueingId = queueingId;
	}

	public Long getQueueingNumber() {
		return queueingNumber;
	}

	public void setQueueingNumber(Long queueingNumber) {
		this.queueingNumber = queueingNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getConsumeStatus() {
		return consumeStatus;
	}

	public void setConsumeStatus(String consumeStatus) {
		this.consumeStatus = consumeStatus;
	}

	public String getConsumeId() {
		return consumeId;
	}

	public void setConsumeId(String consumeId) {
		this.consumeId = consumeId;
	}

	public String getQueueingDate() {
		return queueingDate;
	}

	public void setQueueingDate(String queueingDate) {
		this.queueingDate = queueingDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}
	
	
	
}
