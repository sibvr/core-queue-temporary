package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface UserDao extends BaseCoreDao<UserEntity, Long> {

}