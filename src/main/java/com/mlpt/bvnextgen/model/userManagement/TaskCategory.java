package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table m_task_category
 * @author hans.kristianto
 * @version 1.0.0
 *
 */

@Entity(name=TaskCategory.ENTITY_NAME)
@Table(name=TaskCategory.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class TaskCategory implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "m_task_category";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.model.userManagement.TaskCategory";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	
	@Column(name="task_category_id")
	private Long id;
	
	
	@Column(name="task_category_code")
	private String taskCategoryCode;
	
	
	@Column(name="task_category_name")
	private String taskCategoryName;
	
	
	@Column(name="active")
	private String active;
	
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	
	@Column(name="update_user_id")
	private Long updateUserId;

}
