package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;



public interface TaskCategoryDao extends BaseCoreDao<TaskCategory, Long> {

}