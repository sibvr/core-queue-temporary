package com.mlpt.bvnextgen.model.userManagement;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;

@Repository
public class LogAPIAccessByUserDaoImpl extends BaseCoreDaoImpl<LogAPIAccessByUser, Long> implements LogAPIAccessByUserDao {

}