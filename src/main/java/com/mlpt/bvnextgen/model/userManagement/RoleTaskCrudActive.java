package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name=RoleTaskCrudActive.ENTITY_NAME)
@Table(name=RoleTaskCrudActive.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class RoleTaskCrudActive implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "m_role_task_crud_active";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.model.userManagement.RoleTaskCrudActive";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="role_task_crud_active_id")
	private Long id;
	
	@Column(name="role_id")
	private Long roleId;
	
	@Column(name="task_crud_id")
	private Long taskCrudId;
	
	@Column(name="create_task_active")
	private String createTaskActive;
	
	@Column(name="read_task_active")
	private String readTaskActive;
	
	@Column(name="update_task_active")
	private String updateTaskActive;
	
	@Column(name="delete_task_active")
	private String deleteTaskActive;
	
	@Column(name="active")
	private String active;
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	@Column(name="update_user_id")
	private Long updateUserId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getTaskCrudId() {
		return taskCrudId;
	}

	public void setTaskCrudId(Long taskCrudId) {
		this.taskCrudId = taskCrudId;
	}

	public String getCreateTaskActive() {
		return createTaskActive;
	}

	public void setCreateTaskActive(String createTaskActive) {
		this.createTaskActive = createTaskActive;
	}

	public String getReadTaskActive() {
		return readTaskActive;
	}

	public void setReadTaskActive(String readTaskActive) {
		this.readTaskActive = readTaskActive;
	}

	public String getUpdateTaskActive() {
		return updateTaskActive;
	}

	public void setUpdateTaskActive(String updateTaskActive) {
		this.updateTaskActive = updateTaskActive;
	}

	public String getDeleteTaskActive() {
		return deleteTaskActive;
	}

	public void setDeleteTaskActive(String deleteTaskActive) {
		this.deleteTaskActive = deleteTaskActive;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

}
