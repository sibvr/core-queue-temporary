package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;



public interface TaskDao extends BaseCoreDao<Task, Long> {

}