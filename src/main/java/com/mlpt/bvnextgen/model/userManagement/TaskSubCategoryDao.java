package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;



public interface TaskSubCategoryDao extends BaseCoreDao<TaskSubCategory, Long> {

}