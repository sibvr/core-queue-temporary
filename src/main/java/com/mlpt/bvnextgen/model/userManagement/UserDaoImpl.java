package com.mlpt.bvnextgen.model.userManagement;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.userManagement.UserEntity;
import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;;

@Repository
public class UserDaoImpl extends BaseCoreDaoImpl<UserEntity, Long> implements UserDao {

}