package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;



public interface UserRoleDao extends BaseCoreDao<UserRole, Long> {

}