package com.mlpt.bvnextgen.model.userManagement;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;

@Repository
public class TaskDaoImpl extends BaseCoreDaoImpl<Task, Long> implements TaskDao {

}