package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table m_user
 * @author hans.kristianto
 * @version 1.0.0
 *
 */

@Entity(name=LogAPIAccessByUser.ENTITY_NAME)
@Table(name=LogAPIAccessByUser.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class LogAPIAccessByUser implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "log_api_access_by_user_id";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.model.userManagement.LogAPIAccess";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="log_api_access_by_user_id")
	private Long id;
	
	@Column(name="user_id")
	private Long userId;
	
	@Column(name="role_id")
	private Long roleId;
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	@Column(name="ip_address")
	private String ipAddress;
	
	@Column(name="token")
	private String token;
	
	@Column(name="api_accessed")
	private String apiAccessed;
	
	@Column(name="task_category")
	private String taskCategory;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="url_accessed")
	private String urlAccessed;
	
	@Column(name="datetime")
	private String datetime;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}
	
	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public String getApiAccessed() {
		return apiAccessed;
	}

	public void setApiAccessed(String apiAccessed) {
		this.apiAccessed = apiAccessed;
	}
	
	public String getTaskCategory() {
		return taskCategory;
	}

	public void setTaskCategory(String taskCategory) {
		this.taskCategory = taskCategory;
	}
	
	public String getUrlAccessed() {
		return urlAccessed;
	}

	public void setUrlAccessed(String urlAccessed) {
		this.urlAccessed = urlAccessed;
	}
	
	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	
	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	
}
