package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;


public interface RoleDao extends BaseCoreDao<Role, Long> {

}