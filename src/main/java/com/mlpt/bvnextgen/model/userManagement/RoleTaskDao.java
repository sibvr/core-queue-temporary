package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;


public interface RoleTaskDao extends BaseCoreDao<RoleTask, Long> {

}