package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table t_role_task
 * @author hans.kristianto
 * @version 1.0.0
 *
 */

@Entity(name=RoleTask.ENTITY_NAME)
@Table(name=RoleTask.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class RoleTask implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "t_role_task";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.RoleTask";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="role_task_id")
	private Long id;
	
	@Column(name="role_id")
	private Long roleId;
	
	@Column(name="task_id")
	private Long taskId;
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	@Column(name="update_user_id")
	private Long updateUserId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
	
}
