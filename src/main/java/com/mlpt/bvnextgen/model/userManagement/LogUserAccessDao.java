package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface LogUserAccessDao extends BaseCoreDao<LogUserAccess, Long> {

}