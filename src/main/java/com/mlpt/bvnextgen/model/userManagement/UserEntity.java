package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

/**
 * Entity class for table m_user
 * 
 * @author hans.kristianto
 * @version 1.0.0
 *
 */

@Entity(name = UserEntity.ENTITY_NAME)
@Table(name = UserEntity.TABLE_NAME)
/* @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) */
public class UserEntity implements Serializable {

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "m_user";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.model.userManagement.UserEntity";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "user_id")
	private Long id;

//	@Column(name = "manager_id")
//	private Long managerId;

	@Column(name = "username")
	private String username;

	@Column(name = "user_fullname")
	private String userFullname;

	@Column(name = "password")
	private String password;

	@Column(name = "email")
	private String email;

	@Column(name = "active")
	private String active;

	@Column(name = "create_datetime")
	private String createDatetime;

	@Column(name = "create_user_id")
	private Long createUserId;

	@Column(name = "update_datetime")
	private String updateDatetime;

	@Column(name = "update_user_id")
	private Long updateUserId;

	@Column(name = "branch_code")
	private String branchCode;

	@Column(name = "phone")
	private String phone;

	@Column(name = "address")
	private String address;

	@Column(name = "village")
	private String village;

	@Column(name = "district")
	private String district;

	@Column(name = "city")
	private String city;

	@Column(name = "province")
	private String province;

	@Column(name = "user_image")
	private String userImage;

	@Column(name = "user_role")
	private String userRole;

	// tambahan baru hakim untuk single sign on
	@Column(name = "status_login")
	private String statusLogin;
	
	@Column(name = "confirmation_code")
	private String confirmationCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserFullname() {
		return userFullname;
	}

	public void setUserFullname(String userFullname) {
		this.userFullname = userFullname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	// tambahan baru hakim untuk single sign on
	public String getStatusLogin() {
		return statusLogin;
	}

	public void setStatusLogin(String statusLogin) {
		this.statusLogin = statusLogin;
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}
	
	

//	public Long getManagerId() {
//		return managerId;
//	}
//
//	public void setManagerId(Long managerId) {
//		this.managerId = managerId;
//	}

}
