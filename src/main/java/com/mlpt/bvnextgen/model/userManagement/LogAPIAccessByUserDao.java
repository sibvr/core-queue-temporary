package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface LogAPIAccessByUserDao extends BaseCoreDao<LogAPIAccessByUser, Long> {

}