package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface ClientDao extends BaseCoreDao<Client, Long> {

}