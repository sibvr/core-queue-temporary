package com.mlpt.bvnextgen.model.userManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;


public interface RoleTaskCrudActiveDao extends BaseCoreDao<RoleTaskCrudActive, Long> {

}