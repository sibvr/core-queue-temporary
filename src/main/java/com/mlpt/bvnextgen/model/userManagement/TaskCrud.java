package com.mlpt.bvnextgen.model.userManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name=TaskCrud.ENTITY_NAME)
@Table(name=TaskCrud.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class TaskCrud implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "m_task_crud";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.model.userManagement.TaskCrud";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	
	@Column(name="task_crud_id")
	private Long id;
	
	
	@Column(name="task_crud_code")
	private String taskCrudCode;
	
	
	@Column(name="task_crud_name")
	private String taskCrudName;
	
	
	@Column(name="task_category_id")
	private Long taskCategoryId;
	
	
	@Column(name="create_task_id")
	private Long createTaskId;
	
	
	@Column(name="read_task_id")
	private Long readTaskId;
	
	
	@Column(name="update_task_id")
	private Long updateTaskId;
	
	
	@Column(name="delete_task_id")
	private Long deleteTaskId;
	
	
	@Column(name="active")
	private String active;
	
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	
	@Column(name="update_user_id")
	private Long updateUserId;

}
