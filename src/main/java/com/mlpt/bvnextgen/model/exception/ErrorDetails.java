package com.mlpt.bvnextgen.model.exception;

import java.util.Date;

/**
 *
 * @author angga.wijaya
 * Format Message Error Exception
 */
public class ErrorDetails {

    private Date timestamp;
    private String message;
    private String details;
    private static final String apiVersion = "v1.0";


    public ErrorDetails(Date timestamp, String message, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public String getApiVersion() {
        return apiVersion;
    }
}