package com.mlpt.bvnextgen.model.configuration;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table m_config
 * @author jonathan.tandarma
 * @version 1.0.0
 *
 */

@Entity(name=Configuration.ENTITY_NAME)
@Table(name=Configuration.TABLE_NAME)
/*@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
public class Configuration implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "m_config";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.Configuration";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="config_id")
	private Long id;
	
	@Column(name="config_menu")
	private String configMenu;
	
	@Column(name="subconfig")
	private String subconfig;
	
	@Column(name="config_value")
	private String configValue;
	
	@Column(name="active")
	private String active;
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	@Column(name="update_user_id")
	private Long updateUserId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConfigMenu() {
		return configMenu;
	}

	public void setConfigMenu(String configMenu) {
		this.configMenu = configMenu;
	}

	public String getSubconfig() {
		return subconfig;
	}

	public void setSubconfig(String subconfig) {
		this.subconfig = subconfig;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
	
}
