package com.mlpt.bvnextgen.model.configuration;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;

@Repository
public class ConfigurationDaoImpl extends BaseCoreDaoImpl<Configuration, Long> implements ConfigurationDao {

}