package com.mlpt.bvnextgen.model.configuration;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface ConfigurationDao extends BaseCoreDao<Configuration, Long> {

}