package com.mlpt.bvnextgen.model.audittrail;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name=Audittrail.ENTITY_NAME)
@Table(name=Audittrail.TABLE_NAME)
public class Audittrail implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "audittrail";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.Audittrail";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="audittrail_id")
	private Long id;	

	@Column(name="primary_key")
	private String primaryKey;
	
	@Column(name="primary_key_secondary")
	private String primaryKeySecondary;
	
	@Column(name="field_name")
	private String fieldName;
	
	@Column(name="original_value")
	private String originalValue;
	
	@Column(name="new_value")
	private String newValue;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="api_name")
	private String apiName;
	
	@Column(name="datetime")
	private String datetime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getPrimaryKeySecondary() {
		return primaryKeySecondary;
	}

	public void setPrimaryKeySecondary(String primaryKeySecondary) {
		this.primaryKeySecondary = primaryKeySecondary;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOriginalValue() {
		return originalValue;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	
}
