package com.mlpt.bvnextgen.model.audittrail;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;


@Repository
public class AudittrailDaoImpl extends BaseCoreDaoImpl<Audittrail, Long> implements AudittrailDao {

}