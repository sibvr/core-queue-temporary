package com.mlpt.bvnextgen.model.audittrail;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface AudittrailDao extends BaseCoreDao<Audittrail, Long> {

}