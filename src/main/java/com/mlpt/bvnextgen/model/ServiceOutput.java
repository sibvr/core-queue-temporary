package com.mlpt.bvnextgen.model;

import java.math.BigInteger;

import org.json.simple.JSONObject;

public class ServiceOutput {

    private BigInteger id;
    
    private String transactionStatus;

    private JSONObject serviceContent;

    public JSONObject getServiceContent() {
		return serviceContent;
	}

	public void setServiceContent(JSONObject serviceContent) {
		this.serviceContent = serviceContent;
	}

	public ServiceOutput() {

    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

}
