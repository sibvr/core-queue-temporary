package com.mlpt.bvnextgen.model.approvalManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface ApprovalDao extends BaseCoreDao<Approval, Long> {

}