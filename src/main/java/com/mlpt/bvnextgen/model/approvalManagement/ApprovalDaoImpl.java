package com.mlpt.bvnextgen.model.approvalManagement;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;


@Repository
public class ApprovalDaoImpl extends BaseCoreDaoImpl<Approval, Long> implements ApprovalDao {

}