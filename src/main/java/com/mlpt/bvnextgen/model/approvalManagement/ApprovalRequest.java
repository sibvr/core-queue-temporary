package com.mlpt.bvnextgen.model.approvalManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name=ApprovalRequest.ENTITY_NAME)
@Table(name=ApprovalRequest.TABLE_NAME)
public class ApprovalRequest implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "t_approval_request";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.ApprovalRequest";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="approval_request_id")
	private Long approvalRequestId;	

	@Column(name="user_id_request")
	private Long userIdRequest;
	
	@Column(name="role_id_request")
	private Long roleIdRequest;
	
	@Column(name="role_id_approve")
	private Long roleIdApprove;
	
	@Column(name="task_id")
	private Long taskId;
	
	@Column(name="json_string_data")
	private String jsonStringData;
	
	@Column(name="json_string_data_before")
	private String jsonStringDataBefore;

	@Column(name="json_string_data_after")
	private String jsonStringDataAfter;
	
	@Column(name="request_status")
	private String requestStatus;

	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="update_datetime")
	private String updateDatetime;

	@Column(name="update_user_id")
	private Long updateUserId;
	
	public Long getApprovalRequestId() {
		return approvalRequestId;
	}

	public void setapprovalRequestId(Long approvalRequestId) {
		this.approvalRequestId = approvalRequestId;
	}

	public Long getUserIdRequest() {
		return userIdRequest;
	}

	public void setUserIdRequest(Long userIdRequest) {
		this.userIdRequest = userIdRequest;
	}

	public Long getRoleIdRequest() {
		return roleIdRequest;
	}

	public void setRoleIdRequest(Long roleIdRequest) {
		this.roleIdRequest = roleIdRequest;
	}

	public Long getRoleIdApprove() {
		return roleIdApprove;
	}

	public void setRoleIdApprove(Long roleIdApprove) {
		this.roleIdApprove = roleIdApprove;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getJsonStringData() {
		return jsonStringData;
	}

	public void setJsonStringData(String jsonStringData) {
		this.jsonStringData = jsonStringData;
	}
	
	public String getJsonStringDataBefore() {
		return jsonStringDataBefore;
	}

	public void setJsonStringDataBefore(String jsonStringDataBefore) {
		this.jsonStringDataBefore = jsonStringDataBefore;
	}

	public String getJsonStringDataAfter() {
		return jsonStringDataAfter;
	}

	public void setJsonStringDataAfter(String jsonStringDataAfter) {
		this.jsonStringDataAfter = jsonStringDataAfter;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}
	
}
