package com.mlpt.bvnextgen.model.approvalManagement;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;
import org.springframework.stereotype.Repository;


@Repository
public class ApprovalRequestDaoImpl extends BaseCoreDaoImpl<ApprovalRequest, Long> implements ApprovalRequestDao {

}