package com.mlpt.bvnextgen.model.approvalManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface ApprovalRequestDao extends BaseCoreDao<ApprovalRequest, Long> {

}