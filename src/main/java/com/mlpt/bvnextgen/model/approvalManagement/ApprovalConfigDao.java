package com.mlpt.bvnextgen.model.approvalManagement;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface ApprovalConfigDao extends BaseCoreDao<ApprovalConfig, Long> {

}