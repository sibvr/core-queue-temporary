package com.mlpt.bvnextgen.model.approvalManagement;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;


@Repository
public class ApprovalConfigDaoImpl extends BaseCoreDaoImpl<ApprovalConfig, Long> implements ApprovalConfigDao {

}