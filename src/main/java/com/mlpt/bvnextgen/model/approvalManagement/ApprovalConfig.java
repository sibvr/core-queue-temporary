package com.mlpt.bvnextgen.model.approvalManagement;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name=ApprovalConfig.ENTITY_NAME)
@Table(name=ApprovalConfig.TABLE_NAME)
public class ApprovalConfig implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "t_approval_config";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.ApprovalConfig";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="approval_config_id")
	private Long approvalConfigId;
	
	@Column(name="approval_id")
	private Long approvalId;	
	
	@Column(name="role_task_id_submit")
	private Long roleTaskIdSubmit;
	
	@Column(name="role_task_id_approval")
	private Long roleTaskIdApproval;

	@Column(name="flag_need_approval")
	private String flagNeedApproval;
	
	@Column(name="create_datetime")
	private String createDatetime;
	
	@Column(name="update_datetime")
	private String updateDatetime;
	
	@Column(name="create_user_id")
	private Long createUserId;
	
	@Column(name="update_user_id")
	private Long updateUserId;
	
	@Column(name="active")
	private String active;

	public Long getApprovalConfigId() {
		return approvalConfigId;
	}

	public void setApprovalConfigId(Long approvalConfigId) {
		this.approvalConfigId = approvalConfigId;
	}

	public Long getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(Long approvalId) {
		this.approvalId = approvalId;
	}

	public Long getRoleTaskIdSubmit() {
		return roleTaskIdSubmit;
	}

	public void setRoleTaskIdSubmit(Long roleTaskIdSubmit) {
		this.roleTaskIdSubmit = roleTaskIdSubmit;
	}

	public Long getRoleTaskIdApproval() {
		return roleTaskIdApproval;
	}

	public void setRoleTaskIdApproval(Long roleTaskIdApproval) {
		this.roleTaskIdApproval = roleTaskIdApproval;
	}

	public String getFlagNeedApproval() {
		return flagNeedApproval;
	}

	public void setFlagNeedApproval(String flagNeedApproval) {
		this.flagNeedApproval = flagNeedApproval;
	}

	public String getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(String createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getUpdateDatetime() {
		return updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
}
