package com.mlpt.bvnextgen.model.questionHelp;

import org.springframework.stereotype.Repository;

import com.mlpt.bvnextgen.model.BaseCoreDaoImpl;


@Repository
public class QuestionHelpDaoImpl extends BaseCoreDaoImpl<QuestionHelp, Long> implements QuestionHelpDao {

}