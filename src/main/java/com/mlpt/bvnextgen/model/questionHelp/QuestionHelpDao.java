package com.mlpt.bvnextgen.model.questionHelp;

import com.mlpt.bvnextgen.model.BaseCoreDao;

public interface QuestionHelpDao extends BaseCoreDao<QuestionHelp, Long> {

}