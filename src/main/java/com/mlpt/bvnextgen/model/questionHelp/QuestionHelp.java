package com.mlpt.bvnextgen.model.questionHelp;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name=QuestionHelp.ENTITY_NAME)
@Table(name=QuestionHelp.TABLE_NAME)
public class QuestionHelp implements Serializable{

	private static final long serialVersionUID = -1L;
	public static final String TABLE_NAME = "question_help";
	public static final String ENTITY_NAME = "com.mlpt.bvnextgen.entity.QuestionHelp";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
	@Column(name="question_id")
	private Long questionId;	

	@Column(name="question")
	private String question;

	@Column(name="queueing_destination")
	private String queueingDestination;
	

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQueueingDestination() {
		return queueingDestination;
	}

	public void setQueueingDestination(String queueingDestination) {
		this.queueingDestination = queueingDestination;
	}
	

	
	
	
}
